/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform,
  View,
  Image,
  Alert,
  Dimensions,
  AsyncStorage
} from 'react-native';
import {AppContainer} from './Router';
import {SafeAreaView} from 'react-native';
import configureStore from './src/store/ConfigureStore';
import {Provider} from 'react-redux';
// import type { Notification } from 'react-native-firebase';
 import firebase from 'react-native-firebase';

const {height, width} = Dimensions.get("window");
const store = configureStore();



export default class App extends Component {

  constructor() {
    super();
    console.disableYellowBox = true;
    this.state={
      isnadroid: false
    }
  }
  
  async componentDidMount() {
    setTimeout(()=>{
      this.setState({isnadroid: true});
  
    }, 1);

    const enabled = await firebase.messaging().hasPermission();
if (enabled) {
    // user has permissions
} else {
  try {
    await firebase.messaging().requestPermission();
    // User has authorised
} catch (error) {
    // User has rejected permissions
}
}

// this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
//   // alert("display")
//   // alert('display',notification)
//   // Process your notification as required
//   // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
// });
// this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
// //  alert('on',notification)
//   // Process your notification as required
//   // alert("on")
// });
    // const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
    // .setDescription('My apps test channel');
    // firebase.notifications().android.createChannel(channel);
  }

  componentWillUnmount() {
    // this.notificationDisplayedListener();
    // this.notificationListener();
}


  render() {

    return (
      <Provider store={store}>
          <SafeAreaView style={{flex:1}}>
            <AppContainer/>
        </SafeAreaView>
     </Provider>

    );
    
  }
}