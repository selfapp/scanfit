
import React from 'react';
import { Image } from 'react-native';

import {createAppContainer, } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import Splash from './src/Screens/Splash';
import Login from './src/Screens/loginAndRegistration/Login';
import Registration from './src/Screens/loginAndRegistration/Registration';
import Equipments from './src/Screens/loginAndRegistration/Equipments';
import AddWquipment from './src/Screens/loginAndRegistration/AddWquipment';
import EquipmentList from './src/Screens/loginAndRegistration/EquipmentList';
import WorkoutDaySelection from './src/Screens/loginAndRegistration/WorkoutDaySelection';
import CalendarSelection from './src/Screens/loginAndRegistration/CalendarSelection';

import Otp from './src/Screens/loginAndRegistration/Otp';
import TellUs from './src/Screens/loginAndRegistration/TellUs';
import WhatsYourEndGoal from './src/Screens/loginAndRegistration/WhatsYourEndGoal';
import WhereAreYou from './src/Screens/loginAndRegistration/WhereAreYou';
import IdentifyUrself from './src/Screens/loginAndRegistration/IdentifyUrself';

import WelcomePage from './src/Screens/tabBar/WelcomePage';
import UserListing from './src/Screens/tabBar/chatting/UserListing';
import CameraView from './src/Screens/tabBar/CameraView';
import Setting from './src/Screens/tabBar/Setting';
import Contactus from './src/Screens/tabBar/contactus';
import Aboutus from './src/Screens/tabBar/aboutus';

import Profile from './src/Screens/tabBar/Profile';
import MyAssesment from './src/Screens/tabBar/myAssesment';

import SideMenu from './src/Screens/tabBar/SideMenu';
import HomeView from './src/Screens/tabBar/HomeView';
import Artical from './src/Screens/tabBar/artical';

//workoutplansetting
import wpTellus from './src/Screens/tabBar/woroutplanSetting/wpTellus';
import wpAllset from './src/Screens/tabBar/woroutplanSetting/wpAllset';
import wpDays from './src/Screens/tabBar/woroutplanSetting/wpDays';
import wpEndgoal from './src/Screens/tabBar/woroutplanSetting/wpEndgoal';
import wpIdentify from './src/Screens/tabBar/woroutplanSetting/wpIdentify';
import wpwhere from './src/Screens/tabBar/woroutplanSetting/wpwhere';

//chat
import ListContacts from './src/Screens/tabBar/chatting/ListContacts';
import ChatScreen from './src/Screens/tabBar/chatting/ChatScreen';

export const  workoutPlanSetting = createStackNavigator(

    {
        wpTellus:{
            screen:wpTellus
        },
        wpAllset:{
            screen:wpAllset
        },
        wpDays:{
            screen:wpDays
        },
        wpEndgoal:{
            screen:wpEndgoal
        },
        wpIdentify:{
            screen:wpIdentify
        },
        wpwhere:{
            screen:wpwhere
        },
    },{
        headerMode:'none'
    }
);

export const loginNavigationOption = createStackNavigator(

    {
        Login:{
            screen:Login
        },
        Otp:{
            screen:Otp
        }
    },{
        headerMode:'none'
    }
);


export const registrationNavigationOption = createStackNavigator(

    {
       
        TellUs:{
            screen:TellUs
        },
        IdentifyUrself:{
            screen:IdentifyUrself
        },
        WhereAreYou:{
            screen:WhereAreYou
        },
        WhatsYourEndGoal:{
            screen:WhatsYourEndGoal
        },
        Login:{
            screen:Login
        },
        Otp:{
            screen:Otp
        },
        Registration:{
            screen:Registration
        },
        Equipments:{
            screen:Equipments
        },
        EquipmentList:{
            screen:EquipmentList
        },
        AddWquipment:{
            screen:AddWquipment
        },
        WorkoutDaySelection:{
            screen:WorkoutDaySelection
        },
         CalendarSelection:{
            screen:CalendarSelection
        },
    },{
        headerMode:'none'
    }
);
export const ChatNavigation = createStackNavigator({
    UserList:{
        screen:UserListing
    },
    MyContacts:{
        screen:ListContacts
    },
    ChatPage:{
        screen:ChatScreen,
    }
},
{
    headerMode: 'none'
})

ChatNavigation.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    for (let i = 0; i < navigation.state.routes.length; i++) {
      if (navigation.state.routes[i].routeName == "ChatPage") {
        tabBarVisible = false;
      }
    }
  
    return {
      tabBarVisible
    };
  };
  
export const sideNavigationOptions = createStackNavigator(
    {
        SideMenu: {
            screen: SideMenu
        },
        Setting:{
            screen:Setting
        },
        contactus:{
        screen:Contactus
        },
        aboutus:{
            screen:Aboutus
            },
        Profile:{
            screen:Profile
        },
        MyAssesment:{
            screen:MyAssesment
        },
        SNEquipments:{
            screen:Equipments
        },
        SNEquipmentList:{
            screen:EquipmentList
        },
        SNWorkout:{
            screen:workoutPlanSetting
        }
    },
    {
        headerMode: 'none'
    }
);

sideNavigationOptions.navigationOptions = ({ navigation }) => {
    console.log("sidenav->",navigation.state.routes[navigation.state.index].routeName,navigation.state.index)
    let tabBarVisible = true; 
    if(navigation.state.routes[navigation.state.index].routeName==='SNEquipments'){
        tabBarVisible=false
    }

    return {
      tabBarVisible,
    };
};

export const HomeArtical = createStackNavigator(
    {
        HomeView: {
            screen: HomeView
        },
        Artical:{
            screen:Artical
        },
        
    },
    {
        headerMode: 'none'
    }
);

export const CamNavigation = createStackNavigator({
    CameraView:{
        screen:CameraView
    },
},
{
    headerMode: 'none'
})
CamNavigation.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    for (let i = 0; i < navigation.state.routes.length; i++) {
      if (navigation.state.routes[i].routeName == "CameraView") {
         
        tabBarVisible = false;
      }
    }
  
    return {
      tabBarVisible
    };
  };
export const TabNavigator = createBottomTabNavigator(
    {
        WELCOME:{
            screen: WelcomePage,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/book.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        CHAT:{
            screen:ChatNavigation,
            navigationOptions:{
                
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/message.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                },
            }
        },
        CAMERA:{
            screen: CamNavigation,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/camera.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        HOME:{
            screen: HomeArtical,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/home.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        MENU:{
            screen: sideNavigationOptions ,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/menu.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        }
    },
    {
        initialRouteName: 'WELCOME',        
        tabBarOptions: {
            activeTintColor: "#69D3A9",
            inactiveTintColor: "gray",
            showLabel: false,
            // showIcon: true,
            tabBarPosition: 'bottom',
            // labelStyle: {
            //     fontSize: 12,
            // },
            iconStyle:{
                width: 30,
                height: 30
            },
            // style: {
            //     backgroundColor: 'rgb(245,245,245)',
            //     borderBottomWidth: 1,
            //     borderBottomColor: '#ededed',
            //     alignItems: 'center',
            //     justifyContent: 'center',
            //     alignSelf: 'center',
            //     // height: ((Platform.OS === 'ios' && iphoneHeight === 812) ? 20 : 55)
            // },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
);

export const appNavigationOptions = createStackNavigator(

    {
        Splash:{
            screen: Splash
        },
        registrationNavigationOption:{
            screen:registrationNavigationOption
        }, loginNavigationOption:{
            screen:loginNavigationOption
        }, TabNavigator: {
            screen: TabNavigator
        }
    },{
        headerMode:'none'
    }
);

export const AppContainer = createAppContainer(appNavigationOptions);
