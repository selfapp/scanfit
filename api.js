
import { AsyncStorage } from 'react-native';
const baseURL = 'http://18.219.161.44/api';//Staging
// const baseURL = 'http://127.0.0.1:8000/api';//Local

var api = {
    request(url, method, body, user_id_required=false) {
        // AsyncStorage.setItem('accesstoken',response.access_token)

        return AsyncStorage.getItem('accesstoken').then((data)=> {
            if (user_id_required) {
               
                let userObject = JSON.parse(data);
               
                return fetch(baseURL+url+userObject.user.id, {
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+ (userObject ? userObject : null)
                    },
                    body: body === null ? null : JSON.stringify(body)
                })
            } else {
                // let userObject = JSON.parse(data);
               
                return fetch(baseURL+url, {
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+ (data ? data : null)
                    },
                   
                    body: body === null ? null : JSON.stringify(body)
                })
                
            }
        })

    },
    // getLocationRequest(url, method, header) {
    //     return fetch(url, {
    //         method: method,
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': header
    //         }
    //     })
    // },
    renewAccessToken() {
        return AsyncStorage.getItem('user').then((data)=> {
            let userObject = JSON.parse(data);
            return fetch(baseURL + '/refresh-token', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"refresh_token": userObject.refresh_token})
            })
        });
    },
    // createCardToken(body) {
    //     return fetch('https://api.stripe.com/v1/tokens', {
    //         method: 'post',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/x-www-form-urlencoded',
    //            // 'Authorization': 'Bearer pk_test_RwUKzGSejW84DKPvd6fCEZwT006hHYs9qt',

    //             // 'Authorization': 'Bearer sk_test_cylJrk0BByeCrn2vPK8Ep5Nc',
    //             'Authorization': 'Bearer sk_live_kzuDVhRYleNIqOcfR8vsEy1K',
    //         },
    //         body: body
    //     });
    // },
};


module.exports = api;