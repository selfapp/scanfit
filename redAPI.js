import api from './api'

export default class API {

    static fetchMyEquipmentList(){
        return api.request('/my-equipments', 'GET', null, null)
    }

    static addEquipment(item){
        let body = {
            'equipment_id': item.id
    };
        return api.request('/add-equipment', 'post', body);
    }
    static profile(body){
        return api.request('/update-profile', 'post', body);
    }
    static todWorkout(body){
        return api.request('/find-todays-exercises','post',body)
    }
    static completexr (body){
        return api.request('/complete-exercises','post',body)
    }
    static  yrActivity (){
        return api.request('/my-activity','get')
    }
    static  async getProfile (){
         return api.request('/get-profile','get')
    }
    static  async getBlogList (page){
         return api.request(`/blog-list?page=${page}`,'get')
    }
    static  async getBlogDetails (id){
        return api.request(`/blog-details/${id}`,'get')
   }
   static  async getUserList (body){
    return api.request('/search-user','post',body)
}
static  async getAvailableContacts (body){
    return api.request('/chatkit/get-sf-users-from-contacts','post',body)
}
static  async createRoom (body){
    return api.request('/chatkit/create-room','post',body)
}
static async getotherParty (sendid){
    return api.request('/chatkit/get-user-by-id','post',{id:sendid})
}

static async getmyrooms (sendid){
    return api.request('/chatkit/fetch-room-by-user-id','post',{id:sendid})
}
static async saveDeviceTokenApi (token){
    return api.request('/update-device-token','post',{device_token:token})
}
static async getMyAssesment (){
    return api.request(`/get-my-assessments`,'get')
}
static async PostMyAssesment (body){
    return api.request(`/post-my-assessments`,'POST',body)
}
static async UploadImage (body){
    return api.request(`/chatkit/chat-image`,'POST',body)
}
static async DeleteMessage (body){
    return api.request(`/chatkit/delete-message`,'POST',body)
}
static async DeleteChatRoom (body){
    return api.request(`/chatkit/delete-room`,'POST',body)
}
static async CheckDob(body){
    return api.request(`/check-age`,'POST',body)
}
}