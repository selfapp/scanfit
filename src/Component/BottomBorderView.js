
import React from 'react';
import {View} from 'react-native';

export default BottomBorderView = (props) => {

    return(
        <View style={{

            marginHorizontal:props.horizontal,
            backgroundColor:'#ECECEC',
            height:2,
            marginTop:props.top
        }}>
        </View>
    )
}