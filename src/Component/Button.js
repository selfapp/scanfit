
import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

export default Button = (props) => {

    return(
        <TouchableOpacity style={{
           marginHorizontal:props.horizontal,
           marginTop:props.top,
           borderRadius:props.radius,
           backgroundColor:props.backgColor,
           height:props.height,
           alignItems:'center',
           justifyContent:'center',
           marginBottom:props.bottom,
           width:props.width
        }} onPress={props.buttonAction}>
            <Text style={{
                color:props.textColor,
                fontSize:props.titleSize,
                fontWeight:props.weight
            }}>{props.title} {"  "}
            </Text>
        </TouchableOpacity>
    )
}