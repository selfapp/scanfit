

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Modal, Alert,
  Platform,Picker
} from 'react-native';
import moment, { months, invalid } from 'moment';
import BottomBorderView from './BottomBorderView';
import DatePicker from 'react-native-datepicker';
import DatePickerAndroid from './dateandroid';
import HeightPickerAndroid from './heightPickerAndroid';
var agetype=false
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height
export default class ExpandableItemComponent extends Component {
  constructor() {
    super();
    this.state = {
      H_feet:'5',
      H_inch:'5',
      layoutHeight: 0,
      checkValue: 'tttt', showpicker: false,showpickerHeight:false,
      dateselected: moment().add(-18, 'years').toDate(),
      D_date: moment().add(-18, 'years').toDate(),
      iosDated:false,
      
    };
  }
  componentDidMount() {
    agetype=this.props.ageType;
    if (this.props.age1) {
    }
    if ((this.props.age2 !== null) && (this.props.age2)) {
       var d =this.props.age2;
       var dateToStore= new Date(d);
     if(true){
       this.setState({
        dateselected:dateToStore
      },()=>{
      })
     }else{
     }
      
    }

    if (this.props.height1) {
      let splited = this.props.height1.split(".");
      Sheight = splited[0] + "'." + splited[1] + '"';
      this.setState({
        H_feet:splited[0],
        H_inch:splited[1],
      },()=>{
      })
    }
    if(this.props.height2){
     
      let splited = this.props.height2.split(".");
      this.setState({
        H_feet:splited[0],
        H_inch:splited[1],
      })
    }
    
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.item.testExpand) {
      this.setState(() => {
        return {
          checkValue: 'rrrrrr',
        };
      });
    }
    if (nextProps.item.isExpanded) {
      this.setState(() => {
        return {
          layoutHeight: null,
        };
      });
    } else {
      this.setState(() => {
        return {
          layoutHeight: 0,
        };
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.layoutHeight !== nextState.layoutHeight) {
      return true;
    }
    if (this.state.checkValue) {
      return true;
    }
    return false;
  }
  actionPerformOnCell(sectionCount, value, itemId) {
    this.props.updateUI(sectionCount, value, itemId)
  }
  confirmHEIGHT(){
    this.props.updateHeight(this.state.H_feet,this.state.H_inch)
  }
  render() {
    let Sheight;
    if (this.props.height1) {
      let splited = this.props.height1.split(".");
      Sheight = splited[0] + "'." + splited[1] + '"';
    }
    return (
      <View>
        {/*Header of the Expandable List Item*/}
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.props.onClickFunction}
          style={styles.header}>
          <Image
            style={{ height: this.props.item.height, width: this.props.item.width, marginRight: 5 }}
            source={this.props.item.image} resizeMode='contain'
          />
          {
            this.props.item.index === 2 ? (
              <Text style={[styles.headerText, { width: 90 }]}>{this.props.item.category_name}</Text>

            ) : (this.props.item.index !==4) ?(
                <Text style={[styles.headerText]}>{this.props.item.category_name}</Text>
              ):(
                <Text style={[styles.headerText,{width:350}]}>{this.props.item.category_name}</Text>
              )
      
          }
          {/* { width: 140 } */}
          {
            this.props.item.index === 0 ? (
              <View>

                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.showpicker}
                onRequestClose={() => {
                  this.setState({showpicker:false})
                }}
                >
                  <View style={{ backgroundColor: '#888888a3', height: height, flex: 1 }}>
                    <View style={{ position: 'absolute', bottom: 0, width: width, height: 265,backgroundColor:'white' }}>
                      <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 50, padding: 10 }}>
                        <TouchableOpacity
                          style={{ position: 'absolute', left: 15, top: '50%' }}
                          onPress={() => {
                            this.setState({ showpicker: !this.state.showpicker })
                          }}>
                          <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 15, fontWeight: 'bold' }}>Cancel</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          style={{ position: 'absolute', right: 15, top: '50%' }}
                          onPress={() => {
                              this.props.updateAge(moment(this.state.dateselected).format('MM/DD/YYYY'))
                              this.setState({ showpicker: !this.state.showpicker })
                          }}>
                          <Text style={{ fontFamily: 'Montserrat-Regular', color: '#8cd3be', fontSize: 15, fontWeight: 'bold' }}>Confirm</Text>
                        </TouchableOpacity>

                      </View>
                      <BottomBorderView
                        horizontal={15}
                        top={0}
                      />

                      <DatePickerAndroid
                        mode="date"
                        labelUnit={{ month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'OCtober', 'November', 'December'], year: '', date: '' }}
                        date={this.state.dateselected}
                        // selectedDate={this.state.D_date}
                        showIcon={false}
                        order="M-D-Y"
                        maximumDate={new Date(moment().add(-220, 'months').toDate())}
                        onDateChange={date => {
                            this.props.updateAge(moment(date).format('MM/DD/YYYY')); this.setState({ dateselected: date }) }}
                        style={{ backgroundColor: 'white' }}
                      />

                    </View>
                  </View>
                </Modal>




                {
                  Platform.OS === "android" ?
                    <TouchableOpacity 
                    disabled={agetype}
                    style={{
                      width: 110,
                      marginRight: 20,
                      textAlign: 'right',
                      alignItems: 'flex-end',
                      height: 40,
                      justifyContent: 'center'
                    }}
                      onPress={() => this.setState({ showpicker: !this.state.showpicker })} >
                      <Text >{this.state.dateselected !== 'MM/DD/YYYY' ? moment(this.state.dateselected).format('MM/DD/YYYY') : 'MM/DD/YYYY'}</Text>
                    </TouchableOpacity>
                    :
                    // <Text>rgvds</Text>
                    <DatePicker
                      disabled={this.props.ageType}
                      style={{ width: 120, alignSelf: 'flex-end',}}
                      date={this.props.age1}
                      mode="date" //The enum of date, datetime and time
                      placeholder="MM/DD/YYYY"
                      format="MM/DD/YYYY"
                      maxDate={new Date(moment().add(-220, 'months').toDate())}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      showIcon={false}
                      customStyles={{
                        dateInput: {
                          borderWidth: 0,
                          alignItems: 'flex-end',
                        },
                        disabled: {
                          backgroundColor: 'white',
                          marginLeft: 20
                        },
                        dateText: {
                          textAlign: 'right',

                        }
                      }}
                      onDateChange={date => {this.props.updateAge(date);this.setState({iosDated:true})}}
                    />
                }
              </View>

            ) : (this.props.item.index === 1 ? (
              <View>


<Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.showpickerHeight}
                onRequestClose={() => {
                   this.setState({showpickerHeight:false})
                }}
                >
                  <View style={{ backgroundColor: 'rgba(255,255,255,0.4)', height: height, flex: 1 }}>
                    <View style={{ position: 'absolute', bottom: 0, width: width, height: 265, backgroundColor:'white'}}>
                      <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 50, padding: 10 }}>
                        <TouchableOpacity
                          style={{ position: 'absolute', left: 15, top: '50%' }}
                          onPress={() => {
                            this.setState({ showpickerHeight: !this.state.showpickerHeight })
                          }}>
                          <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 15, fontWeight: 'bold' }}>Cancel</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          style={{ position: 'absolute', right: 15, top: '50%' }}
                          onPress={() => {
                            this.confirmHEIGHT();
                            this.setState({ showpickerHeight: !this.state.showpickerHeight })
                          }}>
                          <Text style={{ fontFamily: 'Montserrat-Regular', color: '#8cd3be', fontSize: 15, fontWeight: 'bold' }}>Confirm</Text>
                        </TouchableOpacity>

                      </View>
                      <BottomBorderView
                        horizontal={15}
                        top={0}
                      />
                      {
                        Platform.OS === "android"?
                        <View>
                            <HeightPickerAndroid
                            height={{feet:this.state.H_feet,inch:this.state.H_inch}}
                        showIcon={false}
                        onHeightChange={height => {
                          this.setState({
                            H_inch:height.inch,
                            H_feet:height.feet
                          }, ()=>{this.confirmHEIGHT();})
                        }}
                        style={{ backgroundColor: 'white' }}
                      />
                        </View>:
                        <View style={{flexDirection:'row',justifyContent:'space-around',backgroundColor:'white'}}>
                          <View style={{width:'50%',alignItems:'center'}}>

<Text style={{position:'absolute', left:'40%', fontWeight:'bold',padding:10}}>Feet</Text>
                          <Picker
  selectedValue={this.state.H_feet}
  style={{height: 50, width: 100}}
  onValueChange={(itemValue, itemIndex) =>
    this.setState({H_feet: itemValue})
  }>
    {/* ['4', '5', '6', '7', '8','9'] */}
  <Picker.Item label="4" value="4" />
  <Picker.Item label="5" value="5" />
  <Picker.Item label="6" value="6" />
  <Picker.Item label="7" value="7" />
  <Picker.Item label="8" value="8" />
  <Picker.Item label="9" value="9" />
</Picker>
                          </View>
                          <View style={{width:'50%',alignItems:'center'}}>
<Text style={{position:'absolute', right:'40%', fontWeight:'bold',padding:10}}>Inch</Text>

 <Picker
  selectedValue={this.state.H_inch}
  style={{height: 50, width: 100,}}
  onValueChange={(itemValue, itemIndex) =>
    this.setState({H_inch: itemValue})
  }>
  <Picker.Item label="0" value="0" />
  <Picker.Item label="1" value="1" />
  <Picker.Item label="2" value="2" />
  <Picker.Item label="3" value="3" />
  <Picker.Item label="4" value="4" />
  <Picker.Item label="5" value="5" />
  <Picker.Item label="6" value="6" />
  <Picker.Item label="7" value="7" />
  <Picker.Item label="8" value="8" />
  <Picker.Item label="9" value="9" />
  <Picker.Item label="10" value="10" />
  <Picker.Item label="11" value="11" />
  {/* ['0', '1', '2', '3', '4', '5', '6', '7', '8','9','10','11'] */}
</Picker>
</View>
                        </View>
                      }
                    </View>
                  </View>
                </Modal>
                 <TouchableOpacity style={{
                width: 100,
                marginRight: 20,
                textAlign: 'right',
                height: 40,
                justifyContent: 'center'
              }} onPress={
               ()=> this.setState({showpickerHeight:true})
                }>
                <Text style={{ fontSize: 13, textAlign: 'right', }}>
                  {this.props.height1 ? Sheight : 'Select height'}
                </Text>
              </TouchableOpacity>
              </View>
            ) : (this.props.item.index === 2 ? (
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TextInput style={[styles.detailText, { width: (width - 215) / 2, marginRight: 5 }]}
                  placeholder='0'
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={this.props.weight1}
                  keyboardType='decimal-pad'
                  onChangeText={value => this.props.updateWeight(value)} />
                <Text> kgs </Text>

                <TextInput style={[styles.detailText, { width: (width - 215) / 2, marginLeft: 10, marginRight: 5 }]}

                  placeholder='0'
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={this.props.weightLbs}
                  keyboardType='decimal-pad'
                  onChangeText={value => this.props.updateWeightInLbs(value)} />
                <Text> lbs </Text>
              </View>
            ) : (null)))
          }

        </TouchableOpacity>
        <BottomBorderView
          horizontal={15}
          top={0}
        />
        <View
          style={{
            height: this.state.layoutHeight,
            overflow: 'hidden',
          }}>
          {/*Content under the header of the Expandable List Item*/}
          {this.props.item.subcategory.map((item, key) => (
            <View key={key}>
              <TouchableOpacity
                key={key}
                style={styles.content}
                onPress={() => this.actionPerformOnCell(this.props.item.index, item.val, item.id)}>
                <Image
                  style={{ height: 26, width: 20, }}
                  source={item.image} resizeMode='contain'
                />
                <Text style={styles.text}>
                  {item.val}
                </Text>
                {
                  this.state.checkValue.length > 0 ? (
                    <View style={{ height: 0 }}>
                    </View>
                  ) : (null)
                }
                {
                  this.props.item.index === 3 ? (
                    item.isCheck ? (
                      <Image style={styles.imageCheckBox} source={require('../assets/check.png')} />
                    ) : (
                        <Image style={styles.imageCheckBox} source={require('../assets/uncheck.png')} />
                      )) : (this.props.item.index === 4 ? (
                        item.isCheck ? (
                          <Image style={styles.imageCheckBox} source={require('../assets/check.png')} />
                        ) : (
                            <Image style={styles.imageCheckBox} source={require('../assets/uncheck.png')} />
                          )
                      ) : (null))
                }


              </TouchableOpacity>
              <View style={styles.separator} />
            </View>


          ))}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({

  header: {
    padding: 15,
    flexDirection: 'row',
    // backgroundColor: 'red',
    height: 50,
    alignItems: 'center'
  },
  headerText: {
    fontSize: 13,
    fontWeight: '300',
    // width:210,
    width: width - 170,
    marginLeft: 7
    // backgroundColor:'pink'
  },
  detailText: {
    fontSize: 13,
    width: 100,
    marginRight: 20,
    textAlign: 'right',
    // backgroundColor:'yellow',
    height: 40
  },
  separator: {
    height: 0.5,
    backgroundColor: '#808080',
    marginLeft: 30,
    marginRight: 30,
  },
  text: {
    fontSize: 12,
    color: '#606070',
    padding: 10,
    width: width - 110,
    // backgroundColor:'yellow'
  },
  content: {
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor:'red',
    height: 50
  },
  imageCheckBox: { width: 15, height: 15, marginLeft: 10 }
});
