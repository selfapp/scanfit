
import React, { Component } from 'react';
import {
    View, Text, KeyboardAvoidingView, Dimensions, Keyboard,
    TouchableOpacity, Image, ScrollView, StyleSheet, FlatList, TextInput,
    ActivityIndicator, Platform, StatusBar, BackHandler,Alert,Modal,TouchableWithoutFeedback,
} from 'react-native';
import MyActivityIndicator from './activity_indicator';
import ImagePicker from 'react-native-image-crop-picker';
import { withNavigationFocus } from 'react-navigation';
import { RNCamera } from 'react-native-camera';
import ToggleSwitch from 'toggle-switch-react-native';

var once = 1

const width = Dimensions.get("window").width;
class ScanCamera extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Image64: null,
            orignalImage: null,
            Scanned: false,
            isHome: false,
            equipmentName: '',
            loader: false,
            noData: false,
            enableScrollViewScroll: true,
            arrEqip: [],
            arrAddedEquipIndex: [],
            selectedItems: [],
            openList: false,
            userCancelled: false,
            focusedScreen: false,
            isScan: false,
            shareable: false,
            SearchResult:{},
            modalVisible: false,
            selected:null,
            NoClicked:false,
            front:false
            
        }
        this.arrEqipInitial = [];
    }
    
  

    componentDidMount() {
        const { navigation } = this.props;
        navigation.addListener('willFocus', () =>
            this.setState({
                focusedScreen: true,
                 Image64: null,
                Scanned: false,
            })
        );
        navigation.addListener('willBlur', () =>
            this.setState({
                focusedScreen: false, Image64: null,
                orignalImage: null,
                Scanned: false,
            })
        );
      
    } 

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            // console.log(data);
            this.props.OnClickPickture( `data:image/jpeg;base64,${data.base64}`)

        }
    };



    render() {
        const { selectedItems } = this.state;
        const { focusedScreen } = this.state;
        return (
            <View style={{ flex: 1, }}>
         {this.state.loader && <MyActivityIndicator /> }
       
 
                {!this.state.Scanned ?
            
                    <View style={styles.container}>
                       
                        {
                         ( this.state.front ?<RNCamera
                            flashMode={RNCamera.Constants.FlashMode.off}
                            type={RNCamera.Constants.Type.front}
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={RNCamera.Constants.Type.front}
                                flashMode={RNCamera.Constants.FlashMode.off}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                            />:
                            <RNCamera
                            flashMode={RNCamera.Constants.FlashMode.off}
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={RNCamera.Constants.Type.back}
                                flashMode={RNCamera.Constants.FlashMode.off}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                            />)
                        }
                         <View style={{height:50,width:'100%',justifyContent:'space-between', flexDirection:'row',alignItems:'center',paddingHorizontal:20,paddingTop:20,position:'absolute',top:20,}}>
                            <TouchableOpacity onPress={this.props.Arrow}>
                                <Image  source={require('../assets/arrow.png')} style={{tintColor:'white'}} />
                                </TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this.setState({front:!this.state.front}),console.log(this.state)}}><Image  source={require('../assets/flip.png')} style={{tintColor:'white'}} /></TouchableOpacity>
                        </View>
                        <View style={{ left: '5%', position: 'absolute', top: 90, height: Dimensions.get('window').height*0.2, flex: 0, width: '90%', flexDirection: 'row', justifyContent: 'center', }}>
                            <Image source={require('../assets/camcenter.png')} style={{height: Dimensions.get('window').height*0.6,}} resizeMode={'contain'} />
                        </View>
                        <View style={{ position: 'absolute', bottom: -10, flex: 0, width: '100%', flexDirection: 'row', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.4)', height: 80, alignItems: 'center', justifyContent: 'space-evenly' }}>
                            <View style={{ width: '33%' }} >
                                <TouchableOpacity
                                //  disabled={this.state.isScan}
                                  onPress={
                                    () => {
                                        ImagePicker.openPicker({
                                            // width: 300,
                                            // height: 400,
                                            cropping: false,
                                            includeBase64: true,
                                            compressImageQuality: 0.5,
                                            mediaType: 'photo'
                                        }).then(image => {
                                            this.props.OnClickPickture(`data:image/jpeg;base64,${image.data}`)
                                        })
                                    }

                                } style={[styles.capture, {}]} >
                                    <Image source={require('../assets/camgalery.png')} style={{ tintColor: 'white', height: 50, width: 50 }} />
                                    {/* <Text style={{ fontSize: 14 }}> SNAP </Text> */}
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '33%' }} >
                                <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                                    <Image source={require('../assets/bt-camera.png')} style={{ tintColor: 'white', height: 50, width: 50 }} />
                                    {/* <Text style={{ fontSize: 14 }}> SNAP </Text> */}
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '33%' }} >
                                {
                                    this.props.isToggle && <View style={[, { flexDirection: 'row' }]}>
                                    {<Text style={{ color: !this.state.isScan ? 'green' : 'white', padding: 5,fontSize:8}}>Scan</Text>}
                                    <ToggleSwitch
                                        isOn={this.state.isScan}
                                        onColor="green"
                                        offColor="#d3d3d3"
                                        label=""
                                        labelStyle={{ color: "white", fontWeight: "900" }}
                                        size="small"
                                        onToggle={() => { this.setState({ isScan: !this.state.isScan }) }}
                                    />
                                    {<Text style={[this.state.isScan ? { color: 'green', padding: 5,fontSize:8 } : { color: "white", padding: 5,fontSize:8 }]}>Photo</Text>}
                                </View>
                                }
                            </View>
                        </View>
                    </View>

                    :

                    <View style={{ flex: 1,backgroundColor:'black' }}>
                      
                        
                    </View>

                }
            </View>


        )

    }

}



export default withNavigationFocus(ScanCamera);
const styles = StyleSheet.create({
    mainContainer: { flex: 1, marginHorizontal: 15, marginVertical: 15 },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',

    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        borderRadius: 5,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
});