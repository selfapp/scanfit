import React from 'react';
import {TextInput, View, Image} from 'react-native';

export default TextField = (props) => {

    return(

        <View style={{
            marginHorizontal:props.horizontal,
            flexDirection:'row',
            height:35,
            marginTop:20,
            // height:props.height,
            // marginTop:props.top,
            // backgroundColor:'red',
            alignItems:'center',
        }}>
            <Image
                style={{ height: 20, width: 28}}
                source={props.image} resizeMode='contain'
                        />
                <TextInput style={{
                    fontSize:props.size,
                    fontWeight:props.weight,
                    height:50,
                    marginLeft:10,
                    flex:1
                }}  placeholder={props.placeholderText}
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    maxLength={props.length}
                    value={props.value}
                    keyboardType={props.keyboard}
                    onChangeText={props.onChangeText}
                    // keyboardType='phone-pad'
                >
                </TextInput>
        </View>
    )
}