
import React from 'react';
import {Text, View} from 'react-native';

export default TitleText = (props) => {

    return(
        <View style={{justifyContent:'center', alignItems:'center'}}>
            <Text style={{

                fontSize:props.size,
                color:props.color,
                fontWeight:props.weight,
                textAlign:props.alignment,
                // textAlignVertical: 'center',
                width:props.width,
                marginTop:props.top,
                fontFamily: 'Montserrat-Regular'
            }}>{props.title}
            </Text>
        </View>
    )
}