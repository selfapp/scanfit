import React, { PureComponent } from 'react';
import { ColorPropType, StyleSheet, View, ViewPropTypes as RNViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import Picker from './picker';

const ViewPropTypes = RNViewPropTypes || View.propTypes;

const styles = StyleSheet.create({
  picker: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
});

const stylesFromProps = props => ({
  itemSpace: props.itemSpace,
  textColor: props.textColor,
  textSize: props.textSize,
  style: props.style,
});

export default class DatePicker extends PureComponent {
  static propTypes = {
    labelUnit: PropTypes.shape({
      year: PropTypes.string,
      month: PropTypes.string,
      date: PropTypes.string,
    }),
    order: PropTypes.string,
    date: PropTypes.instanceOf(Date).isRequired,
    maximumDate: PropTypes.instanceOf(Date),
    minimumDate: PropTypes.instanceOf(Date),
    mode: PropTypes.oneOf(['date', 'time', 'datetime']),
    onDateChange: PropTypes.func.isRequired,
    style: ViewPropTypes.style,
    textColor: ColorPropType,
    textSize: PropTypes.number,
    itemSpace: PropTypes.number,
    selectedDate: PropTypes.number,
  };

  static defaultProps = {
    labelUnit: { year: '', month: '', date: '' },
    order: 'D-M-Y',
    mode: 'date',
    maximumDate: moment().add(10, 'years').toDate(),
    minimumDate: moment().add(-100, 'years').toDate(),
    date: new Date(),
    style: null,
    textColor: '#333',
    textSize: 20,
    itemSpace: 20,
  };

  constructor(props) {
    super(props);

    const { date, minimumDate, maximumDate, labelUnit } = props;

    this.state = { date, monthRange: [], yearRange: [], M_month:'',M_date:'',M_year:''};

    this.newValue = {};

    this.parseDate(date);
    const mdate = moment(date);

    const dayNum = mdate.daysInMonth();
    this.state.dayRange = this.genDateRange(dayNum);

    const minYear = minimumDate.getFullYear();
    const maxYear = maximumDate.getFullYear();

    for (let i = 1; i <= 12; i += 1) {
      [{ value: i, label: labelUnit.month[i-1] }].push({ value: i, label: labelUnit.month[i-1] });
    }

    this.state.yearRange.push({ value: minYear, label: `${minYear}${labelUnit.year}` });

    for (let i = minYear + 1; i <= maxYear; i += 1) {
      this.state.yearRange.push({ value: i, label: `${i}${labelUnit.year}` });
    }
  }
componentDidMount(){
  var CurrentDateObj = this.props.date;
var C_Month=CurrentDateObj.getMonth();
var C_Date = CurrentDateObj.getDate();
var C_year = CurrentDateObj.getFullYear();
  this.setState({
    M_month:C_Month+1,
    M_date:C_Date,
    M_year:C_year
  },()=>{
    var tosend = new Date(this.state.M_year,this.state.M_month,this.state.M_date)
  })
}
 

  parseDate = (date) => {
    const mdate = moment(date);

    ['year', 'month', 'date', 'hour', 'minute'].forEach((s) => { this.newValue[s] = mdate.get(s); });
  }

  

  genDateRange(dayNum) {
    const days = [];

    for (let i = 1; i <= dayNum; i += 1) {
      days.push({ value: i, label: `${i}${this.props.labelUnit.date}` });
    }

    return days;
  }

  handelMonth=async(month)=>{
    var {M_month , M_year, dayRange} = this.state;
                var dayNum=0,newDayRange=0;

 if (M_month !== month) {
  dayNum = moment(`${M_year}-${month}`, 'YYYY-MM').daysInMonth();
  if (dayNum !== dayRange.length){
    newDayRange =  await this.genDateRange(dayNum);
    this.setState({dayRange:newDayRange})
  }
}
await this.setState({M_month:month},()=>{
  this.props.onDateChange(this.getValue());
});
  }
  handelYear= async (year)=>{
    var {M_month , M_year, dayRange} = this.state;
    var dayNum=0,newDayRange=0;
    if (M_year !== year) {
      dayNum = moment(`${year}-${M_month}`, 'YYYY-MM').daysInMonth();
      if (dayNum !== dayRange.length){
        newDayRange =  await this.genDateRange(dayNum);
        this.setState({dayRange:newDayRange})
      }
    }

    
    await this.setState({M_year:year},(()=>{
      this.props.onDateChange(this.getValue());
    }))
  }

  render() {
    return (
      <View style={styles.row}>
        {['date', 'datetime'].includes(this.props.mode) && this.datePicker}
        {['time', 'datetime'].includes(this.props.mode) && this.timePicker}
      </View>
    );
  }

  get datePicker() {
    const propsStyles = stylesFromProps(this.props);

    const { order } = this.props;

    if (!order.includes('D') && !order.includes('M') && !order.includes('Y')) {
      throw new Error(`WheelDatePicker: you are using order prop wrong, default value is 'D-M-Y'`);
    }

    return this.props.order.split('-').map((key) => {
      switch (key) {
        case 'D': return (
          <View key='date' style={styles.picker}>
            <Picker
              {...propsStyles}
              style={this.props.style}
              ref={(date) => { this.dateComponent = date; }}
              selectedValue={this.state.M_date}
              pickerData={this.state.dayRange}
              onValueChange={(date)=>{
                this.setState({
                  M_date:date
                },()=>{
                  this.props.onDateChange(this.getValue());
                });
                
              }}
            />
          </View>
        );
        case 'M': return (
          <View key='month' style={styles.picker}>
            <Picker
              {...propsStyles}
              style={this.props.style}
              // ref={(month) => { this.monthComponent = month; }}
              selectedValue={this.state.M_month}
              pickerData={[{ value: 1, label: 'January' },{ value: 2, label: 'February' },{ value: 3, label: 'March' },{ value: 4, label: 'April' },{ value: 5, label: 'May' },{ value: 6, label: 'June' },{ value: 7, label: 'July' },{ value: 8, label: 'August' },{ value: 9, label: 'September' },{ value: 10, label: 'October' },{ value: 11, label: 'November' },{ value: 12, label: 'December' }]}
              onValueChange={(month)=>{this.handelMonth(month)}}
            />
          </View>
        );
        case 'Y': return (
          <View key='year' style={styles.picker}>
            <Picker
              {...propsStyles}
              style={this.props.style}
              ref={(year) => { this.yearComponent = year; }}
              selectedValue={this.state.M_year}
              pickerData={this.state.yearRange}
              onValueChange={(year)=>{this.handelYear(year)    }}
            />
          </View>
        );
        default: return null;
      }
    })
  }

  get timePicker() {
    const propsStyles = stylesFromProps(this.props);

    const [hours, minutes] = [[], []];

    for (let i = 0; i <= 24; i += 1) {
      hours.push(i);
    }

    for (let i = 0; i <= 59; i += 1) {
      minutes.push(i);
    }

    return [
      <View key='hour' style={styles.picker}>
        <Picker
          ref={(hour) => { this.hourComponent = hour; }}
          {...propsStyles}
          selectedValue={this.state.date.getHours()}
          pickerData={hours}
          onValueChange={this.onHourChange}
        />
      </View>,
      <View key='minute' style={styles.picker}>
        <Picker
          ref={(minute) => { this.minuteComponent = minute; }}
          {...propsStyles}
          selectedValue={this.state.date.getMinutes()}
          pickerData={minutes}
          onValueChange={this.onMinuteChange}
        />
      </View>,
    ];
  }

  checkDate(oldYear, oldMonth) {
    const currentMonth = this.newValue.month;
    const currentYear = this.newValue.year;
    const currentDay = this.newValue.date;

    let dayRange = this.state.dayRange;
    let dayNum = dayRange.length;

    if (oldMonth !== currentMonth || oldYear !== currentYear) {
      dayNum = moment(`${currentYear}-${currentMonth + 1}`, 'YYYY-MM').daysInMonth();
    }

    if (dayNum !== dayRange.length) {
      dayRange = this.genDateRange(dayNum);

      if (currentDay > dayNum) {
        this.newValue.date = dayNum;
        this.dateComponent.setState({ selectedValue: dayNum });
      }

      this.setState({ dayRange });
    }

    const unit = this.props.mode === 'date' ? 'day' : undefined;
    const current = Object.assign({}, this.newValue, { date: this.newValue.date });
    let currentTime = moment(current);
    const min = moment(this.props.minimumDate);
    const max = moment(this.props.maximumDate);
    let isCurrentTimeChanged = false;

    if (currentTime.isBefore(min, unit)) {
      [currentTime, isCurrentTimeChanged] = [min, true];
    } else if (currentTime.isAfter(max, unit)) {
      [currentTime, isCurrentTimeChanged] = [max, true];
    }

    if (isCurrentTimeChanged) {
      if (this.monthComponent) {
        this.monthComponent.setState({ selectedValue: currentTime.get('month') + 1 });
      }

      ['year', 'date', 'hour', 'minute'].forEach((segment) => {
        const ref = this[`${segment}Component`];

        return ref && ref.setState({ selectedValue: currentTime.get(segment) });
      });
    }
  }

  getValue() {
  var tosend = new Date(this.state.M_year,this.state.M_month-1,this.state.M_date)
    return tosend
    
  }
}
