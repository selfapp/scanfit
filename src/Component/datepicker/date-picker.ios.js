import React, { PureComponent } from 'react';
// import { DatePickerIOS } from 'react-native';
import DatePickerIOS from 'react-native-datepicker';
import PropTypes from 'prop-types';

export default class DatePicker extends PureComponent {
  static propTypes = {
    date: PropTypes.instanceOf(Date).isRequired,
    maximumDate: PropTypes.instanceOf(Date),
    minimumDate: PropTypes.instanceOf(Date),
    mode: PropTypes.oneOf(['date', 'time', 'datetime']),
    onDateChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    mode: 'date',
    date: new Date(),
  };

  state = {
    date: new Date(),
  };

  onDateChange = (date) => {
    this.setState({ date });
    this.props.onDateChange(date);
  };

  componentWillMount() {
    this.setState({ date: this.props.date });
  }

  componentWillReceiveProps({ date }) {
    this.setState({ date });
  }

  render() {
    return (
      <DatePickerIOS
      {...this.props}
          mode={'date'}
          date={this.state.date}
          onDateChange={this.onDateChange}
        />
    );
  }

  getValue() {
    return this.state.date;
  }
}
