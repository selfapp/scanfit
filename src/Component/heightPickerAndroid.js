import React, { PureComponent } from 'react';
import { ColorPropType, StyleSheet, View, ViewPropTypes as RNViewPropTypes, Text } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import Picker from './picker';

const ViewPropTypes = RNViewPropTypes || View.propTypes;

const styles = StyleSheet.create({
  picker: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
});

const stylesFromProps = props => ({
  itemSpace: props.itemSpace,
  textColor: props.textColor,
  textSize: props.textSize,
  style: props.style,
});

export default class DatePicker extends PureComponent {
  static propTypes = {
    labelUnit: PropTypes.shape({
      year: PropTypes.string,
      month: PropTypes.string,
      date: PropTypes.string,
    }),
    order: PropTypes.string,
    height: PropTypes.object.isRequired,
    maximumDate: PropTypes.instanceOf(Date),
    minimumDate: PropTypes.instanceOf(Date),
    mode: PropTypes.oneOf(['date', 'time', 'datetime']),
    onHeightChange: PropTypes.func.isRequired,
    style: ViewPropTypes.style,
    textColor: ColorPropType,
    textSize: PropTypes.number,
    itemSpace: PropTypes.number,
    selectedDate: PropTypes.number,
  };

  static defaultProps = {
    labelUnit: { year: '', month: '', date: '' },
    order: 'f-i',
    mode: 'date',
    maximumDate: moment().add(10, 'years').toDate(),
    minimumDate: moment().add(-100, 'years').toDate(),
    date: new Date(),
    style: null,
    textColor: '#333',
    textSize: 20,
    itemSpace: 23,
  };

  constructor(props) {
    super(props);

    const { height, minimumDate, maximumDate, labelUnit } = props;

    this.state = { feet: 0, inch: 1 };


  }
  componentDidMount() {
// alert(this.props.height)
this.setState({
  feet:parseInt(this.props.height.feet),
  inch:parseInt(this.props.height.inch)
})
  }


  render() {
    return (
      <View style={styles.row}>
        {['date', 'datetime'].includes(this.props.mode) && this.datePicker}
      </View>
    );
  }

  get datePicker() {
    const propsStyles = stylesFromProps(this.props);

    const { order } = this.props;


    return this.props.order.split('-').map((key) => {
      switch (key) {
        case 'f': return (
          <View key='date' style={styles.picker}>
            {/* <Text style={{ textAlign: 'center', fontWeight: 'bold', padding: 10, fontSize: 15 }}>Feet</Text> */}
            <Picker
              {...propsStyles}
              style={this.props.style}
              ref={(date) => { this.dateComponent = date; }}
              selectedValue={this.state.feet}
              pickerData={[{ value: 4, label: '4' }, { value: 5, label: '5' }, { value: 6, label: '6' }, { value: 7, label: '7' }, { value: 8, label: '8' }, { value: 9, label: '9' }]}
              onValueChange={(feet) => {
                this.setState({
                  feet
                }, () => {
                  this.props.onHeightChange(this.getValue());
                });

              }}
            />
          </View>
        );
        case 'i': return (
          <View key='month' style={styles.picker}>
            {/* <Text style={{ textAlign: 'center', fontWeight: 'bold', padding: 10, fontSize: 15 }}>Inch</Text> */}
            <Picker
              {...propsStyles}
              style={this.props.style}
              // ref={(month) => { this.monthComponent = month; }}
              selectedValue={this.state.inch}
              pickerData={[{ value: 0, label: '0' }, { value: 1, label: '1' }, { value: 2, label: '2' }, { value: 3, label: '3' }, { value: 4, label: '4' }, { value: 5, label: '5' }, { value: 6, label: '6' }, { value: 7, label: '7' }, { value: 8, label: '8' }, { value: 9, label: '9' }, { value: 10, label: '10' }, { value: 11, label: '11' }]}
              onValueChange={(inch) => {
                this.setState({ inch }, () => {
                  this.props.onHeightChange(this.getValue());
                });

              }}
            />
          </View>
        );

        default: return null;
      }
    })
  }


  getValue() {
    var tosend = { feet: this.state.feet, inch: this.state.inch }
    return tosend
  }
}
