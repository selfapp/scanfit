import React, {Component} from 'react';
import { WebView } from 'react-native-webview';

export default class MyInlineWeb extends Component {
  render() {

    return (
      <WebView
      source={{uri: `http://18.219.161.44/article-details/${this.props.id}`}}
      />
    );
  }
}
