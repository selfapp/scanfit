
import React, {Component} from 'react';
import {View,AsyncStorage,Image,Dimensions,Alert} from 'react-native';
import { connect } from 'react-redux';
import { savenewDetails } from "../store/actions/user";
import firebase from 'react-native-firebase';
import { StoreToken , ChatRoomSelected} from '../store/actions/chat'
// Build a channel
const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
  .setDescription('My apps test channel');
  firebase.notifications().android.createChannel(channel);
class Splash extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
        this.init();
    }
    async init() {
      this.checkPermission();
      this.createNotificationListeners();
    }
    async componentWillMount (){
      AsyncStorage.getItem('accessToken').then((accessToken)=>{
        // this.props.myProfile();
      });
      var getUSER = await AsyncStorage.getItem('user');
      var jsonUSs = await JSON.parse(getUSER);
      var getTraining = await AsyncStorage.getItem('daysf');
      var daysf = await JSON.parse(getTraining);
      this.props.savenewDetails(jsonUSs,daysf);

        AsyncStorage.getItem('user').then((user)=>{
            let userJson = JSON.parse(user);
          if(userJson){
            this.props.navigation.navigate('TabNavigator');
            
            // this.props.navigation.navigate('CalendarSelection');

            
          }else{
            this.props.navigation.navigate('registrationNavigationOption');
          }
            
        })

        
    }

  async componentDidMount(){
     
    this.notificationListener = firebase.notifications().onNotification(async(Notification) => {
      Notification.android.setAutoCancel(true);


   await    Notification
  .android.setChannelId('test-channel')
  .android.setLargeIcon('ic_launcher')
  .android.setSmallIcon('ic_stat_ic_notification')
  .setSound('default');
  if(this.props.chat){
    if(this.props.chat.roomId === Notification.data.room_id){

    }else{
      firebase.notifications().displayNotification(Notification)
    }
  }else{
    firebase.notifications().displayNotification(Notification)
  }
     
   
      const { title, body,data } = Notification;
      if(data.sender){
        // this.showAlert(JSON.parse(data.sender).name,body);
      }else{
        // this.showAlert(title, body);
      }
      
        // Process your notification as required
    });
    }
    async createNotificationListeners() {
      // alert('crt')  
      /*
      * Triggered when a particular notification has been received in foreground
      * */
  //    this.notificationListener = firebase.notifications().onNotification((notification) => {
  //     const { title, body } = notification;
  //     this.showAlert(title, body);
  // });

    
      /*
      * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
      * */
     this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, data } = notificationOpen.notification;
      this.props.ChatRoomSelected(
        data.room_id,
        data.sender_name,
        data.sender_picture,
        this.props.navigation
    )
  });
    
      /*
      * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
      * */
      const notificationOpen = await firebase.notifications().getInitialNotification();
      if (notificationOpen) {
        var getUSER = await AsyncStorage.getItem('user');
        var jsonUSs = await JSON.parse(getUSER);
        var getTraining = await AsyncStorage.getItem('daysf');
        var daysf = await JSON.parse(getTraining);
        this.props.savenewDetails(jsonUSs,daysf);
        console.log('notificationOpen.notification',notificationOpen.notification.data)
        const { title, data } = notificationOpen.notification;
    
    if(data){
     
     if(this.props.userData){

      setTimeout(() => {
        this.props.ChatRoomSelected(
          data.room_id,
          data.sender_name,
          data.sender_picture,
  
          // this.props.userMobile === item.custom_data.sender.id ?item.custom_data.receiver.name:item.custom_data.sender.name,
          // this.props.userMobile === item.custom_data.sender.id ?item.custom_data.receiver.avatar_url :item.custom_data.sender.avatar_url,
          this.props.navigation
  
      )
      }, 200);
     }else{
      this.props.navigation.navigate('registrationNavigationOption');
     }
    }
        // this.showAlert(title, body);
    }
      /*
      * Triggered for data only payload in foreground
      * */
     this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
    });
  }
    
    showAlert(title,body) {
      Alert.alert(
        title,
        body,
        [
            { text: 'OK', onPress: () => {
              // console.log('OK Pressed')
             }},
        ],
        { cancelable: false },
      );
    }
    
  
  
    //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }
  
    //3
  async getToken() {
    AsyncStorage.getItem('fcmToken').then((fcmToken)=>{
     if(fcmToken){
      this.props.StoreToken(fcmToken)
     }
     if (!fcmToken) {
     this.gettoken()
     }
    });
  
 
    
  }
  async gettoken(){
    firebase.messaging().getToken().then(async(fcmToken2)=>{
      if (fcmToken2) {
        await AsyncStorage.setItem('fcmToken', fcmToken2);
        // alert(fcmToken2);
        this.props.StoreToken(fcmToken2)
    }
    })
    // var  fcmToken2 = await firebase.messaging().getToken();
    
  }
  
    //2
  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
    }
  }
    render(){
        return(
            <View style={{ flex: 1, alignItems:'center'}}>
                 <Image source={require('../assets/launch_screen.png')} style={{width:Dimensions.get('window').width, height:Dimensions.get('window').height,resizeMode:'cover'}}/>
             </View>
        )
    }
}
const mapStateToProps = state =>{
  return{
      userData: state.user,
      chat:state.chat
  }
};
const mapDispatchToProps = (dispatch) =>{
  return{
    savenewDetails:(data,days) => savenewDetails(data,days,dispatch),
      StoreToken:(token) => StoreToken(token,dispatch),
      ChatRoomSelected:(id,name,urpic,navigation) => ChatRoomSelected (id,name,urpic,navigation,dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Splash);