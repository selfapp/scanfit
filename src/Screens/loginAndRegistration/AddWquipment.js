

import React, {Component} from 'react';
import {View, Text, KeyboardAvoidingView, Dimensions, Keyboard, 
     Image, ScrollView, StyleSheet, TextInput} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import TitleText from '../../Component/TitleText';
import BackButton from '../../Component/BackButton';

const width = Dimensions.get("window").width;

export default class AddWquipment extends Component {

    constructor(props) {
        super(props);
        this.state={
            equipmentType:'',
            title:'',
            description:'',
            capturedImage:'',
            loader:false          
        }
    }

    componentWillMount() {
        this.setState({capturedImage:this.props.navigation.state.params.image})
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    async addEquipmentAction() {

        // if(this.state.title.length === 0){
        //     alert("Please enter title.")
        // }else if(this.state.description.length === 0) {
        //     alert("Please enter description.")
        // }else{
            this.props.navigation.navigate('EquipmentList')
        // }
    }
   
    render(){

        return(
        <View style={{flex:1}}>
             {this.state.loader ? <MyActivityIndicator /> : null}
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', 
                              height:50, backgroundColor:'#fff', zIndex:1}}>
                        <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        />
                    <TitleText
                            size = {15}
                            color = 'black'
                            weight = '400'
                            alignment = 'center'
                            title = 'Add an item'
                            />

                    <View style={{width:90}}/>
                </View>
          
            <BottomBorderView 
                horizontal={0}
                top={0}
                />
            <ScrollView style={styles.mainContainer} keyboardDismissMode='on-drag'>
                    <KeyboardAvoidingView behavior={'position'}>

                       

                        <View style={{
                                    marginTop:0, 
                                    borderRadius:10, 
                                    borderWidth:1, 
                                    borderColor:'lightgray',
                                    height:200,
                                    // backgroundColor:'red',
                                    justifyContent:'center',
                                    alignItems:'center'}}>
                            
                            <Image style={{
                                width:'100%',
                                height:'100%'
                            }} source={{uri:this.state.capturedImage}}>
                            </Image>                            
                        </View>
                        <Text style={styles.titleText}>
                            Is this your equipment?
                        </Text>
                        <TextInput style={styles.textInput}  
                                        placeholder='Populate items when type here'
                                        underlineColorAndroid='transparent'
                                        autoCorrect={false}
                                        value={this.state.equipmentType}
                                        onChangeText={(value)=>this.setState({equipmentType:value})}
                                    >
                            </TextInput>
                            {/* <Text style={styles.titleText}>
                            Description
                        </Text>
                        <TextInput style={[styles.textInput, {height:100}]} 
                                        placeholder='Enter here ...'
                                        multiline = {true}
                                        textAlignVertical = 'top'
                                        underlineColorAndroid='transparent'
                                        autoCorrect={false}
                                        value={this.state.description}
                                        onChangeText={(value)=>this.setState({description:value})}
                                    >
                        </TextInput> */}
                        <Button
                            horizontal = '8%'
                            top = {50}
                            radius = {20}
                            backgColor = '#69D3A9'
                            height = {40}
                            textColor = 'white'
                            titleSize = {16}
                            title = 'Add'
                            buttonAction = {()=>this.addEquipmentAction()}
                            />
                    </KeyboardAvoidingView>

                </ScrollView>
        </View>
        )}
            
}

const styles = StyleSheet.create({
    mainContainer:{flex:1, marginHorizontal:15, marginVertical:15},
    titleText:{marginTop:20, fontSize:16, marginLeft:20},
    textInput:{fontSize:14,
        height:40,
        marginLeft:10,
        borderRadius:5,
        borderWidth:0.7,
        borderColor:'gray',
        marginTop:5,
        padding:10}
});