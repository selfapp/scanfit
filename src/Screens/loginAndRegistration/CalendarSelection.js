import React, { Component } from 'react';
import {
  View, Text, Dimensions,
  Image, StyleSheet, AsyncStorage
} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import Moment from "moment";
import BackButton from '../../Component/BackButton';
import { Calendar } from "react-native-calendars";
import moment from 'moment';
import { ScrollView } from 'react-native-gesture-handler';

const width = Dimensions.get("window").width;
export default class CalendarSelection extends Component {

  constructor(props) {
    super(props);
    this.state = {
      arrSelectedDays: [],
      loader: false,
      currentDate: '',
      originalMarkedDates: '',
      selectedDate: undefined,
      dtex: ''
    }
  }

  componentWillMount() {
    var todayDate = Moment().toDate()
    var todayMoment = Moment(todayDate)
    const todayDateFormatted = todayMoment.format('YYYY-MM-DD')
    this.setState({
      date: todayDateFormatted
    });
  }
  componentDidMount() {
    var D_date = new Date();
    var f_date = moment().format('YYYY-MM-DD');
    var TimestampToday = Date.parse(f_date);
    this.getCurrentDateParams(TimestampToday);
  }

  async setAction() {
    if (this.state.selectedDate) {
      this.setState({ loader: true })
      const formatedDate = Moment(this.state.selectedDate).format("DD-MM-YYYY");
      let body = {
        start_date: formatedDate
      }
      try {
        let response = await api.request('/add-start-date', 'post', body);
        if (response.status === 200) {
          response.json().then(async (respons) => {
            if (respons.success) {
              let jRes = await JSON.stringify(respons.days)
              this.props.navigation.navigate('HOME');
            }
          }
          );
          this.setState({ loader: false });
        }
        else if (response.status === 401) {
          this.setState({ loader: false });
          alert('Invalid credentials');
        }
        else {
          this.setState({ loader: false });
          response.json().then((response) => {
            if (response.errors) {
            }
          })
        }
      } catch (error) {
        this.setState({ loader: false });
        alert(error.message);
      }
    }
  }

  setCalendar = (selectedDate) => {
    let markedDates = {}
    var endDateCheck = Moment(selectedDate).add(84, 'days')
    const endDateFormatted = endDateCheck.format('YYYY-MM-DD')
    let currentDate = Moment(selectedDate)
    let endDate = Moment(endDateFormatted)
    const tapDateFormatted = currentDate.format('YYYY-MM-DD')
    var daysValue = this.props.navigation.state.params.selectedDays;

    // #looping 
    while (currentDate <= endDate) {
      const day = currentDate.day()
      var isWeekend = false;

      if (daysValue.length === 1) {
        isWeekend = parseInt(daysValue[0]) === day ? true : false
      } else if (daysValue.length === 2) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day) ? true : false
      } else if (daysValue.length === 3) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day) ? true : false
      } else if (daysValue.length === 4) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day || parseInt(daysValue[3]) === day) ? true : false
      } else if (daysValue.length === 5) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day || parseInt(daysValue[3]) === day ||parseInt( daysValue[4]) === day) ? true : false
      } else if (daysValue.length === 6) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day || parseInt(daysValue[3]) === day ||parseInt( daysValue[4]) === day || parseInt(daysValue[5]) === day) ? true : false
      } else if (daysValue.length === 7) {
        isWeekend = true
      }
      const currentDateFormatted = currentDate.format('YYYY-MM-DD')
      let markup = {}
      if (tapDateFormatted === currentDateFormatted) {
        markedDates[currentDateFormatted]={
          customStyles: {
            container: {
              backgroundColor:'#e6f5ba'
            },
            // text: {
            //   color: 'black',
            // },
          },
        }
      }
    
       if (isWeekend) {
        markedDates[currentDateFormatted]={
          customStyles: {
            container: {
              backgroundColor: '#62c795'
            },
            // text: {
            //   color: 'black',
            // },
          },
        }
         markup.marked = true, markup.dotColor = '#34BF83', markup.activeOpacity = 0
       } else {
        markedDates[currentDateFormatted]={
          customStyles: {
            container: {
              backgroundColor: '#b8ea86'
            },
            // text: {
            //   color: 'black',
            // },
          },
        }
         markup.marked = true, markup.dotColor = '#ABE773', markup.activeOpacity = 0
      }
      currentDate = Moment(currentDate).add(1, 'days')
    }

   // #looping end
   var today = new Date(); 
   var todayDate = Moment(today).format('YYYY-MM-DD');
      markedDates[todayDate]={
       customStyles: {
         container: {
           backgroundColor: '#62c760'
         },
        //  text: {
        //    color: 'black',
        //  },
       },
      }
    this.setState({
      originalMarkedDates: markedDates,
    },()=>{
    })
  }

  backButtonAction() {
    this.props.navigation.goBack()
  }

  nextButtonAction() {

  }

  getCurrentDateParams(date2) {
    var offset = new Date().getTimezoneOffset();
var datex;
    var dateString = Moment(date2).format("YYYY-MM-DD");
       datex = Moment(dateString).format("YYYY-MM-DD");
       datex = Moment(dateString).add(1,'day').format("YYYY-MM-DD");
    this.setState({
      selectedDate: datex
    })
    this.setCalendar(datex)
    const date = Moment(dateString).format("D MMM YYYY");
    const dateInitail = Moment(dateString).format("DD");
    const dateMiddle = Moment(dateString).format("MMMM YYYY");
    const dateLast = Moment(dateString).format("dddd");
  }

  render() {
    return (
      <View style={{ flex: 1,paddingBottom:15}}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 50, backgroundColor: '#fff', zIndex: 1 }}>
          <BackButton buttonAction={() => this.backButtonAction()} />
        </View>

        <BottomBorderView horizontal={0} top={0} />
        {this.state.loader ? <MyActivityIndicator /> : null}

       <ScrollView>
       <Text style={styles.titleText}> When do you want to start? </Text>
        <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 10, elevation: 1, }}>
          <Calendar
             theme={{
              backgroundColor: '#ffffff',
              calendarBackground: '#ffffff',
              textSectionTitleColor: '#b6c1cd',
              selectedDayBackgroundColor: '#00adf5',
              selectedDayTextColor: '#ffffff',
              // todayTextColor: '#00adf5',
              dayTextColor: '#2d4150',
              textDisabledColor: '#bbbbbb',
              dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              arrowColor: 'orange',
              // monthTextColor: '',
              indicatorColor: 'blue',
              // textDayFontFamily: 'monospace',
              // textMonthFontFamily: 'monospace',
              // textDayHeaderFontFamily: 'monospace',
              textDayFontWeight: '300',
              textMonthFontWeight: 'bold',
              textDayHeaderFontWeight: '300',
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 16
            }}
            markingType={'custom'}
            markedDates={this.state.originalMarkedDates}
            // current={this.state.currentDateForCalender}
            minDate={this.state.date}//{"2019-08-30"}
            onDayPress={day => { this.getCurrentDateParams(day.timestamp); }}
            onDayLongPress={day => { console.log("selected day", day); }}
            monthFormat={"MMMM yyyy"}
            onMonthChange={month => { console.log("month changed", month); }}
            disableMonthChange={false}
            firstDay={1}
            onPressArrowLeft={substractMonth => substractMonth()}
            onPressArrowRight={addMonth => addMonth()}
          />
          <Button
          horizontal='8%'
          top={20}
          radius={20}
          backgColor='#69D3A9'
          height={40}
          textColor='white'
          titleSize={16}
          title='All set'
          buttonAction={() => this.setAction()}
        />
        </View>
       </ScrollView>
        
      </View>
    )
  }
}
const styles = StyleSheet.create({
  titleText: { marginTop: 20, fontSize: 16, marginHorizontal: '10%', textAlign: 'center', fontWeight: '400' },
});
