
import React, {Component} from 'react';
import {View, Text, Dimensions, 
    Image, TouchableOpacity, StyleSheet, TouchableWithoutFeedback,Platform} from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import BottomBorderView from '../../Component/BottomBorderView';
import { connect } from 'react-redux';
import { fetchMyEquipment } from "../../store/actions/user";
import api from '../../../api';
import TitleText from '../../Component/TitleText';
import BackButton from '../../Component/BackButton';
import MyActivityIndicator from '../../Component/activity_indicator';
import Button from '../../Component/Button';
import {AfterInteractions} from 'react-native-interactions';
import { withNavigationFocus } from 'react-navigation';
import FastImage from 'react-native-fast-image'
const width = Dimensions.get("window").width;

class EquipmentList extends Component {

    constructor(props) {
		super(props);
		this.state = {
            loader:false,
            arrEqip:[],
            isHome:false,
		};
    }
    componentDidUpdate(){
    }
    async componentWillMount(){
       const isHome =  await this.props.navigation.getParam('HomeView');
       console.log("ishome ",isHome)
        if(isHome){
            this.setState({isHome});
        }
        this.props.fetchMyEquipment();
        // this.fetchEquipmentList()
    }

    continueAction() {
      
       if(this.state.isHome){
        this.props.navigation.navigate('WorkoutDaySelection')
       }else{
        this.props.navigation.navigate('SideMenu')
       }
    }
    
    deleteEquipment = async(rowMap, index, item)=> {
        this.setState({
            loader: true
        })
        try{
            let body = {
                'equipment_id': item.item.equipments.id
            };
            let response = await api.request('/delete-equipment', 'delete', body);

            if (response.status == 200) {
                response.json().then((jres) => {
                    console.log(jres);
                    this.props.fetchMyEquipment();
                    this.deleteRow(rowMap, index)
                    this.setState({
                        loader: false
                    })


                })
                
                // this.props.fetchMyEquipment();

                // response.json().then((data) => {
                    
                // })
                
            }
            else {
                this.setState({loader: false});
                response.json().then((respons) => {
                    alert((respons.errors));
                })
            }
        }catch (error) {
            this.state.loader(false);
            alert(error.message);
        }
    }

    fetchEquipmentList = async()=> {

        this.setState({
            loader: true
        })
        try{

            let response = await api.request('/my-equipments', 'get', null, null);

            if (response.status == 200) {
                response.json().then((data) => {
                    this.setState({
                        arrEqip:data.records,
                        loader: false
                    })
                })
            }
            else {
                this.setState({loader: false});
                response.json().then((respons) => {
                    alert(Object.values(respons.errors));
                })
            }
        }catch (error) {
            this.state.loader(false);
            alert(error.message);
        }
    }

    addMoreAction() {
           if(this.state.isHome){
            this.props.navigation.navigate('SNEquipments',{isHome:this.state.isHome})
           }else{
            this.props.navigation.navigate('Equipments',{isHome:this.state.isHome})
           }
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    onRowDidOpen = (rowKey, rowMap) => {
		setTimeout(() => {
			this.closeRow(rowMap, rowKey);
		}, 2000);
    }

    closeRow(rowMap, rowKey) {
		if (rowMap[rowKey]) {
			rowMap[rowKey].closeRow();
		}
	}

	deleteRow(rowMap, rowKey) {
        this.closeRow(rowMap, rowKey);
        
        const newData = [...this.state.arrEqip];
        newData.splice(rowKey, 1);
        this.setState({arrEqip: newData});
	}

    _renderItem = ({item,index}) => {
    console.log(item)
        return(
            
            <View>
                 {/* <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate('WorkoutDaySelection')}> */}
                 <View>

                    <View style={{
                        backgroundColor:'white',
                        height:70,
                        marginVertical:10,
                        justifyContent:'center',
                        alignItems:'center',
                        flexDirection:'row'
                    }}>
                        <FastImage style={{
                            marginLeft:10,
                            width:60,
                            height:60,
                            borderRadius:30
                        }} 
                        resizeMode={FastImage.resizeMode.contain}
                        source={item.equipments.image ? {uri: (item.equipments.image)} : null}>
                        
                        </FastImage>
                        <View style={{marginLeft:10}}>
                            <Text>
                                {item.equipments.title}
                            </Text>
                            <Text style={{
                                fontSize:12,
                                color:'gray',
                                width:width-110
                            }}numberOfLines={3}>
                                {/* {item.Description} */}
                                {item.equipments.description ? item.equipments.description : "Lorem ipsum is simply"}
                            </Text>
                        </View>
                        {/* <Image source={require('../../assets/arrow-right.png')}>
                        </Image> */}
                    </View>
                    </View>
                    <BottomBorderView 
                    horizontal={0}
                    top={0}
                    />
            </View>
        )
    }
    
        render() {
            const{isFocused}=this.props.navigation
            const shouldForceOffscreen = (Platform.OS === 'android') && !isFocused;
            return (
                <AfterInteractions placeholder={<MyActivityIndicator />}>
                <View style={{flex:1}}>
                {this.state.loader ? <MyActivityIndicator /> : null}
                       {
                           this.state.isHome ? 
                           <View style={{flexDirection:'row',height:50, backgroundColor:'#fff'}}>
                           <View style={{width:'100%'}}> 
                           
                           <TitleText
                                                           size = {15}
                                                           color = 'black'
                                                           weight = '400'
                                                           width = {width-135}
                                                           alignment = 'center'
                                                           height = {50}
                                                           title = 'Equipment list'
                                                           top = {30}
                                                   />
                                                   <View style={{
                                                       position:'absolute',
                                                       right:0,
                                                       height:50,
                                                       alignItems:'center',
                                                       justifyContent:'flex-end'
                                                       
                                                   }}>
                                                       <TouchableOpacity style={{
                                                           backgroundColor:'#69D3A9',
                                                           height:30,
                                                           borderRadius:15,
                                                           right:5,
                                                           width:100,
                                                           alignItems:'center',
                                                           justifyContent:'center',
                                                           flexDirection:'row',
                                                       }} onPress={()=> this.addMoreAction()}>
                                                           <Text style={{
                                                               color:'white',
                                                               fontSize:12
                                                           }}>
                                                               Add more
                                                           </Text>
                                                           <Image style={{
                                                               marginLeft:2
                                                           }} source={require('../../assets/add-more.png')}>
                                                           </Image>
                                                       </TouchableOpacity>
                                                   </View>
                           
                           </View>
                           
                                                   </View> 
                                                   :
                                                    <TitleText
                                                    size = {15}
                                                    color = 'black'
                                                    weight = '400'
                                                    width = {width-135}
                                                    alignment = 'center'
                                                    height = {50}
                                                    title = 'Equipment list'
                                                    top = {30}
                                            />

                       }
                        

                        <BottomBorderView 
                        horizontal={0}
                        top={10}
                        />


                        { (! this.state.isHome) && <View style={{
                            // height:40,
                            alignItems:'flex-end',
                            marginVertical:10
                        }}>
                            <TouchableOpacity style={{
                                backgroundColor:'#69D3A9',
                                height:30,
                                borderRadius:15,
                                right:40,
                                width:100,
                                alignItems:'center',
                                justifyContent:'center',
                                flexDirection:'row'
                            }} onPress={()=> this.addMoreAction()}>
                                <Text style={{
                                    color:'white',
                                    fontSize:12
                                }}>
                                    Add more
                                </Text>
                                <Image style={{
                                    marginLeft:2
                                }} source={require('../../assets/add-more.png')}>
                                </Image>
                            </TouchableOpacity>
                        </View> }


                        <BottomBorderView 
                        horizontal={0}
                        top={0}
                        />
                        <View
                         
                        ></View>
                        <SwipeListView
                        useFlatList
                        data={this.props.equipmetList}
                        renderItem = {this._renderItem} 
                        // keyExtractor={(item) => item.id}
                        stopLeftSwipe = {-1}               
                
                        renderHiddenItem={ (data, rowMap) => (
                        
                            <View style={styles.rowBack}
                            //  needsOffscreenAlphaCompositing={shouldForceOffscreen}
                            // style={shouldForceOffscreen && styles.forceComposition}
                            >
                                    <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]} 
                                                onPress={ _ => this.deleteEquipment(rowMap, data.index, data) }>
                                                    {/* // onPress={ _ => this.deleteRow(rowMap, data.index) }> */}
                                    <View style={{flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                                        <Text style={styles.backTextWhite}>Delete  </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )}
                        rightOpenValue={-90}
                        previewRowKey={'0'}
                        previewOpenValue={-40}
                        previewOpenDelay={3000}
                        onRowDidOpen={this.onRowDidOpen}
                    />
                    
                    
                        <Button
                        horizontal = '8%'
                        top = {20}
                        bottom = {20}
                        radius = {20}
                        backgColor = '#69D3A9'
                        height = {40}
                        weight = 'bold'
                        textColor = 'white'
                        titleSize = {16}
                        title = 'Continue'
                        buttonAction = {()=>this.continueAction()}
                        />
                        
                    
                 </View>
            </AfterInteractions>
            )
        }
}

const mapStateToProps = state =>{
    return{
        equipmetList: state.user.equipmetList
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        fetchMyEquipment:() => fetchMyEquipment(dispatch)
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(EquipmentList));

const styles = StyleSheet.create({
    backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 90
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        height:70,
        marginVertical:10,
        right: 0
    },
    backTextWhite: {
        color: 'white',
        fontWeight:'bold'
    },
    rowBack: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
        paddingLeft: 15
    },
    forceComposition: {
        opacity: 0.99,
      },
});