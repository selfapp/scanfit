
import React, { Component } from 'react';
import {
    View, Text, Dimensions, Keyboard,TouchableWithoutFeedback,
    TouchableOpacity, Image, StyleSheet, FlatList, TextInput, Modal
} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { fetchMyEquipment } from "../../store/actions/user";
import { withNavigationFocus } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ScanCamera from '../../Component/ScanCamera'
import FastImage from 'react-native-fast-image'

const width = Dimensions.get("window").width;
class Equipments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isHome: false,
            loader: false,
            enableScrollViewScroll: true,
            selectedItems: [],
            openList:true,
            CameraOpenSelection:false,

            allEquipment:[],
            searchedEquipment:[],
            text:'',
            modalVisible: false,
            byImageSearchRes:'',
            selectOption: false
        }
        this.arrEqipInitial = [];
        this.takeImage.bind(this);
    }

    async componentWillMount() {
        this.props.fetchMyEquipment();
        this.fetchEquipmentList()
        const isHome = await this.props.navigation.getParam('HomeView');
        console.log(isHome,'-----------------------')
        if (isHome) {
            this.setState({ isHome:true }, console.log("home", this.state.isHome));
        }
    }
    
    refreshDataAction() {
        this.fetchEquipmentList()
    }
// By Dk
    setPreSelectedEquipment(){
        this.props.myEquipmetList.map(myItem=>{
                for(i=0; i<this.state.allEquipment.length; i++){
                    if(myItem.equipment_id === this.state.allEquipment[i].id){
                        this.selectItem(this.state.allEquipment[i])
                        break;
                    }
                }
        })
    }

    fetchEquipmentList = async () => {
        this.props.fetchMyEquipment();
        this.setState({
            loader: true
        })
        try {
            let response = await api.request('/equipment-list', 'get', null, null);
            if (response.status == 200) {
                response.json().then((data) => {
                    this.setState({ loader: false });
                      console.log("equipment list data response ......", data)
                        this.setState({ allEquipment: data.records, searchedEquipment: data.records, loader: false})
                        this.setPreSelectedEquipment()
                })
            }
            else {
                this.setState({ loader: false });
                response.json().then((respons) => {
                    alert(Object.values(respons.errors));
                })
            }
        } catch (error) {
            this.state.loader(false);
            alert(error.message);
        }
    }
    added = async (toEdit) => {
        console.log('PPOPOPOPOOPOPOPOPOPOPOPPOPOPOPOPOPO')
        return { ...toEdit, added: true };
    }

    searchImage = async (imageData) => {
        this.setState({ loader: true });
        let body = {
            "image": imageData//"data:image/jpeg;base64"
        };
        
        try {
             console.log('Search image body  ', JSON.stringify(body));
            let response = await api.request('/image-recognition', 'post', body);
            console.log(response)
            if (response.status === 200) {
                response.json().then((data) => {
                        this.setState({
                            loader: false
                        })
                        if(data.records.length > 0){
                                this.setState({ byImageSearchRes: data.records[0], modalVisible: true})
                        }
                });
            }
            else if (response.status === 401) {
                this.setState({ loader: false });
            }
            else if (response.status === 422) {
                this.setState({ loader: false });
                alert("Can not process this image try others")
            }
            else {
                this.setState({ loader: false });
                response.json().then((response) => {
                    if (response.errors) { alert(JSON.stringify(response.errors)); }
                })
            }
        } catch (error) {
            this.setState({ loader: false });
        }
    }

    renderModalContent() {
        return(
            <TouchableWithoutFeedback
                onPress={()=>Keyboard.dismiss()}
            >
                { !this.state.selectOption ? (
                    <View style={{ width: width-50, padding:10,borderRadius:10, justifyContent: 'center', backgroundColor:'#fff'}}>
                    <Text style={{fontSize: 20, fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>{this.state.byImageSearchRes.title}</Text>
                <View>
                    <View style={{ marginVertical:20}}>
                        <Text style={{textAlign:'center', marginVertical:10, marginHorizontal: 20, color:'gray'}}>{`    Is this your equipment?      `}</Text>
                    </View>
                   </View>
                   <View style={{ borderStyle: 'dashed', borderWidth: .5, borderColor:'#E8E8E8' }}/>
                    <View style={{ flexDirection:'row', height:40}}>
                        <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                             onPress={()=>this.setState({selectOption:true})}
                        >
                            <Text style={{color:'red', fontWeight:'600', fontSize: 20 }}>No</Text>
                        </TouchableOpacity>
                        <View style={{width:1, backgroundColor:'#E8E8E8'}}/>
                        <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                            onPress={()=>{
                                this.setState({modalVisible:false, openList: true}),
                                this.selectItem(this.state.byImageSearchRes)
                            }}
                        >
                            <Text style={{color:'#69D3A9', fontWeight:'600', fontSize: 20}}>Yes</Text>
                        </TouchableOpacity>
                    </View>
            </View>
                ):(
                    <View style={{ width: width-50, padding:10,borderRadius:10, justifyContent: 'center', backgroundColor:'#fff'}}>
                <View>
                    <View style={{ marginVertical:20}}>
                        <Text style={{textAlign:'center', marginVertical:10, marginHorizontal: 20, color:'gray'}}>{`    Please select option      `}</Text>
                    </View>
                   </View>
                   <View style={{ borderStyle: 'dashed', borderWidth: .5, borderColor:'#E8E8E8' }}/>
                    <View style={{ flexDirection:'row', height:40}}>
                        <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                             onPress={()=>this.setState({modalVisible:false, selectOption: false, CameraOpenSelection: true})}
                        >
                            <Text style={{color:'red', fontWeight:'600', fontSize: 18 }}>Rescan</Text>
                        </TouchableOpacity>
                        <View style={{width:1, backgroundColor:'#E8E8E8'}}/>
                        <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                            onPress={()=>{
                                this.setState({modalVisible:false, selectOption: false, openList: true})
                            }}
                        >
                            <Text style={{color:'#69D3A9', fontWeight:'600', fontSize: 18}}>Add manually</Text>
                        </TouchableOpacity>
                    </View>
            </View>
                )}
            </TouchableWithoutFeedback>
        )
    }


    showActionSheet() {
        this.ActionSheet.show()
    }

    onEnableScroll(value) {
        this.setState({
            enableScrollViewScroll: value,
        });
    };


    addEquipmentAction = async () => {

        let ids=[];
        this.state.allEquipment.map(item=>{
            if(item.is_selected){
                ids.push(item.id)
            }
        })
        this.setState({ loader: true });
        let body = {
            'equipment_ids': ids
        };
         console.log('add equipment Body', JSON.stringify(body))
        try {
            let response = await api.request('/add-equipment', 'post', body);
            if (response.status === 200) {
                response.json().then((respons) => {
                });
                // console.log('addREsponse ', response)
                this.props.fetchMyEquipment();
                // { !this.state.isHome ? 
                    // this.props.navigation.goBack():
                     this.props.navigation.navigate('EquipmentList', { HomeView: this.state.isHome})
                // }

            }
            else {
                this.setState({ loader: false });
                response.json().then((response) => {
                    if (response.errors) {
                        alert(response.errors);
                    }
                    if (response.message) {
                        alert(response.message)
                    }
                })
            }
        } catch (error) {
            this.setState({ loader: false });
            alert(error.message);
        }
    }

    takeImageAction(index) {

        if (index === 0) {
            ImagePicker.openCamera({
                compressImageQuality:0.4,
                cropping: false,
                includeBase64: true,
                mediaType: 'photo'
            }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                this.searchImage(data)
            });
        } else if (index === 1) {
            ImagePicker.openPicker({
                cropping: false,
                includeBase64: true,
                mediaType: 'photo'
            }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                this.searchImage(data)
            });
        }

    }

Arrow(){
    this.setState({CameraOpenSelection:false})
    }

    takeImage(data){
        // console.log(data,"muhe mil gaya")
       this.setState({
           CameraOpenSelection:false
       })
       this.searchImage(data)
        }

  //  Search function by DK
        SearchFilterFunctionbytext(text) {
            const newData = this.state.allEquipment.filter(function(item) {
              const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
              const textData = text.toUpperCase();
              return itemData.indexOf(textData) > -1;
            });
            this.setState({
              searchedEquipment: newData,
              text: text,
            });
          }

// Select item by DK
        selectItem = (item1) => {
            item1.is_selected = !item1.is_selected;
            const index = this.state.allEquipment.findIndex(
              (item,index) => {
                 if( item.id === item1.id)return index
                }
            );
            console.log(index);
            this.state.allEquipment[index] = item1;
            this.setState({
              allEquipment: this.state.allEquipment,
            });
            this.SearchFilterFunctionbytext(this.state.text)
          };


    render() {
        return (
            <View style={{flex:1,}}>
            {
                (this.state.CameraOpenSelection) ?
                <ScanCamera
                Arrow={this.Arrow.bind(this)}
                isToggle={false}
                OnClickPickture={this.takeImage.bind(this)}
                />
                :
                <View style={{ flex: 1 , marginBottom:10}}>
                {this.state.loader ? <MyActivityIndicator /> : null}
              
                <View style={{ flexDirection: 'row', height: 50, backgroundColor: '#fff' }}>
                    <View style={{ width: '100%' }}>
                        <TitleText
                            size={15}
                            color='black'
                            weight='400'
                            width={width - 135}
                            alignment='center'
                            height={50}
                            title='Select Multiple Equipments'
                            top={30}
                        />
                        <View style={{
                            position: 'absolute',
                            right: 8,
                            height: 50,
                            alignItems: 'center',
                            justifyContent: 'flex-end'

                        }}>
                            <TouchableOpacity style={{
                                backgroundColor: 'white',
                                height: 27,

                                alignItems: 'center',
                                justifyContent: 'center',
                                flexDirection: 'row',
                            }} onPress={() => this.refreshDataAction()}>
                                <Icon name='refresh' size={30} color='#69D5A9'></Icon>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>

                <BottomBorderView
                    horizontal={0}
                    top={10}
                />

                <KeyboardAwareScrollView extraHeight={135} enabledOnAndroid={true} extraScrollHeight={70} style={styles.mainContainer} 
                automaticallyAdjustContentInsets={true}
                enableOnAndroid={true} 
                keyboardShouldPersistTaps='handled'
                //  scrollEnabled={this.state.enableScrollViewScroll}
                    // keyboardDismissMode='on-drag'
                     scrollEnabled={true} >

                    <ActionSheet
                        ref={o => this.ActionSheet = o}
                        title={'Choose option for image'}
                        options={['Camera', 'Gallery', 'Cancel']}
                        cancelButtonIndex={2}
                        destructiveButtonIndex={2}
                        onPress={(index) => { this.takeImageAction(index) }}
                    />


                    <Text style={{
                        fontSize: 15,
                        marginHorizontal: 30,
                        marginTop: 20,
                        fontWeight: '400',
                        // color:'#6C6C6C',
                        fontFamily: 'Montserrat-Thin'
                    }}>
                        What equipments do you have?
                </Text>

                    <View style={{
                        marginTop: 20,
                        borderRadius: 10,
                        borderWidth: 1,
                        borderColor: 'lightgray',
                        height: 120,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text>
                            Tap for image
                        </Text>
                        <TouchableOpacity style={{
                            marginTop: 10,
                        }} onPress={() =>
                        this.setState({
                            CameraOpenSelection:true
                        })
                        // this.showActionSheet()
                        
                        }>
                            <Image source={require('../../assets/camera-picture.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ marginVertical: 20, width: '100%', textAlign: 'center' }}>
                        OR
                </Text>
                    <View style={{
                        paddingTop: 10,
                        borderRadius: 10,
                        borderWidth: 1,
                        borderColor: 'lightgray',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    >
                        <Text>
                            Add your equipments
                        </Text>
                        <View style={{
                            flexDirection: 'row',
                            marginTop: 5,
                            width: '95%',
                            flex: 1,
                        }}>
                            <View style={{ flex: 1 }}>
                                <View style={{ height: 40, flexDirection: 'row', alignItems:'center', margin: 10, paddingHorizontal: 5, borderWidth:.5, borderRadius: 5}}>
                                    <View style={{ width:'85%' ,flexDirection: 'row', alignItems:'center',}}>
                                    <Image
                                        source={require('../../assets/search.png')}
                                        style={{ height: 20, width: 20, tintColor: 'gray'}}
                                    />
                                <TextInput
                                    style={{ paddingLeft: 5}}
                                     onChangeText={text => this.SearchFilterFunctionbytext(text)}
                                    value={this.state.text}
                                    placeholder={'Search Equipments...'}
                                    />
                                    </View>
                                    <TouchableOpacity style={{ height: 40, width: 40, right:1, alignItems:'flex-end', justifyContent:'center', }}
                                        onPress={()=> this.setState({ openList: !this.state.openList})}
                                    >
                                        <Image
                                            source={require('../../assets/arrowList.png')}
                                            style={{ height: 20, width: 20, tintColor: 'gray', transform: [{ rotate: this.state.openList ? '90deg' : '0deg'}]}}
                                            resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1}}>
                                <FlatList
                                    data = { this.state.openList ? this.state.searchedEquipment : []}
                                    numColumns={3}
                                    keyExtractor = {(item, index) => index.toString()}
                                    style={{ flex: 1 }}
                                    renderItem = {(item)=>{
                                        return(
                                            <View style={{ padding: 5 }}>
                                            <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', borderRadius: 5, height: 100,width: (width-80)/3, borderWidth:1, borderColor: item.item.is_selected ? '#69D3A9' : 'gray', }}
                                                onPress={()=>{this.selectItem(item.item)}}
                                            >
                                                    <View style={{ flex: 1, alignItems:'center', justifyContent:'center',padding: 2 }}>
                                                    <FastImage
                                                        source={ (item.item.image !== null) ? {uri: item.item.image} : require('../../assets/Male.png')}
                                                        style={{ flex: 1, height: 100, width: 100, borderRadius: 50 }}
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </View>
                                                    <View style={{ flex: .8, justifyContent:'center', alignItems:'center', paddingHorizontal: 2  }}>
                                                    <Text ellipsizeMode='tail' numberOfLines={2} style={{ textAlign:'center'}}>{item.item.title}</Text>
                                                    </View>
                                                    {item.item.is_selected ? (<View style={{ position: 'absolute', height: 20, width: 20, top: 0, right: 2}}>
                                                        <Image 
                                                            source={require('../../assets/tick.png')}
                                                            style={{ height: 20, width: 20 }}
                                                            resizeMode={'contain'}
                                                        />
                                                    </View>) :(null)
                                                    }
                                                    
                                            </TouchableOpacity>
                                            </View>
                                        )
                                    }}
                                 />
                            </View>
                            </View>
                        </View>
                        <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.setState({modalVisible: false, selectOption: false})
                            }}>
                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.67)', padding:15 }}>
                                    <TouchableWithoutFeedback>
                                        {this.renderModalContent()}
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>

                </KeyboardAwareScrollView>
                {
                    !this.state.isHome ?
                        <Button
                            horizontal='8%'
                            top={5}
                            radius={20}
                            backgColor='#69D3A9'
                            height={40}
                            weight='bold'
                            textColor='white'
                            titleSize={16}
                            title='Continue'
                            buttonAction={() => this.addEquipmentAction()}
                        />
                        :
                        <Button
                            horizontal='8%'
                            top={5}
                            radius={20}
                            backgColor='#69D3A9'
                            height={40}
                            weight='bold'
                            textColor='white'
                            titleSize={16}
                            title='Continue'
                            buttonAction={() => {
                                this.addEquipmentAction()
                                
                            }}
                        />
                }
            </View>
       
            }
            </View>
        )
    }

}
const mapStateToProps = state => {
    return {
        myEquipmetList: state.user.equipmetList,
        selectedItems_Equipments: state.user.selectedItems_Equipments,
        noEquipStatus:state.user.HAVENOEQUIP,
        loading: state.user.loading,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchMyEquipment: () => fetchMyEquipment(dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Equipments));
const styles = StyleSheet.create({
    mainContainer: { flex: 1, marginHorizontal: 15, marginVertical: 15 },
});

