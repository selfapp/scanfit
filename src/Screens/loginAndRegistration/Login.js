
import React, {Component} from 'react';
import {View, Text, KeyboardAvoidingView, Dimensions, Keyboard} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import TextField from '../../Component/TextField';
import Button from '../../Component/Button';
import BackButton from '../../Component/BackButton';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';

const width = Dimensions.get("window").width;

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state={
            phoneNo:'',
            loader:false
        }
    }

    async loginAction() {
        Keyboard.dismiss();
        if(this.state.phoneNo.length === 10){
            this.setState({loader: true});
            let body = {
                    'country_code':"+1",
                    'mobile':this.state.phoneNo
            };
            console.log("body is ")
            console.log(body)
    
            try {
                console.log(body);
                let response = await api.request('/resend-verification-code', 'post', body);
    
                if (response.status === 200) {
                    console.log("enter into 200 .,....")
    
                    console.log("response for otp")
                    console.log(response)
                    response.json().then((respons) => {
                       
                    });
                    alert("Access code is send to your mobile number.")
                    this.setState({loader: false});
                    this.props.navigation.navigate('Otp',{phoneNum:this.state.phoneNo})
                }
                else if (response.status === 401) {
                    console.log("enter into 401 .,....")
    
                    this.setState({loader: false});
                    alert('Invalid credentials');
                }
                else {
                    console.log("enter into else .,....")
    
                    this.setState({loader: false});
    
                    response.json().then((response) => {
                        console.log("sjkdsdfh")
                        alert(JSON.stringify(response));
                    })
                }
            } catch (error) {
                console.log("enter into error .,....")
                this.setState({loader: false});
                console.log("qwqwqwq")
    
                alert(error.message);
            }
        }else{
            alert("Please enter correct phone number.")
        }
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    render(){

        return(
        <View style={{flex:1}}>
             {this.state.loader ? <MyActivityIndicator /> : null}
            <KeyboardAvoidingView behavior={'position'}>
            <View style={{flexDirection:'row', alignItems:'center', 
                              height:50, marginTop:10}}>
                        <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        />
            </View>
            <TitleText
                        size = {15}
                        color = 'black'
                        weight = '400'
                        width = {width-135}
                        alignment = 'center'
                        height = {50}
                        title = 'Login'
                        top = {-50}
                        />
           
            <BottomBorderView 
                horizontal={0}
                top={10}
                />
               
             <Text style={{
                 fontSize:17,
                 color:'black',
                 fontWeight:'200',
                 marginLeft:30,
                 marginTop:50
             }}>Phone Number
                </Text>

                <TextField 
                    size = {15}
                    weight = '200'
                    horizontal = "10%"
                    image = {require('../../assets/phone.png')}
                    placeholderText = "Enter your phone number"
                    value = {this.state.fName}
                    length={10}
                    onChangeText = {(phoneNo)=> this.setState({phoneNo})}
                    keyboard = 'phone-pad'
               />
                <BottomBorderView 
                horizontal= '8%'
                top={5}
                />
                <Text style={{
                    fontSize:12,
                    marginHorizontal:30,
                    marginTop:20,
                    fontWeight:'400',
                    color:'#6C6C6C',
                    fontFamily:'Montserrat-Thin'
                }}>
                    A 4 digit passcode will be sent via text to verify your mobile number.
                </Text>

                {/* <Button
                horizontal = '18%'
                top = {30}
                radius = {0}
                backgColor = 'white'
                height = {50}
                weight = '300'
                textColor = 'black'
                titleSize = {18}
                title = 'New user register here?'
                buttonAction = {()=>this.goToRegistration()}
                /> */}
            
            <Button
                horizontal = '8%'
                top = {50}
                radius = {20}
                backgColor = '#69D3A9'
                height = {40}
                weight = 'bold'
                textColor = 'white'
                titleSize = {16}
                title = 'Continue'
                buttonAction = {()=>this.loginAction()}
                />
                </KeyboardAvoidingView>
             
        </View>
        )
        
    }
}