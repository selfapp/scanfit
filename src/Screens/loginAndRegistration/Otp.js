
import React, {Component} from 'react';
import {View, Text, Dimensions, Image, KeyboardAvoidingView, 
    Keyboard,
    TouchableWithoutFeedback,AsyncStorage} from 'react-native';
import OTPInput from 'react-native-otp';
import TitleText from '../../Component/TitleText';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import { connect } from 'react-redux';
import { updateProfile } from "../../store/actions/user";
import { saveDeviceToken } from "../../store/actions/chat";
const width = Dimensions.get("window").width;

class Otp extends Component {

    constructor(props){
        super(props);

        this.state={
            otp:'',
            loader:false
        }
    }

    backButtonAction() {
        console.log("back button called .......")
        this.props.navigation.goBack()
    }

    async resendCode() {

        this.setState({loader: true});
        let body = {
                'country_code':"+1",
                'mobile':this.props.navigation.state.params.phoneNum
        };
        console.log("body is ")
        console.log(body)

        try {
            console.log(body);
            let response = await api.request('/resend-verification-code', 'post', body);

            if (response.status === 200) {
                console.log("enter into 200 .,....")

                console.log("response for otp")
                console.log(response)
              
                response.json().then((respons) => {
                    Keyboard.dismiss();
                });
                alert("Access code has been sent to your mobile.")
                this.setState({loader: false});
            }
            else if (response.status === 401) {
                console.log("enter into 401 .,....")

                this.setState({loader: false});
                alert('Invalid credentials');
            }
            else {
                console.log("enter into else .,....")

                this.setState({loader: false});

                response.json().then((response) => {
                    console.log("sjkdsdfh")
                    alert(JSON.stringify(response));
                })
            }
        } catch (error) {
            console.log("enter into error .,....")
            this.setState({loader: false});
            console.log("qwqwqwq")

            alert(error.message);
        }
    }

    async goToRegistration() {

    if(this.state.otp.length === 4){

        this.setState({loader: true});
        let body = {
                'country_code':"+1",
                'mobile':this.props.navigation.state.params.phoneNum,
                'verification_code':this.state.otp
        };
        console.log("body is ")
        console.log(body)

        try {
            console.log(body);
            let response = await api.request('/verify-code', 'post', body);
            console.log("response for otp")
            console.log(response)
            if (response.status === 200) {
                console.log("enter into 200 .,....")

                
                response.json().then(async(respons) => {
                    console.log("final response ,,,,,,")
                    console.log(respons)
                    console.log(respons.access_token)
                    await AsyncStorage.setItem('accesstoken',respons.access_token)
                    Keyboard.dismiss();
                    if(respons.user.first_name !== null){
                         this.props.updateProfile(false,respons.user);
                         console.log()
                         this.props.saveDeviceToken(this.props.fcmToken);
                        this.props.navigation.navigate('Equipments',{HomeView:true})
                    }else{
                        this.props.navigation.navigate('Registration',{mobileNum:this.props.navigation.state.params.phoneNum,
                            otp:this.state.otp})
                    }
                   
                });
                this.setState({loader: false});
              
            }
            else if (response.status === 401) {
                console.log("enter into 401 .,....")

                this.setState({loader: false});
                alert('Invalid credentials');
            }
            else {
                console.log("enter into else .,....")

                this.setState({loader: false});
                response.json().then((response) => {
                    console.log("sjkdsdfh")
                    console.log("Error message")
console.log(response)
                    alert(response.error_message);
                })
            }
        } catch (error) {
            console.log("enter into error .,....")
            this.setState({loader: false});
            console.log("qwqwqwq")

            alert(error.message);
        }
    }else{
        alert("Please enter access code.")
    }
    //        this.props.navigation.navigate('Registration')

        // this.props.navigation.navigate('registrationNavigationOption')
    }

    render() {

        return(
            <View style={{flex:1}}>
                <TouchableWithoutFeedback
                onPress={()=> Keyboard.dismiss()}
                >
                <KeyboardAvoidingView behavior={'position'}>

                <View style={{flexDirection:'row', alignItems:'center', 
                              height:50, marginTop:10}}>
                        <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        />

                </View>
              
                <TitleText
                        size = {15}
                        color = 'black'
                        weight = '400'
                        width = {width-135}
                        alignment = 'center'
                        height = {50}
                        title = 'Access code'
                        top = {-50}
                        />
                <BottomBorderView 
                    horizontal={0}
                    top={0}
                />
                {this.state.loader ? <MyActivityIndicator /> : null}
                <View style={{alignItems:'center'}}>
                    <Image style={{width:141, height:141, marginTop:20}} source={require('../../assets/login_icon.png')}>
                    </Image>
                </View>
                <Text style={{
                    marginHorizontal:30,
                    marginTop:15,
                    color:'#6C6C6C',
                    fontSize:13,
                    textAlign:'center',
                    // fontWeight:'300',
                    fontFamily: 'Montserrat-Regular'
                }}>We have sent you an access code via TEXT for Mobile number verification.
                </Text>
                <View style={{marginTop: "3%"}}>

                    <OTPInput
                    value={this.state.otp}
                    onChange={value => this.setState({otp: value})}
                    tintColor='gray'
                    offTintColor='gray'
                    otpLength={4}
                    />
                </View>
                <Button
                    horizontal = '8%'
                    top = {30}
                    radius = {20}
                    backgColor = '#69D3A9'
                    height = {40}
                    weight = 'bold'
                    textColor = 'white'
                    titleSize = {16}
                    title = 'Next'
                    buttonAction = {()=>this.goToRegistration()}
                />

                <BottomBorderView 
                horizontal='10%'
                top={40}
                />

                <Button
                    horizontal = '8%'
                    top = {10}
                    radius = {0}
                    backgColor = 'white'
                    height = {50}
                    weight = '300'
                    textColor = '#6C6C6C'
                    titleSize = {18}
                    title = 'Resend code'
                    buttonAction = {()=> this.resendCode()}
                    />
                     </KeyboardAvoidingView>
                     </TouchableWithoutFeedback>
            </View>
        )
    }
}
const mapStateToProps = state =>{
    return{
        userData: state.user.personal,
        fcmToken: state.chat.fcmToken
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
        saveDeviceToken:(token) => saveDeviceToken(token,dispatch)
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(Otp);