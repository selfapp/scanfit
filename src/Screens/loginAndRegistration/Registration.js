
import React, {Component} from 'react';
import {
    View, 
    Dimensions, 
    Image, 
    Text, 
    AsyncStorage, 
    KeyboardAvoidingView, 
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native';
import TitleText from '../../Component/TitleText';
import BottomBorderView from '../../Component/BottomBorderView';
import TextField from '../../Component/TextField';
import Button from '../../Component/Button';
import BackButton from '../../Component/BackButton';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import { connect } from 'react-redux';
import {updateProfile } from "../../store/actions/user";
const {height, width} = Dimensions.get("window");

class Registration extends Component {

    constructor(props) {
        super(props);
        this.state={
            fName:'',
            lName:'',
            password:'',
            email:'',
            phoneNo:'',
            age:'',
            height:'',
            weight:'',
            currentLifestyle:'',
            currentExp:'',
            gender:'',
            whereAreU:'',
            whatYourGoal:'',
            loader:false
        }
    }

    componentDidMount() {
        this.fetchAllValues()
    }

    fetchAllValues(){

        this.setState({
            phoneNo:this.props.navigation.state.params.mobileNum
        })
        AsyncStorage.getItem('tellus').then((data)=> {

        console.log("tellus data is", data)
        if(data !== null){
          const item = JSON.parse(data);

        //   console.log("item is", item)
        //   console.log("height is", item[0].height)
        //   console.log("weight is", item[1].weight)
        //   console.log("age is", item[2].age)
        //   console.log("lifestyle is", item[3].lifestyle)
        //   console.log("experience is", item[4].experience)
          this.setState({
            height:item[0].height,
            weight:item[1].weight,
            age:item[2].age,
            currentLifestyle:item[3].lifestyle,
            currentExp:item[4].experience
          })
        }
    })

    AsyncStorage.getItem('identity').then((data)=> {

        console.log("Identity data is", data)
        if(data !== null){
           this.setState({gender:data})
        }
    })

    AsyncStorage.getItem('whereru').then((data)=>{

        console.log("selected where are you ", data)
        if(data !== null){
            let jsonValue = JSON.parse(data);
            console.log("selected where are you 111111", jsonValue)
            console.log(jsonValue.id)
            this.setState({whereAreU:jsonValue.id})
        }
    })

    AsyncStorage.getItem('endgoal').then((data)=>{

        console.log("selected endgoal are ", data)
        if(data !== null){
            let jsonValue = JSON.parse(data);
            console.log("selected where are you 111111", jsonValue)
            console.log(jsonValue.id)
            this.setState({whatYourGoal:jsonValue.id})
        }
    })

    }

   async registrationAction() {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    // this.props.navigation.navigate('Equipments')
       console.log("registration button called .......")

    if(this.state.fName.length === 0){
        alert("Please enter your first name.")
    }else if(this.state.lName.length === 0){
        alert("Please enter your last name.")
    }else if(this.state.email.length === 0 || reg.test(this.state.email) === false){
        alert("Please enter a valid email address.")
    }else{
        this.setState({loader: true});
        let body = {
            'name':this.state.fName + " " +this.state.lName,
            'email':this.state.email,
            'new_user':true,
            // 'password':this.state.password,
            // 'password_confirmation':this.state.password,
            'first_name':this.state.fName,
            'last_name':this.state.lName,
            'age':this.state.age,
            'height':this.state.height,
            'weight':this.state.weight,
            'current_lifestyle':this.state.currentLifestyle,
            'current_experience':this.state.currentExp,
            'gender':this.state.gender,
            'whereareyouat_id':this.state.whereAreU,
            'whatyourendgoal_id':this.state.whatYourGoal,
            'country_code':"+1",
            'mobile':this.state.phoneNo,
        };
        console.log("body is ")
        console.log(body)
        
        try {
            console.log(body);
            let response = await api.request('/update-profile', 'post', body);
            console.log(response)
            if (response.status === 200) {
                console.log("enter into 200 .,....")

                console.log("response for registration ...")
                
                console.log(response)
                response.json().then(async(respons) => {
                  if(respons.success){
                    console.log("final registration response ,,,,,")
                    console.log(respons)
                    // this.props.saveuser(respons.user);
                    this.props.updateProfile(false,respons.user);
                        console.log("before try----------")
                      try {
                          console.log("try----------")
                      } catch (error) {
                          console.log(error,"error")
                      }
                    Keyboard.dismiss();
                  }else{
                    alert(respons.errors)
                  }
                   
                });
                this.setState({loader: false});
                this.props.navigation.navigate('Equipments',{HomeView:true})
            }
            else if (response.status === 401) {
                console.log("enter into 401 .,....")

                this.setState({loader: false});
                alert('Invalid credentials');
            } else if(response.status === 422){
                this.setState({loader: false});
                response.json().then(async(respons) => {
                    alert(respons.errors)
                })
            }
            else {
                console.log("enter into else .,....")

                this.setState({loader: false});
                response.json().then((response) => {
                    console.log("sjkdsdfh")
                    console.log(response)
                    if(response.errors){

                        if(response.errors.mobile){
                            alert(response.errors.mobile);
                        }
                    }
                    
                })
            }
        } catch (error) {
            console.log("enter into error .,....")
            this.setState({loader: false});
            console.log("qwqwqwq")

            alert(error.message);
        }
    }
}

    backButtonAction() {
        console.log("back button called .......")
        // this.props.navigation.navigate('loginNavigationOption')
        this.props.navigation.navigate('Login')

    }

    render() {

        return(
            <View style={{flex:1}}>
                
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', 
                              height:50, backgroundColor:'#fff', zIndex:1}}>
                        <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        />
                    <TitleText
                            size = {15}
                            color = 'black'
                            weight = '400'
                            alignment = 'center'
                            title = 'Create account'
                            />

                    <View style={{width:90}}/>
                </View>
                <BottomBorderView 
                horizontal={0}
                top={0}
                />
                <TouchableWithoutFeedback
                onPress={()=> Keyboard.dismiss()}
                >
                    <KeyboardAvoidingView behavior="position" enabled
                    style={{height: height, width: width}}
                    contentContainerStyle={{flex:1}}
                                keyboardVerticalOffset={-120} style={{flex: 1}}>
                            <View style={{flex:1}}>
                                {this.state.loader ? <MyActivityIndicator /> : null}
                                <View style={{ flex:1, alignItems:'center', justifyContent:'center'}}>
                                    <Image style={{}} source={require('../../assets/ScanFIT.png')}/>
                                </View>
                                
                                <View style={{flex:4}}>

                                <BottomBorderView 
                                horizontal='10%'
                                top={10}
                                />
                                <Text style={{
                                        fontSize:18,
                                        fontWeight:'300',
                                        width:'100%',
                                        textAlign:'center',
                                        marginTop:10,
                                }}
                                >Let's create your account
                                </Text>
                                <TextField 
                                        size = {15}
                                        weight = '200'
                                        horizontal = "10%"
                                        image = {require('../../assets/name.png')}
                                        placeholderText = "First Name"
                                        value = {this.state.fName}
                                        onChangeText = {(fName)=> this.setState({fName})}
                                        keyboard = 'default'
                                />
                                <BottomBorderView 
                                horizontal= '8%'
                                top={5}
                                />

                                <TextField 
                                    size = {15}
                                    weight = '200'
                                    horizontal = "10%"
                                    image = {require('../../assets/name.png')}
                                    placeholderText = "Last Name"
                                    value = {this.state.lName}
                                    onChangeText = {(lName)=> this.setState({lName})}
                                    keyboard = 'default'
                                />
                                <BottomBorderView 
                                horizontal= '8%'
                                top={5}
                                />

                                <TextField 
                                    size = {15}
                                    weight = '200'
                                    horizontal = "10%"
                                    image = {require('../../assets/email.png')}
                                    placeholderText = "Email address"
                                    value = {this.state.email}
                                    onChangeText = {(email)=> this.setState({email})}
                                    keyboard = 'email-address'
                                />
                                <BottomBorderView 
                                horizontal= '8%'
                                top={5}
                                /> 
                                <Button
                                horizontal = '8%'
                                top = {50}
                                radius = {20}
                                backgColor = '#69D3A9'
                                height = {40}
                                weight = 'bold'
                                textColor = 'white'
                                titleSize = {16}
                                title = 'Get In'
                                buttonAction = {()=>this.registrationAction()}
                                />
                                </View>
                                </View>
                        </KeyboardAvoidingView>
                </TouchableWithoutFeedback>
            </View>
        )
    }
}
const mapStateToProps = state =>{
    return{
        userData: state.user
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        updateProfile:(type,userData) => updateProfile(type,userData,dispatch),

    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(Registration);