import React, { Component } from 'react';
import {
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  ScrollView,
  UIManager,
  Platform,
  Dimensions,
  AsyncStorage,
  Alert
} from 'react-native';
import moment, { months } from 'moment';
import BottomBorderView from '../../Component/BottomBorderView';
import ExpandableItemComponent from '../../Component/ExpandableItemComponent';
// import { connect } from 'react-redux';
// import {setLifeStyle} from '../../store/actions/user';
// import Button from '../../Component/Button';
import Picker from 'react-native-picker';
import { connect } from 'react-redux';
import { updateProfile } from "../../store/actions/user";
import API from '../../../redAPI';
const width = Dimensions.get("window").width;
const options = [['4', '5', '6', '7', '8','9'],
['0', '1', '2', '3', '4', '5', '6', '7', '8','9','10','11']]
const olderDate = moment().add(-18, 'years').toDate()
class TellUs extends Component {
  //Main View defined under this Class
  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = { 
      
      listDataSource: [
      {
        index:0,
        isExpanded: false,
        testExpand:'',
        category_name: 'Age',
        image:require('../../assets/age.png'),
        width:18,
        height:18,
        subcategory: [],
      },
      {
        index:1,
        isExpanded: false,
        testExpand:'',
        width:20,
        height:25,
        category_name: 'Height',
        image:require('../../assets/height.png'),
        subcategory: [],
      },
      {
        index:2,
        isExpanded: false,
        testExpand:'',
        width:18,
        height:18,
        category_name: 'Weight',
        image:require('../../assets/weight-scale.png'),
        subcategory: [],
      },
      {
        index:3,
        isExpanded: false,
        testExpand:'',
        width:22,
        height:21,
        category_name: 'Current Lifestyle',
        image:require('../../assets/gym.png'),
        subcategory: [{ id: 0, val: 'Active',image:require('../../assets/active.png'), isCheck:false }, 
        {id: 1,val: 'Moderately Active',image:require('../../assets/ModActive.png'), isCheck:false},
        {id: 2,val: 'Sednetary(Inactive)', image:require('../../assets/sedentary.png'), isCheck:false}],
      },
      {
        index:4,
        isExpanded: false,
        testExpand:'',
        width:19,
        height:12,
        category_name: 'Current Experience with Excercise',
        image:require('../../assets/dumbbell.png'),
        subcategory: [{ id: 0, val: 'Beginner', image:require('../../assets/Shape.png'), isCheck:false}, 
        {id: 1, val: 'Intermediate', image:require('../../assets/intermediate.png'),isCheck:false}, 
        {id: 2, val: 'Advanced', image:require('../../assets/rings.png'), isCheck:false}],
      },
    ],
        age:moment(olderDate).format('MM/DD/YYYY'),
        height:'',
        weightKg:'',
        weightLbs:'',
        currentLifeStyle:'',
        currentExperience:'',
        selectedValue:'',
        dateSElection:false
     };
    this.updateAge= this.updateAge.bind(this)
  }
 
  componentDidMount() {

 
    // this.props.navigation.navigate('WELCOME')
    // AsyncStorage.getItem('tellus').then((data)=> {

    //     console.log("tellus data is", data)
    //     if(data !== null){
    //       const item = JSON.parse(data);

    //       console.log("item is", item)
    //       console.log("height is", item[0].height)
    //       this.setState({
    //         height:item[0].height,
    //         weight:item[1].weight,
    //         age:item[1].age,
    //       })
    //     }
    // })

    Picker.init({
      pickerData: options,
      pickerTitleText: 'Select Feet and inch',
      // pickerTextEllipsisLen: 20,
      // pickerFontFamily: platform === 'android' ? 'Roboto-Regular' : 'System',
      pickerFontSize: 16,
      pickerBg:[255, 255, 255, 1],
      // pickerRowHeight: 32,
      pickerConfirmBtnText: 'Confirm',
      pickerCancelBtnText: 'Cancel',
      // pickerTitleColor: [7, 47, 106, 1],
      // _this.state.selectedValue= [59],
      onPickerConfirm: data => {
          console.log(data);
          console.log(data[0]);
          console.log(data[1]);
          let height = data[0]+"."+data[1];
          this.setState({height:height})
      },
      onPickerCancel: data => {
          console.log(data);
      },
      onPickerSelect: data => {
          console.log(data);
      }
  });
}

  updateAge = (value) => {
// alert(value)
this.setState({
  age:value
});
    // console.log('ios',value);
    // //  alert(value);
    // // console.log("Updated age ...", value)
    // this.setState({age:value, dateSElection:true})
    // console.log('ios',value);
  }

  // updateHeight = (value) => {
    updateHeight = (feet,inch) => {
      let height = feet+"."+inch;
      this.setState({height:height})
  }

  updateWeight = (value) => {
   // console.log("Updated weight ...", value)
    this.setState({weightKg:value})
    // let convertToPound = String(value * 2.20)
    if(value){
      var convertToPound = value * 2.2046
      convertToPound = convertToPound.toFixed(2)
    //  console.log("Converted pound ", convertToPound)
      this.setState({weightLbs:String(convertToPound)})
    }else{
      this.setState({weightLbs:''})
    }
    
  }

  updateWeightInLbs = (value) => {
   // console.log("Updated weight lbs...", value)
    this.setState({weightLbs:value})
    if(value){
       // let convertToKg =  String(value * 0.45)
      var convertToKg =  value * 0.453592
      convertToKg = convertToKg.toFixed(2)
     // console.log("Converted pound ", convertToKg)
      this.setState({weightKg: String(convertToKg)})
    }else{
      this.setState({weightKg:''})
    }
   
  }

  updateUI = (sectionCount, value, itemId) => {

    const targetOption = this.state.listDataSource[sectionCount];
    
    var count = 0;

    targetOption.subcategory.map((value, placeindex) =>{
      // console.log("llllllllllllllll")
      // console.log(value)
      placeindex === itemId
      ? (targetOption.subcategory[placeindex]['isCheck'] = !targetOption.subcategory[placeindex]['isCheck'])
      : (targetOption.subcategory[placeindex]['isCheck'] = false)

      if(sectionCount === 3){
        console.log(targetOption.subcategory[placeindex]['isCheck'])
        if(targetOption.subcategory[placeindex]['isCheck']){
          count = 1;
        }
      }else if(sectionCount === 4){
        if(targetOption.subcategory[placeindex]['isCheck']){
          count = 1;
        }
      }
    }
  );

  console.log("New target option ....")
  console.log(targetOption);

    const array = [...this.state.listDataSource];
    array.map((value, placeindex) =>
      placeindex === sectionCount
        ? (array[placeindex]['testExpand'] = 'fffff')
        : (array[placeindex]['testExpand'] = '')
    );
    const newArray = [...this.state.listDataSource]
    array[sectionCount] = targetOption
  
      this.setState(() => {
        return {
          listDataSource: newArray,
        };
      });

      if(sectionCount === 3){
       
        if(count === 1){
          this.setState({currentLifeStyle:value})
        }else{
          this.setState({currentLifeStyle:''})
        }
      }else if(sectionCount === 4){
        if(count === 1){
          this.setState({currentExperience:value})
        }else{
          this.setState({currentExperience:''})
        }
      }
  }

  updateLayout = index => {

    console.log("index value is ", index)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...this.state.listDataSource];
    array.map((value, placeindex) =>
      placeindex === index
        ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
        : (array[placeindex]['isExpanded'] = false)
    );
    this.setState(() => {
      return {
        listDataSource: array,
      };
    });
  };

 async continueAction() {
  
    // this.props.updateProfile("data ye bheja");
    // this.props.navigation.navigate('IdentifyUrself')
    // this.props.navigation.navigate('WorkoutDaySelection')
    // alert(this.state.age)
// if(!this.state.dateSElection){
//   var agecopy = this.state.age;
//   await this.setState({
//     age:moment(agecopy).format('MM/DD/YYYY')
//   },()=>{
//     alert("age set",this.state.age)
//   })
// }
    console.log("height is ",olderDate)
    console.log('hi',this.state.age)
    
    if(this.state.age.length === 0) {
      alert("Please select your age.")
    }else if(this.state.height.length === 0) {
      alert("Please enter your height.")
    }else if(this.state.weightLbs.length === 0) {
      alert("Please enter your weight.")
    }else if(this.state.currentLifeStyle.length === 0) {
      alert("Please select current life style.")
    }else if(this.state.currentExperience.length === 0) {
      alert("Please select current experience with excercise.")
    }
    else {
      // hit date check here 
      var Body ={
        date_of_birth : this.state.age,
      }
      // alert(JSON.stringify({
      //   date_of_birth : this.state.age,
      // }));
      let response = await API.CheckDob({date_of_birth : this.state.age})
      response.json().then((JRes) => {
        console.log(JRes)
        if(JRes.success){
          var value = [{'height':this.state.height.toString()}, 
          {'weight':this.state.weightLbs}, 
          {'age':this.state.age},
          {'lifestyle':this.state.currentLifeStyle},
          {'experience':this.state.currentExperience}]
        console.log( 'height', this.state.height.toString());
          AsyncStorage.setItem('tellus',JSON.stringify(value))
          this.props.navigation.navigate('IdentifyUrself')
        
        }
        else{
          alert(JRes.errors)
        }
      })

    }
  }
 
  render() {
    // console.log("render tell us calle d.....")
    // console.log(this.state.weightLbs)
    return (
      <View style={styles.container}>
           <BottomBorderView 
                horizontal={0}
                top={0}
                />
        <Text style={styles.topHeading}>Tell us about yourself</Text>
        <ScrollView>
       
          {this.state.listDataSource.map((item, key) => (
            <ExpandableItemComponent
            ageType={false}
              key={item.category_name}

              age1={this.state.age}
              age2={null}
              height2={null}
              height1={this.state.height}
              weight1={this.state.weightKg}
              weightLbs = {this.state.weightLbs}
              updateUI={this.updateUI.bind(this)}
              updateAge={age =>this.updateAge(age) }
              updateHeight={this.updateHeight.bind(this)}
              updateWeight={this.updateWeight.bind(this.state.weightKg)}
              updateWeightInLbs={this.updateWeightInLbs.bind(this.state.weightLbs)}
              onClickFunction={this.updateLayout.bind(this, key)}
              item={item}
              page={'tellus'}
            />
          ))}
           <Button
                horizontal = '8%'
                top = {50}
                radius = {20}
                backgColor = '#69D3A9'
                height = {40}
                weight = 'bold'
                textColor = 'white'
                titleSize = {16}
                title = 'Continue'
                buttonAction = {()=>this.continueAction()}
                />
        </ScrollView>
      </View>
    );
  }
}
 

// const mapStateToProps = state =>{
//     return {
//      isSelected: '1'
//     }
// }
// const mapDispatchToProps = dispatch =>{
//     return{
//         setLifeStyle:(value) => setLifeStyle(value)
//     }
// }

// let ExpandableItemComponentRedux = connect(mapSatateToProps, mapDispatchToProps)(ExpandableItemComponent);
// let TellUsRedux = connect(mapSatateToProps, mapDispatchToProps)(TellUs);

//  export default connect(mapStateToProps, mapDispatchToProps)(TellUs);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  topHeading: {
    paddingLeft: 10,
    fontSize: 20,
    marginVertical: 15
  },
});
 
const CONTENT = [
  {
    index:0,
    isExpanded: false,
    testExpand:'',
    category_name: 'Age',
    image:require('../../assets/age.png'),
    width:18,
    height:18,
    subcategory: [],
  },
  {
    index:1,
    isExpanded: false,
    testExpand:'',
    width:20,
    height:25,
    category_name: 'Height',
    image:require('../../assets/height.png'),
    subcategory: [],
  },
  {
    index:2,
    isExpanded: false,
    testExpand:'',
    width:18,
    height:18,
    category_name: 'Weight',
    image:require('../../assets/weight-scale.png'),
    subcategory: [],
  },
  {
    index:3,
    isExpanded: false,
    testExpand:'',
    width:22,
    height:21,
    category_name: 'Current Lifestyle',
    image:require('../../assets/gym.png'),
    subcategory: [{ id: 0, val: 'Active',image:require('../../assets/active.png'), isCheck:false }, 
    {id: 1,val: 'Moderately Active',image:require('../../assets/ModActive.png'), isCheck:false},
    {id: 2,val: 'Sednetary(Inactive)', image:require('../../assets/sedentary.png'), isCheck:false}],
  },
  {
    index:4,
    isExpanded: false,
    testExpand:'',
    width:19,
    height:12,
    category_name: 'Current Experience with Excercise',
    image:require('../../assets/dumbbell.png'),
    subcategory: [{ id: 0, val: 'Beginner', image:require('../../assets/Shape.png'), isCheck:false}, 
    {id: 1, val: 'Intermediate', image:require('../../assets/intermediate.png'),isCheck:false}, 
    {id: 2, val: 'Advanced', image:require('../../assets/rings.png'), isCheck:false}],
  },
];
const mapStateToProps = state =>{
  return{
      userData: state.user
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
      updateProfile:(userData) => updateProfile(userData,dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(TellUs);