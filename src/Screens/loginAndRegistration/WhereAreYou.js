
import React, {Component} from 'react';
import {StyleSheet, View, Dimensions, TouchableWithoutFeedback, AsyncStorage, Image} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import BackButton from '../../Component/BackButton';
import TitleText from '../../Component/TitleText';
import api from '../../../api';
import MyActivityIndicator from '../../Component/activity_indicator';
import ImageLoad from 'react-native-image-placeholder';
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const styles = StyleSheet.create({
    slide: {
        height: height-200, width: width-40, backgroundColor:'white', marginTop:10
    },
});

export default class WhereAreYou extends Component {
    constructor(props) {
        super(props);
        this.state={
            selectedOption:null,
            slides:[],
            loader:false,
        }
    }

    componentDidMount() {
        this.getImages()
        if(!this.props.navigation.state.params.identityChange){
            console.log("false .......")
            AsyncStorage.getItem('whereru').then((data)=>{
                console.log("selected where are you ", data)
                if(data !== null){
                    let jsonValue = JSON.parse(data);
                    console.log("selected where are you 111111", jsonValue)
                    console.log(jsonValue.id)
                    this.setState({selectedOption:jsonValue.id})
                }
            })
        }
    }

    getImages = async () => {
        this.setState({
            loader: true
        })
        try{
            let body = {
                "gender": this.props.navigation.state.params.gender
            };
            console.log("body is ")
            console.log(body)

            let response = await api.request('/where-are-you-at', 'post', body, null);
            console.log("where are you response .")
            console.log(response)
            if (response.status == 200) {
                response.json().then((data) => {
                    console.log("Images data where are you response ......")
                    console.log(data);
                    this.setState({
                        slides:data.records,
                        loader: false
                    },()=>{
                        let index=0;
                        console.log("selected",index, this.state.slides[index]);
        const value = {'id':this.state.slides[index].id}
        console.log("Value is ")
        console.log(value)
        AsyncStorage.setItem('whereru',JSON.stringify(value))
        this.setState({selectedOption:this.state.slides[index]})
                    })
                })
            }
            else {
                this.setState({loader: false});
                response.json().then((respons) => {
                    console.log("error repsosne ")
                    console.log(respons)
                    alert(respons.errors);
                })
            }
        }catch (error) {
            this.state.loader(false);
            alert(error.message);
        }
    }

    nextBtnAction() {
        if(this.state.selectedOption === null){
           alert("Please select one option.")
        }else{
            this.props.navigation.navigate('WhatsYourEndGoal', {gender:this.props.navigation.state.params.gender, identityChange:this.state.identityChange})
        }
    }

    backButtonAction() {
        this.props.navigation.navigate('IdentifyUrself')
    }
    _onPress(index){
        console.log("selected",index, this.state.slides[index]);
        const value = {'id':this.state.slides[index].id}
        console.log("Value is ")
        console.log(value)
        AsyncStorage.setItem('whereru',JSON.stringify(value))
        this.setState({selectedOption:this.state.slides[index]})
        // let targetSlide = this.state.slides;
        // console.log(targetSlide);
        // targetSlide[index].mark = !targetSlide[index].mark;
        // console.log(targetSlide);
        // this.setState({slides: [...this.state.slides]});
    }

    _renderItem = ({ item, index }) => {
        return (
          <TouchableWithoutFeedback 
        //   onPress={()=>this._onPress(item, index)}
          >
            <View style={{alignItems:'center', justifyContent:'center'}}>
                <ImageLoad
                    style={styles.slide}
                    loadingStyle={{ size: 'large', color: 'black' }}
                    source={{uri: (item.image)}}
                />
                {
                    this.state.selectedOption === item.id ? (
                        <Image style={{width:150, height:150, marginTop:-180}} source={require('../../assets/SelectedTick.png')} />
                    ) : (null)
                }
            </View>
          </TouchableWithoutFeedback>
        );
      }

    render() {
        console.log("render  called ......")
        return (
            <View style={{flex:1}}>
                 <BackButton
                        buttonAction = {()=> this.backButtonAction()}/>
                <BottomBorderView 
                    horizontal={0}
                    top={0}
                    />
                <TitleText size = {15} color = 'black'weight = '400' alignment = 'left' width = '90%' height = {50} title = 'Where are you at?'top = {10} />
                {this.state.loader ? <MyActivityIndicator /> : null}
                 <AppIntroSlider 
                        renderItem={this._renderItem} 
                        skipLabel={this.state.selectedOption}
                        slides={this.state.slides}
                        showSkipButton={false}
                        showDoneButton={false}
                        showNextButton={false}
                        showPrevButton={false}
                        onSlideChange={(index,lastIndex)=>{console.log("app intro slider selected",index,lastIndex ); this._onPress(index)}}
                        activeDotStyle={style={backgroundColor:'red'}}/>
                <Button
                    horizontal = '12%'
                    top = {-20}
                    radius = {18}
                    bottom = {10}
                    backgColor = '#69D3A9'
                    height = {36}
                    weight = '400'
                    textColor = 'white'
                    titleSize = {18}
                    title = 'Next'
                    buttonAction = {()=>this.nextBtnAction()}
                />
            </View>
        );
    }
}
