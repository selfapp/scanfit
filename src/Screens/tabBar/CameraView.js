
import React, { Component } from 'react';
import {
    View, Text, KeyboardAvoidingView, Dimensions, Keyboard,
    TouchableOpacity, Image, ScrollView, StyleSheet, FlatList, TextInput,
    ActivityIndicator, Platform, StatusBar, BackHandler,Alert,Modal,TouchableWithoutFeedback,
} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { fetchMyEquipment } from "../../store/actions/user";
import CompleteFlatList from 'react-native-complete-flatlist';
import { withNavigationFocus } from 'react-navigation';
import MultiSelect from 'react-native-multiple-select';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Share from 'react-native-share';
import ImageLoad from 'react-native-image-placeholder';
import { RNCamera } from 'react-native-camera';
import ToggleSwitch from 'toggle-switch-react-native';
import { StackActions, NavigationActions } from 'react-navigation';

var once = 1

const width = Dimensions.get("window").width;
class Equipments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Image64: null,
            orignalImage: null,
            Scanned: false,
            isHome: false,
            equipmentName: '',
            loader: false,
            noData: false,
            enableScrollViewScroll: true,
            arrEqip: [],
            arrAddedEquipIndex: [],
            selectedItems: [],
            openList: false,
            userCancelled: false,
            focusedScreen: false,
            isScan: false,
            shareable: false,
            SearchResult:{},
            modalVisible: false,
            selected:null,
            NoClicked:false,
            front:false
            
        }
        this.arrEqipInitial = [];
    }
    onStartScan() {
        this.setState({

            Scanned: false,
        })
        if (!this.state.isScan) {
            this.searchImage(this.state.Image64)
        } else {
            this.onShare();
            this.setState({
                // shareable: true,
                // Scanned: true,
            }, () => { })

        }
    }
    onShare = async () => {
        if (true) {
            const shareOptions = {
                title: 'Shared via ScanFit',
                url: `data:image/jpeg;base64,${this.state.Image64}`, // only for base64 file in Android 
            };
            Share.open(shareOptions)
                .then((res) => { console.log(res) })
                .catch((err) => { err && console.log(err); });
        }
        else {

        }
    };

    componentDidMount() {
        this.props.fetchMyEquipment();
        const { navigation } = this.props;
        // const { navigation } = this.props;
        navigation.addListener('willFocus', () =>
            this.setState({
                focusedScreen: true, Image64: null,
                orignalImage: null,
                Scanned: false,
            })
        );
        navigation.addListener('willBlur', () =>
            this.setState({
                focusedScreen: false, Image64: null,
                orignalImage: null,
                Scanned: false,
            })
        );
      
    }

 

    async componentWillMount() {
        this.props.fetchMyEquipment();
        this.fetchEquipmentList()
        const isHome = await this.props.navigation.getParam('HomeView');
        if (isHome) {
            this.setState({ isHome }, console.log("home", this.state.isHome));
        }
        
    }

    componentWillUnmount() {
        this.setState({ arrAddedEquipIndex: [] })
    }
    renderModalContent() {
        return(
            <TouchableWithoutFeedback
            onPress={()=>Keyboard.dismiss()}
        >
         {
             this.state.NoClicked?
             <View style={{ padding:10,borderRadius:10, justifyContent: 'center', backgroundColor:"white",width:'95%'}}>
             {/* <View style={{paddingVertical:10}}>
             {this.state.selected &&  <Text style={{fontSize: 20, color: 'green', fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>{this.state.selected.title}</Text>}
             </View>
             <View style={{ borderStyle: 'dashed', borderWidth: .5 }}/> */}
             <View>
                 <View style={{ marginVertical:20}}>
                     <Text style={{textAlign:'center', marginVertical:10, marginHorizontal: 20, color:"lighgray"}}>{   ` Please select option`      }</Text>
                 </View>
                </View>
                <View style={{ borderStyle: 'dashed', borderWidth: .5 }}/>
                 <View style={{ flexDirection:'row', height:40}}>
                     <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                         onPress={()=>this.setState({modalVisible:false,NoClicked:false})}
                     >
                         <Text style={{color:'red',fontWeight:'bold'}}>Rescan</Text>
                     </TouchableOpacity>
                     <View style={{width:1, backgroundColor:'lightGray'}}/>
                     <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                         onPress={()=>{
                             this.setState({modalVisible:false,NoClicked:false});
                             this.props.navigation.navigate('SNEquipmentList',{HomeView:false})
                         }}
                     >
                         <Text style={{color:'green',fontWeight:'bold'}}>Add manually</Text>
                     </TouchableOpacity>
                 </View>
         </View>
         :
         <View style={{ padding:10,borderRadius:10, justifyContent: 'center', backgroundColor:"white",width:'95%'}}>
         <View style={{paddingVertical:10}}>
         {this.state.selected &&  <Text style={{fontSize: 20, color: 'green', fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>{this.state.selected.title}</Text>}
         </View>
         <View style={{ borderStyle: 'dashed', borderWidth: .5 }}/>
         <View>
             <View style={{ marginVertical:20}}>
                 <Text style={{textAlign:'center', marginVertical:10, marginHorizontal: 20, color:"lighgray"}}>{   ` Is this your equipment?`      }</Text>
             </View>
            </View>
            <View style={{ borderStyle: 'dashed', borderWidth: .5 }}/>
             <View style={{ flexDirection:'row', height:40}}>
                 <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                     onPress={()=>this.setState({NoClicked:true})}
                 >
                     <Text style={{color:'red',fontWeight:'bold'}}>No</Text>
                 </TouchableOpacity>
                 <View style={{width:1, backgroundColor:'lightGray'}}/>
                 <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}
                     onPress={()=>{
                         this.setState({modalVisible:false}),
                         this.addEquipmentAction(this.state.selected)
                     }}
                 >
                     <Text style={{color:'green',fontWeight:'bold'}}>Yes</Text>
                 </TouchableOpacity>
             </View>
     </View>
         }
        </TouchableWithoutFeedback>
    
        )
    }

    searchImage = async (imageData) => {
        this.setState({ loader: true, Scanned: false });
        let body = {
            "image": `data:image/jpeg;base64,${imageData}`//"data:image/jpeg;base64"
        };

        console.log(body);
        try {
             console.log(body);
            let response = await api.request('/image-recognition', 'post', body);
            console.log(response)
            if (response.status === 200) {
                response.json().then((data) => {
                    console.log("final response search image....")
                    console.log(data.records[0])
                    this.setState({modalVisible: true,selected:data.records[0]})
                    // Alert.alert(data.records[0].title,"Is this your Equpment",[
                    //     { text: 'Cancel', onPress: () => {
                            
                    //        }},
                    //     { text: 'OK', onPress: () => {
                    //       this.addEquipmentAction(data.records[0])
                    //      }} 
                    //     ])
                    console.log("++++++++++++ search images +++++++++++++");
                    // this.setAddedToList(data.records, true),
                        this.setState({
                            loader: false,
                            SearchResult: data.records[0]
                        })
                });
            }
            else if (response.status === 401) {
                // console.log("enter into 401 .,....")
                this.setState({ loader: false });
            }
            else if (response.status === 422) {
                // console.log("enter into 401 .,....")
                this.setState({ loader: false });
                alert("Can not process this image try others")
            }
            else {
                // console.log("enter into else .,....")
                this.setState({ loader: false });
                response.json().then((response) => {
                    // console.log("sayad yaha",response )
                    if (response.errors) { alert(JSON.stringify(response.errors)); }
                })
            }
        } catch (error) {
            // console.log("enter into error .,....")
            this.setState({ loader: false });
            // console.log("qwqwqwq")
            // alert(error.message);
        }
    }

    showActionSheet() {
        // console.log("Show action sheet called l..........")
        this.ActionSheet.show()
    }

    onEnableScroll(value) {
        this.setState({
            enableScrollViewScroll: value,
        });
    };

    addEquipmentAction = async (item) => {

        console.log("item details")
        console.log(item)
        var ids=[];
        ids.push(item.id);
        this.setState({ loader: true });
        let body = {
            'equipment_ids': ids
        };
         console.log("body is ", ids)
        // console.log(body)

        try {
            // console.log(body);
            let response = await api.request('/add-equipment', 'post', body);

            if (response.status === 200) {

                this.setState({ loader: false },()=>{
                    // alert('this', this.state.loader)
                });
                // console.log("response for add equipment")
                // console.log(response)
                response.json().then((respons) => {
                    this.setState({ loader: false },()=>{
                        
                    });
                    alert(respons.message)
                    this.setState({
                        Image64: null,
                    })
                     console.log("final response ")
                     console.log(respons)
                });
                

            }
            else {
                // console.log("enter into else .,....")
                this.setState({ loader: false });
                response.json().then((response) => {
                    if (response.errors) {
                        alert(response.errors);
                        this.setState({ loader: false });
                    }
                    if (response.message) {
                        alert(response.message)
                        this.setState({ loader: false });
                    }
                })
            }
        } catch (error) {
            // console.log("enter into error .,....")
            this.setState({ loader: false });
            // console.log("qwqwqwq")
            alert(error.message);
        }
        this.setState({ loader: true });
    }

    // takeImageAction

    continueAction() {
        this.setState({ arrAddedEquipIndex: [] })
        this.props.navigation.navigate('SNEquipmentList')
    }




 
    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            console.log(data);
            this.setState({
                Image64: data.base64,
                orignalImage: data.uri
            }, () => {
                this.onStartScan()
            })

        }
    };



    render() {
        const { selectedItems } = this.state;
        const { focusedScreen } = this.state;
        return (
            <View style={{ flex: 1, }}>
         {this.state.loader && <MyActivityIndicator /> }
         <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                           <TouchableWithoutFeedback onPress={() => {
                               this.setState({modalVisible: false,error:''})
                           }}>
                               <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.67)', padding:15 }}>
                                   <TouchableWithoutFeedback>
                                       {this.renderModalContent()}
                                   </TouchableWithoutFeedback>
                               </View>
                           </TouchableWithoutFeedback>
                       </Modal>
 
                {!this.state.Scanned ?
            
                    <View style={styles.container}>
                       
                        {
                            focusedScreen && ( this.state.front ?<RNCamera
                            flashMode={RNCamera.Constants.FlashMode.off}
                            type={RNCamera.Constants.Type.front}
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={RNCamera.Constants.Type.front}
                                flashMode={RNCamera.Constants.FlashMode.off}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                            />:
                            <RNCamera
                            flashMode={RNCamera.Constants.FlashMode.off}
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={RNCamera.Constants.Type.back}
                                flashMode={RNCamera.Constants.FlashMode.off}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                            />)
                        }
                         <View style={{height:50,width:'100%',justifyContent:'space-between', flexDirection:'row',alignItems:'center',paddingHorizontal:20,paddingTop:20,position:'absolute',top:20,}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('HOME')}}><Image  source={require('./../../assets/arrow.png')} style={{tintColor:'white'}} /></TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this.setState({front:!this.state.front}),console.log(this.state)}}><Image  source={require('./../../assets/flip.png')} style={{tintColor:'white'}} /></TouchableOpacity>
                        </View>
                        <View style={{ left: '5%', position: 'absolute', top: 90, height: Dimensions.get('window').height - 300, flex: 0, width: '90%', flexDirection: 'row', justifyContent: 'center', }}>
                            <Image source={require('../../assets/camcenter.png')} />
                        </View>
                        <View style={{ position: 'absolute', bottom: -10, flex: 0, width: '100%', flexDirection: 'row', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.4)', height: 80, alignItems: 'center', justifyContent: 'space-evenly' }}>
                            <View style={{ width: '33%' }} >
                                <TouchableOpacity
                                //  disabled={this.state.isScan}
                                  onPress={
                                    () => {
                                        ImagePicker.openPicker({
                                            // width: 300,
                                            // height: 400,
                                            cropping: false,
                                            includeBase64: true,
                                            compressImageQuality: 0.5,
                                            mediaType: 'photo'
                                        }).then(image => {
                                            this.setState({
                                                Image64: image.data,
                                            }, () => {
                                                this.onStartScan()
                                            })
                                            // this.props.navigation.navigate('AddWquipment', {image:data})
                                            console.log(image);
                                        })
                                    }

                                } style={[styles.capture, {}]} >
                                    <Image source={require('../../assets/camgalery.png')} style={{ tintColor: 'white', height: 50, width: 50 }} />
                                    {/* <Text style={{ fontSize: 14 }}> SNAP </Text> */}
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '33%' }} >
                                <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                                    <Image source={require('../../assets/bt-camera.png')} style={{ tintColor: 'white', height: 50, width: 50 }} />
                                    {/* <Text style={{ fontSize: 14 }}> SNAP </Text> */}
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '33%' }} >
                                <View style={[, { flexDirection: 'row' }]}>
                                    {<Text style={{ color: !this.state.isScan ? 'green' : 'white', padding: 5,fontSize:8}}>Scan</Text>}
                                    <ToggleSwitch
                                        isOn={this.state.isScan}
                                        onColor="green"
                                        offColor="#d3d3d3"
                                        label=""
                                        labelStyle={{ color: "white", fontWeight: "900" }}
                                        size="small"
                                        onToggle={() => { this.setState({ isScan: !this.state.isScan }) }}
                                    />
                                    {<Text style={[this.state.isScan ? { color: 'green', padding: 5,fontSize:8 } : { color: "white", padding: 5,fontSize:8 }]}>Photo</Text>}
                                </View>
                            </View>
                        </View>
                    </View>

                    :

                    <View style={{ flex: 1,backgroundColor:'black' }}>
                        {/* {
                            console.log(this.state.Image64)
                        } */}

                        {
                            Platform.OS === "android" ?<Image style={{height:Dimensions.get('window').height*0.7,width:Dimensions.get('window').width,transform: [{rotate: '90deg' }],resizeMode:'cover'}}  source={{uri:`data:image/jpeg;base64,${this.state.Image64}`}}/>:<Image style={{height:'100%',width:'100%',transform}} resizeMode={"contain"} source={{uri:`data:image/jpeg;base64,${this.state.Image64}`}}/>
                        }
                      <View style={{height:0.2,width:'100%'}}>
                      <Button
                               horizontal='8%'
                               top={5}
                               radius={20}
                               backgColor='#69D3A9'
                               height={40}
                               weight='bold'
                               textColor='white'
                               titleSize={16}
                               title='Share'
                               buttonAction={() => this.onShare()}
                           />
                          </View>
                        
                    </View>

                }
            </View>


        )

    }

}
const mapStateToProps = state => {
    return {
        equipmetList: state.user.equipmetList,
        selectedItems_Equipments: state.user.selectedItems_Equipments,
        noEquipStatus: state.user.HAVENOEQUIP,
        loading: state.user.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchMyEquipment: () => fetchMyEquipment(dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Equipments));
const styles = StyleSheet.create({
    mainContainer: { flex: 1, marginHorizontal: 15, marginVertical: 15 },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',

    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        borderRadius: 5,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
});