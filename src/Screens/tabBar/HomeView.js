
import React, {Component} from 'react';
import {View, Text, Dimensions, Image, ScrollView, StyleSheet,
TouchableOpacity, AsyncStorage,FlatList} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
const edit = require('../../assets/edit.png');
const width = Dimensions.get("window").width;
import { connect } from 'react-redux';
import {GetBlogListFunction} from "../../store/actions/user";

class BlogListingPage extends Component {

    constructor(props) {
        super(props);
        this.state={
            loader:false,
            page:1
        }
    }
    componentDidMount(){
        this.props.GetBlogListFunction(this.state.page)
    }

    renderItem = ({index, item}) =>{
        return(
            <TouchableOpacity style={{width:'100%',paddingHorizontal:10,marginVertical:10}} onPress={()=>this.props.navigation.navigate('Artical',{BlogID:item.id})}>
            <View style={{paddingTop:10, paddingBottom:5,paddingHorizontal:5,}}>
            <Text style={{fontWeight:'bold', color:'#525252', fontSize:16,fontFamily: 'Montserrat-Regular'}} numberOfLines={1}>{item.title}</Text>
            <View style={{paddingTop:10,flexDirection:'row'}}>
            <Image source={require('../../assets/time.png')} style={{height:10, width:10,marginRight:7}} />
            <Text style={{fontWeight:'bold', color:'#525252', fontSize:10, textAlign:'right',}}>{item.posted_on}</Text>

            </View>
            </View>
            <View style={{padding:7,}}>
                <Text style={{color:'#9b9b9b', fontSize:13, maxHeight:150}} numberOfLines={3}>{item.content}</Text>
            </View >
            { item.image &&  <View style={{paddingVertical:10}}>
           <Image
style={{width: '100%', height: 200,resizeMode : 'cover'}}
source={{uri:item.image}} 

/> 
            </View>}
        <BottomBorderView 
        horizontal={0}
        top={10}
        />
        </TouchableOpacity>
        )
    }
  empetyList = () => {
   
        return(
           <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <BottomBorderView horizontal={0} top={10}/>
            <View style={{padding:10,height:150,width:'100%'}}>
            <Text style={{fontSize:26, fontWeight:'bold',textAlign:'center'}}>
                 No Blogs
            </Text>
            </View>
        <BottomBorderView horizontal={0} top={10}/>
        </View>
        )
  }

    

    render(){
        console.log("this.state.profileDetail...")
        return(
        <View style={{flex:1,paddingBottom:50}}>
            {this.props.blogLoading ? <MyActivityIndicator /> : null}

            <TitleText
                        size = {20}
                        // color = 'black'
                        weight = '400'
                        width = {width-135}
                        alignment = 'center'
                        height = {50}
                        title = 'ScanFIT'
                        top = {10}
                        />
           
            <BottomBorderView 
                horizontal={0}
                top={10}
                />
                <View>
                <FlatList
    // data={this.props.todayWorkout.exercises}
 data={this.props.blogList}
    keyExtractor = {(item, index) => index.toString()}
    renderItem = {this.renderItem}
    ListEmptyComponent = { this.empetyList}
    ItemSeparatorComponent = {this.renderSeparater}
    refreshing={this.props.blogLoading}
    onRefresh={()=> {this.setState({page:1},()=>{this.props.GetBlogListFunction(this.state.page,true)})}}
    onEndReached={()=>{this.setState({page:this.state.page+1},()=>{this.props.GetBlogListFunction(this.state.page,false)})}}
              onEndThreshold={0.5}
/>
                </View>
        </View>
        )
        
    }
}

const styles = StyleSheet.create({
    mainContainer:{flex:1,  marginVertical:15},
  
    profileBoxView:{ top:60, borderRadius:10, minHeight:200,
     borderColor:'lightgray', borderWidth:0.5, zIndex:-90,  elevation: 5,
     shadowColor: '#999666', shadowOffset: {width: 5, height: 5},
     shadowOpacity: 0.7, shadowRadius: 5,
     backgroundColor:'white'},
   
     smallBoxView:{justifyContent:'center', alignItems:'center', width:width/3-11,paddingBottom:10,
     marginTop:20},
    
    verticalBorder:{width:1, backgroundColor:'gray',marginHorizontal:1, marginTop:20}
    
});

const mapStateToProps = state =>{
    return{
        blogLoading:state.user.blogLoading,
        blogList:state.user.blogList
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        GetBlogListFunction:(page,refreshing)=>GetBlogListFunction(page,refreshing,dispatch)
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(BlogListingPage);