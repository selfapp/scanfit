import React, { Component } from 'react';
import {
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  ScrollView,
  UIManager,
  Platform,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  Alert
} from 'react-native';
import moment from 'moment';
import BottomBorderView from '../../Component/BottomBorderView';
import api from '../../../api';
import ExpandableItemComponent from '../../Component/ExpandableItemComponent';
// import MyActivityIndicator from '../../Component/activity_indicator';
import TitleText from '../../Component/TitleText';
import BackButton from '../../Component/BackButton';
import { connect } from 'react-redux';
import {updateProfile} from '../../store/actions/user';
import Button from '../../Component/Button';
import Picker from 'react-native-picker';
let userData = {};
const width = Dimensions.get("window").width;
const lessWidth=(width-180);
const options = [['4', '5', '6', '7', '8', '9'],
['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']]
var height2='';
var age2='';
class profile extends Component {
  //Main View defined under this Class
  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      listDataSource: CONTENT,
      first_name: '',
      last_name: '',
      age: '',
      height: '',
      weightKg: '',
      weightLbs: '',
      currentLifeStyle: '',
      currentExperience: '',
      selectedValue: '',
      gender: '',
    };
    userData = props.navigation.getParam('user');
    height2=userData.height;
  }

  backButtonAction() {
    this.props.navigation.state.params.onGoBack();
    this.props.navigation.goBack()
  }
  
  async componentDidMount() {
let targted1;
    let listdata = this.state.listDataSource;
    
      listdata.map((v,i)=>{
        console.log(listdata);

        if (listdata[i].index === 4) {
          targted1=i;
          const targetOption = listdata[i];
          targetOption.subcategory.map((value, placeindex) => {
            console.log(placeindex, value)
            if (value.val === userData.current_experience) {
              value.isCheck = true
              console.log('mila kya',value)
            }
          })
        }
        
      })
     if(targted1){
       console.log("targeted",this.state.listDataSource[targted1]);
       this.state.listDataSource[targted1].testExpand='fff'
     }
    console.log("new list data", listdata)
    this.setState({
      listDataSource:listdata
    })
    console.log(this.state.listDataSource);

    let targted2;
    let listdata2 = this.state.listDataSource;
    
      listdata2.map((v,i)=>{
        console.log(listdata2);

        if (listdata2[i].index === 3) {
          targted2=i;
          const targetOption2 = listdata[i];
          targetOption2.subcategory.map((value, placeindex) => {
            console.log(placeindex, value)
            if (value.val === userData.current_lifestyle) {
              value.isCheck = true
              console.log('mila kya',value)
            }
          })
        }
        
      })
     if(targted2){
       console.log("targeted",this.state.listDataSource[targted2]);
       this.state.listDataSource[targted2].testExpand='fff'
     }
    console.log("new list data 2", listdata2)
    this.setState({
      listDataSource:listdata2
    })
    console.log(this.state.listDataSource);
    this.setState({
      first_name: userData.first_name,
      last_name: userData.last_name,
      gender: userData.gender,
      height: userData.height,
      currentExperience:userData.current_experience,
      currentLifeStyle:userData.current_lifestyle,
      age: userData.age
    }, console.log('username', userData))
    console.log('age',userData.age)
    if (userData.weight_base_unit === "lbs") {
      this.updateWeightInLbs(userData.weight);
    }
    Picker.init({
      pickerData: options,
      pickerTitleText: 'Select Feet and inch',
      // pickerTextEllipsisLen: 20,
      // pickerFontFamily: platform === 'android' ? 'Roboto-Regular' : 'System',
      pickerFontSize: 16,
      // pickerRowHeight: 32,
      pickerConfirmBtnText: 'Confirm',
      pickerCancelBtnText: 'Cancel',
      // pickerTitleColor: [7, 47, 106, 1],
      // _this.state.selectedValue= [59],
      onPickerConfirm: data => {
        console.log(data);
        console.log(data[0]);
        console.log(data[1]);
        let height = data[0] + "." + data[1];
        this.setState({ height: height })
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      }
    });








  }

  updateAge = (value) => {
    // console.log("Updated age ...", value)
    this.setState({ age: value })
  }
  updateF_Name = (value) => {
    // console.log("Updated age ...", value)
    this.setState({ first_name: value })
  }
  updateL_Name = (value) => {
    // console.log("Updated age ...", value)
    this.setState({ last_name: value })
  }
  // updateHeight = (value) => {
    updateHeight = (feet,inch) => {
      let height = feet+"."+inch;
      this.setState({height:height})
    }

  updateWeight = (value) => {
    // console.log("Updated weight ...", value)
    this.setState({ weightKg: value })
    // let convertToPound = String(value * 2.20)
    if (value) {
      var convertToPound = value * 2.2046
      convertToPound = convertToPound.toFixed(2)
      //  console.log("Converted pound ", convertToPound)
      this.setState({ weightLbs: String(convertToPound) })
    } else {
      this.setState({ weightLbs: '' })
    }

  }

  updateWeightInLbs = (value) => {
    // console.log("Updated weight lbs...", value)
    this.setState({ weightLbs: value })
    if (value) {
      // let convertToKg =  String(value * 0.45)
      var convertToKg = value * 0.453592
      convertToKg = convertToKg.toFixed(2)
      // console.log("Converted pound ", convertToKg)
      this.setState({ weightKg: String(convertToKg) })
    } else {
      this.setState({ weightKg: '' })
    }

  }

  updateUI = (sectionCount, value, itemId) => {

//     const targetOption = this.state.listDataSource[sectionCount];

//     var count = 0;

//     targetOption.subcategory.map((value, placeindex) => {
//       // console.log("llllllllllllllll")
//       // console.log(value)
//       placeindex === itemId
//         ? (targetOption.subcategory[placeindex]['isCheck'] = !targetOption.subcategory[placeindex]['isCheck'])
//         : (targetOption.subcategory[placeindex]['isCheck'] = false)

//       if (sectionCount === 3) {
//         console.log(targetOption.subcategory[placeindex]['isCheck'])
//         if (targetOption.subcategory[placeindex]['isCheck']) {
//           count = 1;
//         }
//       } else if (sectionCount === 4) {
//         if (targetOption.subcategory[placeindex]['isCheck']) {
//           count = 1;
//         }
//       }
//     }
//     );

//     console.log("New target option ....")
//     console.log(targetOption);

//     const array = [...this.state.listDataSource];
//     array.map((value, placeindex) =>
//       placeindex === sectionCount
//         ? (array[placeindex]['testExpand'] = 'fffff')
//         : (array[placeindex]['testExpand'] = '')
//     );
//     const newArray = [...this.state.listDataSource]
//     array[sectionCount] = targetOption;
// console.log('sectionCount',array[sectionCount])
//     this.setState(() => {
//       return {
//         listDataSource: newArray,
//       };
//     });

//     if (sectionCount === 3) {

//       if (count === 1) {
//         this.setState({ currentLifeStyle: value })
//       } else {
//         this.setState({ currentLifeStyle: '' })
//       }
//     } else if (sectionCount === 4) {
//       if (count === 1) {
//         this.setState({ currentExperience: value })
//       } else {
//         this.setState({ currentExperience: '' })
//       }
//     }
  
  }

  updateLayout = index => {

    console.log("index value is ", index)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...this.state.listDataSource];
    array.map((value, placeindex) =>
      placeindex === index
        ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
        : (array[placeindex]['isExpanded'] = false)
    );
    this.setState(() => {
      return {
        listDataSource: array,
      };
    });
  };
 
  continueAction() {
    const {first_name,
    last_name,
    age,
    height,
    weightKg,
    weightLbs} = this.state
    console.log(this.state);
    console.log(first_name,
    last_name,
    age,
    height,
    weightKg,
    weightLbs,)
this.registrationAction();
  }
  async registrationAction() {
    const {first_name,
      last_name,
      age,
      height,
      weightKg,
      weightLbs} = this.state
    // this.props.navigation.navigate('Equipments')
       console.log("registration button called .......")

    if(first_name.length === 0){
        alert("Please enter your first name")
    }else if(last_name.length === 0){
        alert("Please enter your last name")
    }else if(height === 0){
        alert("Please enter your height")
    }else if(weightLbs ===0){
      alert("Please enter your weiight")
    }else{
        this.setState({loader: true});
     let body ={
       first_name:first_name,
       last_name:last_name,
       height:height,
       weight:weightLbs,
       name: first_name +' '+ last_name,
       age:age
     }
        console.log("body is ")
        console.log(body)

        try {
            console.log(body);
            let response = await api.request('/update-profile', 'post', body);

            if (response.status === 200) {
                console.log("enter into 200 .,....")

                console.log("response for registration ...")
                console.log(response)
                response.json().then(async (respons) => {
                    console.log("final registration response ,,,,,")
                    console.log(respons)
                    this.saver(respons.user);
                });
                this.setState({loader: false});
            }
            else if (response.status === 401) {
                console.log("enter into 401 .,....")

                this.setState({loader: false});
                alert('Invalid credentials');
            }
            else {
                console.log("enter into else .,....")

                this.setState({loader: false});
                response.json().then((response) => {
                    console.log("sjkdsdfh")
                    console.log(response)
                    if(response.errors){
                        // if(response.errors.mobile){
                        //     alert(response.errors.mobile);
                        // }
                    }
                    
                })
            }
        } catch (error) {
            console.log("enter into error .,....")
            this.setState({loader: false});
            console.log("qwqwqwq")

            alert(error.message);
        }
    }
}
saver = (userdata)=>{
  this.props.updateProfile(false,userdata);
this.props.navigation.state.params.onGoBack();
this.props.navigation.goBack()
}
  render() {
    // console.log("render tell us calle d.....")
    // console.log(this.state.weightLbs)
    return (
      <View style={styles.container}>
          
        <View style={{
          flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
          height: 50, backgroundColor: '#fff', zIndex: 1
        }}>
          <BackButton
            buttonAction={() => this.backButtonAction()}
          />
          <TitleText
            size={20}
            // color='black'
            weight='400'
            alignment='center'
            title='Profile'
          />

          <View style={{ width: 90 }} />
        </View>
        <BottomBorderView
          horizontal={0}
          top={0}
        />
        <Text style={styles.topHeading}>Personal Details</Text>
        <ScrollView>
        <TouchableOpacity
            activeOpacity={0.8}
            // onPress={this.props.onClickFunction}
            style={styles.header}>
                 <Image
                  style={{ height: 25,  marginRight:5}}
                  source={require('../../assets/name.png')} resizeMode='contain'
                          />
                          {/* {
                            this.props.item.index === 2 ? ( 
                              <Text style={[styles.headerText, {width:90}]}>{this.props.item.category_name}</Text>

                            ) : ( */}
                              <Text style={styles.headerText}>First Name</Text>
                            {/* )
                          } */}
                          <TextInput 
                          style={[styles.detailText,{position:'absolute',left:'67%'}]}
                  placeholder={this.state.first_name}
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={this.state.first_name}
                  
                  onChangeText={first_name => this.setState({first_name})}
                  />
                          </TouchableOpacity>
                          <BottomBorderView 
                  horizontal={15}
                  top={0}
                  />

<TouchableOpacity
            activeOpacity={0.8}
            // onPress={this.props.onClickFunction}
            style={styles.header}>
                 <Image
                  style={{ height: 25,  marginRight:5}}
                  source={require('../../assets/name.png')} resizeMode='contain'
                          />
                          {/* {
                            this.props.item.index === 2 ? ( 
                              <Text style={[styles.headerText, {width:90}]}>{this.props.item.category_name}</Text>

                            ) : ( */}
                              <Text style={styles.headerText}>Last Name</Text>
                            {/* )
                          } */}
                          <TextInput 
                          style={[styles.detailText,{position:'absolute',left:'67%'}]}
                  placeholder={this.state.last_name}
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={this.state.last_name}
                  
                  onChangeText={last_name => this.setState({last_name})}
                  />
                          </TouchableOpacity>
                          <BottomBorderView 
                  horizontal={15}
                  top={0}
                  />
          {this.state.listDataSource.map((item, key) => (
            <ExpandableItemComponent
            ageType={true}
              key={item.category_name}
              age1={this.state.age}
              age2={this.props.userData.age}
              height2={height2}
              height1={this.state.height}
              weight1={this.state.weightKg}
              weightLbs={this.state.weightLbs}
              updateUI={this.updateUI.bind(this)}
              updateAge={this.updateAge.bind(this.state.age)}
              updateHeight={this.updateHeight.bind(this)}
              updateWeight={this.updateWeight.bind(this.state.weightKg)}
              updateWeightInLbs={this.updateWeightInLbs.bind(this.state.weightLbs)}
              onClickFunction={this.updateLayout.bind(this, key)}
              item={item}
              f_name={this.state.first_name}
              s_name={this.state.last_name}
              updateF_Name={this.updateF_Name.bind(this.state.first_name)}
              updateL_Name={this.updateL_Name.bind(this.state.last_name)}
              page={'profile'}
            />
          ))}


<TouchableOpacity
            activeOpacity={0.8}
            // onPress={this.props.onClickFunction}
            style={styles.header}>
                 <Image
                  style={{ height: 25,  marginRight:5}}
                  source={require('../../assets/name.png')} resizeMode='contain'
                          />
                          {/* {
                            this.props.item.index === 2 ? ( 
                              <Text style={[styles.headerText, {width:90}]}>{this.props.item.category_name}</Text>

                            ) : ( */}
                              <Text style={styles.headerText}>Current Lifestyle</Text>
                            {/* )
                          } */}
                          <TextInput style={[styles.detailText, ]}
                  placeholder={this.state.currentLifeStyle}
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={this.state.currentLifeStyle}
                  editable={false}
                  // onChangeText={first_name => this.setState({first_name})}
                  />
                        
                          </TouchableOpacity>
                          <BottomBorderView 
                  horizontal={15}
                  top={0}
                  />
                  <TouchableOpacity
            activeOpacity={0.8}
            // onPress={this.props.onClickFunction}
            style={styles.header}>
                 <Image
                  style={{ height: 25,  marginRight:5}}
                  source={require('../../assets/name.png')} resizeMode='contain'
                          />
                          {/* {
                            this.props.item.index === 2 ? ( 
                              <Text style={[styles.headerText, {width:90}]}>{this.props.item.category_name}</Text>

                            ) : ( */}
                              <Text style={styles.headerText}>Current Experience</Text>
                            {/* )
                          } */}
                          <TextInput style={[styles.detailText, ]}
                  placeholder={this.state.currentExperience}
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={this.state.currentExperience}
                  editable={false}
                  // onChangeText={first_name => this.setState({first_name})}
                  />
                          </TouchableOpacity>
                          <BottomBorderView 
                  horizontal={15}
                  top={0}
                  />



         
        </ScrollView>
        <Button
            horizontal='8%'
            top={10}
            radius={20}
            backgColor='#69D3A9'
            height={40}
            weight='bold'
            textColor='white'
            titleSize={16}
            title='Update'
            buttonAction={() => this.continueAction()}
          />
      </View>
    );
  }
}
// profile

const mapStateToProps = state =>{
  return{
      userData: state.user.personal,
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
      updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(profile);




const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom:5
  },
  header: {
    padding: 15,
    flexDirection:'row',
    // backgroundColor:'red',
    height:50,
    alignItems:'center'
  },
  headerText: {
    fontSize: 13,
    fontWeight: '300',
    marginRight:7,
    // width:210,
    width:"50%",
    marginLeft:7,
    // backgroundColor:'pink'
  },
  detailText: {
    fontSize: 13,
     width:"40%",
    // marginRight:20,
    textAlign:'right',
    // backgroundColor:'yellow',
    height:40
  },
  separator: {
    height: 0.5,
    backgroundColor: '#808080',
    marginLeft: 30,
    marginRight: 30,
  },
  topHeading: {
    paddingLeft: 10,
    fontSize: 20,
    marginVertical: 15
  },
});

const CONTENT = [
  

  {
    index: 0,
    isExpanded: false,
    testExpand: '',
    category_name: 'Age',
    image: require('../../assets/age.png'),
    width: 18,
    height: 18,
    subcategory: [],
  },
  {
    index: 1,
    isExpanded: false,
    testExpand: '',
    width: 20,
    height: 25,
    category_name: 'Height',
    image: require('../../assets/height.png'),
    subcategory: [],
  },
  {
    index: 2,
    isExpanded: false,
    testExpand: '',
    width: 18,
    height: 18,
    category_name: 'Weight',
    image: require('../../assets/weight-scale.png'),
    subcategory: [],
  },
  // {
  //   index: 3,
  //   isExpanded: false,
  //   testExpand: '',
  //   width: 22,
  //   height: 21,
  //   category_name: 'Current Lifestyle',
  //   image: require('../../assets/gym.png'),
  //   subcategory: [{ id: 0, val: 'Active', image: require('../../assets/active.png'), isCheck: false },
  //   { id: 1, val: 'Moderately Active', image: require('../../assets/ModActive.png'), isCheck: false },
  //   { id: 2, val: 'Sednetary (Not Active)', image: require('../../assets/sedentary.png'), isCheck: false }],
  // },
  // {
  //   index: 4,
  //   isExpanded: false,
  //   testExpand: '',
  //   width: 19,
  //   height: 12,
  //   category_name: 'Current Experience with Excercise',
  //   image: require('../../assets/dumbbell.png'),
  //   subcategory: [{ id: 0, val: 'Beginner', image: require('../../assets/Shape.png'), isCheck: false },
  //   { id: 1, val: 'Intermediate', image: require('../../assets/intermediate.png'), isCheck: false },
  //   { id: 2, val: 'Advanced', image: require('../../assets/rings.png'), isCheck: false }],
  // },

];