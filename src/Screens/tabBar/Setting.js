


import React, {Component} from 'react';
import {View, Text,Dimensions, AsyncStorage,
     Image, StyleSheet, TouchableOpacity} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import TitleText from '../../Component/TitleText';
import BackButton from '../../Component/BackButton';
import ActionSheet from 'react-native-actionsheet'
import { StackActions, NavigationActions } from 'react-navigation';

const height = Dimensions.get("window").height;

export default class Setting extends Component {

    constructor(props) {
        super(props);
        this.state={
           
            loader:false,
            arrData:[{name:'About us', image:require('../../assets/about.png')},
            {name:'Terms and conditions', image:require('../../assets/paper.png')},
            {name:"Contact Us" , image:require('../../assets/about.png')},
            {name:'Delete account', image:require('../../assets/delete.png')}]       
        }
    }

    componentWillMount() {
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    logoutAction() {
        AsyncStorage.clear()
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'registrationNavigationOption' })],
          });
          
          this.props.navigation.dispatch(resetAction);
        // this.props.navigation.navigate('Splash')
        // this.props.navigation.navigate('loginNavigationOption');
    }

    selectedAction(index) {

        console.log("Selected index ////")
        console.log(index)

        if(index === 0){
            this.props.navigation.navigate("aboutus");
        }else if(index === 1){

        }else if(index === 2){
            this.props.navigation.navigate('contactus');
        }
        else if(index === 3){
            this.showActionSheet()
        }
    }

    showActionSheet() {
        console.log("Show action sheet called l..........")
        this.ActionSheet.show()
      }
    deleteME = async(index)=> {
if(index===1){
    this.setState({
        loader: true
    })
    try{

        let response = await api.request('/delete-account', 'get', null, null);
console.log(response);
console.log(JSON.stringify(response));
        if (response.status == 200) {
            response.json().then(async(data) => {
                console.log("delete account ......")
                console.log(data);
                if(data.success){
                    alert(data.message);
                  await AsyncStorage.clear();
                  AsyncStorage.clear();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'registrationNavigationOption' })],
          });
          
          this.props.navigation.dispatch(resetAction);
                    this.props.navigation.navigate('TellUs');
                }
                
            })
        }
        else {
            this.setState({loader: false});
            response.json().then((respons) => {
                alert(Object.values(respons.errors));
            })
        }
    }catch (error) {
        this.state.loader(false);
        alert(error.message);
    }
}
    }
    render(){

        return(
        <View style={{flex:1}}>
             <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Do you really want to Delete your account'}
                    options={[ 'No','Yes',]}
                    cancelButtonIndex={0}
                    destructiveButtonIndex={1}
                    onPress={(index) => { this.deleteME(index)}}
                />
             {this.state.loader ? <MyActivityIndicator /> : null}
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', 
                              height:50, backgroundColor:'#fff', zIndex:1}}>
                        <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        />
                    <TitleText
                            size = {15}
                            color = 'black'
                            weight = '400'
                            alignment = 'center'
                            title = 'Settings'
                            />

                    <View style={{width:90}}/>
                </View>
          
            <BottomBorderView 
                horizontal={0}
                top={0}
                />
                  <View style={{ marginTop:10
                        }}>
                             {
                                 this.state.arrData.map((item, index)=>{

                                    return(
                                        <View key={index}>
                                            <TouchableOpacity style={{
                                                height:60,
                                                alignItems:'center',
                                                // justifyContent:'center',
                                                flexDirection:'row'
                                            }}onPress={()=>this.selectedAction(index)}>
                                               <Image style={{
                                                   marginLeft:15
                                               }} source={item.image}>
                                                </Image>
                                                <Text style={{
                                                    marginLeft:15,
                                                    // width:width-90
                                                }}>
                                                    {item.name}
                                                </Text>
                                                
                                               
                                            </TouchableOpacity>
                                            <BottomBorderView 
                                                    horizontal={0}
                                                    top={0}
                                                    />
                                        </View>
                                    )
                                 })
                             }
                </View>
              <View style={{position:"absolute",bottom:0, width:'100%',justifyContent:'center',alignItems:'center',padding:10}}>
              <TouchableOpacity style={{
                    backgroundColor:'#69D3A9',
                    width:'90%',
                    // backgroundColor:'#5ACD9E',
                    marginHorizontal:'10%',
                    height:40,
                    borderRadius:20,
                    justifyContent:'center',
                    alignItems:'center',
                    flexDirection:'row',
                    marginTop:height/2-100
                }}onPress={()=>this.logoutAction()}>
                    <Text style={{color:'white', fontSize:14}}>
                        Logout
                    </Text>
                </TouchableOpacity>
              </View>
        </View>
        )}
            
}

const styles = StyleSheet.create({
    mainContainer:{flex:1, marginHorizontal:15, marginVertical:15},
    titleText:{marginTop:20, fontSize:16, marginLeft:20},
    textInput:{fontSize:14,
        height:40,
        marginLeft:10,
        borderRadius:5,
        borderWidth:0.7,
        borderColor:'gray',
        marginTop:5,
        padding:10}
});