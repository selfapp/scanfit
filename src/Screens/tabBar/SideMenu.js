
import React, {Component} from 'react';
import {View, Text, Dimensions, Image, ScrollView, StyleSheet,
TouchableOpacity, AsyncStorage,TouchableNativeFeedback} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import {fetchAssesments} from '../../store/actions/user';
import MyActivityIndicator from '../../Component/activity_indicator';
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker';
import api from '../../../api';
import { connect } from 'react-redux';
import {updateProfile} from '../../store/actions/user';
import { withNavigationFocus } from 'react-navigation';
const width = Dimensions.get("window").width;
class SideMenu extends Component {

    constructor(props) {
        super(props);
        this.state={
            loader:false,
            profilepic:null,
            user:null,
            profileDetail:{

            },
            arrDetails:[{name:'Profile', image:require('../../assets/user-profile.png')},
            {name:'My Assessments', image:require('../../assets/my-assessments.png')},
            {name:'My Equipments', image:require('../../assets/my-equipments.png')},
            {name:'Generate my new plan', image:require('../../assets/workout-plan-icon.png')},
            {name:'Settings', image:require('../../assets/setting.png')}
        ]
        }
    }

    componentWillMount(){ 
      
       let userJson =  this.props.userData;
       console.log("user value")
       console.log(userJson)
       this.setState({user:userJson})
       // console.log(JSON.stringify(value))
       this.setState({profileDetail:userJson,profilepic:userJson.picture});
    }

    selectedAction(index) {

        console.log("Selected index ////")
        console.log(index)

        if(index === 0){
            this.props.navigation.navigate('Profile',{user:this.props.userData,onGoBack:()=>this.refresh()})
        }else if(index === 1){
            this.props.fetchAssesments();
            this.props.navigation.navigate('MyAssesment');
        }else if(index === 2){
             this.props.navigation.navigate('SNEquipmentList',{HomeView:false})
        }else if(index === 3){
            this.props.navigation.navigate('SNWorkout',{fromSetting:true,data:this.state.user})
        }else if(index ===4 ){
            this.props.navigation.navigate('Setting',{fromSetting:true,data:this.state.user})
        }
    }
    calculate_age =(dob)=> { 
        // alert(dob);
        var diff_ms =  Date.now() - dob.getTime();
       // alert(diff_ms)
        var age_dt =   new Date(diff_ms); 
      //alert(Math.abs(age_dt.getUTCFullYear() - 1970));
        return  Math.abs(age_dt.getUTCFullYear() - 1970);
    }
    refresh(){
        let userJson = this.props.userData;
            console.log("user value")
            console.log(userJson)
            this.setState({profileDetail:userJson,profilepic:userJson.picture});
            
    }
    showActionSheet() {
        console.log("Show action sheet called l..........")
        this.ActionSheet.show()
      }
      searchImage = async(imageData)=> {
console.log('iamge Data', imageData);
        this.setState({loader: true});
        let body = {
                "picture": imageData//"data:image/jpeg;base64"
        };
        console.log("body is ")
        console.log(body)

        try {
            console.log(body);
            let response = await api.request('/update-profile-picture', 'post', body);
            console.log(response)
            if (response.status === 200) {

                console.log("response for search image....")
                console.log(response)
                response.json().then((data) => {
                    console.log("final response search image....")
                    console.log(data)
                    this.setState({
                        // arrEqip:data.records,
                        loader: false
                    })
                    alert(JSON.stringify(data.message));
                    let jdata = JSON.stringify(data.user);
                    this.saver(jdata,data);
                    // this.arrEqipInitial= data.records
                    
                });
            }
            else if (response.status === 401) {
                console.log("enter into 401 .,....")
                this.setState({loader: false});
            }
            else {
                console.log("enter into else .,....")
                this.setState({loader: false});
                response.json().then((response) => {
                    // console.log("sayad yaha",response )
                    alert(JSON.stringify(response.errors));
                })
            }
        } catch (error) {
            console.log("enter into error .,....")
            this.setState({loader: false});
            console.log("qwqwqwq")
            alert(error.message);
        }
}
saver = (jdata,data)=>{
    console.log('saver',jdata);
    this.props.updateProfile(false,data.user);
    console.log("new picture", data.user);
    this.setState({profileDetail:data.user,profilepic:data.user.picture});
    console.log('profilepic:',data.user.picture)
    // this.props.navigation.state.params.onGoBack();
    // this.props.navigation.goBack()
    }
      takeImageAction(index){
        console.log("index for selected option", index);
    
        if(index === 0){
            ImagePicker.openCamera({
                // width: 300,
                // height: 400,
                cropping: false,
                compressImageQuality:0.4,
                includeBase64: true,
                mediaType: 'photo'
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                this.searchImage(data)
                // this.props.navigation.navigate('AddWquipment', {image:data})
                console.log(image);
              });
        }else if(index === 1){
            ImagePicker.openPicker({
                // width: 300,
                // height: 400,
                cropping: false,
                includeBase64: true,
                mediaType: 'photo'
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                this.searchImage(data)
                // this.props.navigation.navigate('AddWquipment', {image:data})
                console.log(image);
              });
        }
    
    }

    render(){
        return(
        <View style={{flex:1}}>
            <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Choose option for image'}
                    options={['Camera', 'Gallery', 'Cancel']}
                    cancelButtonIndex={2}
                    destructiveButtonIndex={2}
                    onPress={(index) => { this.takeImageAction(index)}}
                />
                {this.state.loader ? <MyActivityIndicator /> : null}

            <TitleText
                        size = {20}
                        // color = 'black'
                        weight = '400'
                        width = {width-135}
                        alignment = 'center'
                        height = {50}
                        title = 'Menu'
                        top = {10}
                        />
           
            <BottomBorderView 
                horizontal={0}
                top={10}
                />
            <ScrollView style={styles.mainContainer}>
            
                <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}>
                
                </View>
                <View style={styles.profileBoxView}>
                <View style={{
                       height:120,
                       width:120,
                       padding:5,
                       backgroundColor:'white',
                       borderRadius:60,
                       borderColor:'green',
                       borderWidth:1,
                       justifyContent:'center',
                       alignItems:'center',
                       position:'absolute',
                       bottom:140,  
                       alignSelf:'center',
                    //    backgroundColor:'pink'
                    }} 
                    
                    >
                        {
                            (this.state.profilepic!==null?
                                <Image style={{
                                    height:110,
                                    width:110,
                                    borderRadius:50,
                                    // backgroundColor:'gray'
                                }} source={{uri:this.state.profilepic}}>
                                </Image>
                                :
                                <Image style={{
                                    height:100,
                                    width:100,
                                    borderRadius:50,
                                }} source={require('./../../assets/sample.png')}>
                                </Image>
                                )
                        }
                        
                        
                    
                        
                    </View>
                    <TouchableOpacity activeOpacity = {.5} style={{
                            zIndex:1,position:'absolute',right:65,bottom:110,
                            width:100,height:100,
                            //  backgroundColor:'pink',
                            alignItems:'center',justifyContent:'center'
                        }} onPress={()=>this.showActionSheet()}>
                            <Image source={require('../../assets/edit.png')} style={{zIndex:2}}></Image>
                        </TouchableOpacity>
                        { this.props.userData.name && <Text style={{top:80, width:'100%', textAlign:'center', fontWeight:'bold'}}>
                        { this.props.userData.name}
                    </Text>}
                    {/* {this.state.profileDetail &&} */}
                    <Text style={{top:80, width:'100%', textAlign:'center'}}>
                        { this.props.userData.gender}
                    </Text>
                    <BottomBorderView 
                    horizontal={0}
                    top={100}
                    />
                    <View style={{
                        flexDirection:'row',
                    }}>

                        <View style={styles.smallBoxView}>
                            <Text style={{fontSize:16}}>
                                2
                            </Text>
                            <Text style={{color:'gray', fontSize:11}}>
                                Workout
                            </Text>
                        </View>
                        <View style={styles.verticalBorder}/>
                        <View style={styles.smallBoxView}>
                            
                                { this.props.userData && (
                                    <Text style={{fontSize:16}}>
                            {this.props.userData.weight } lbs
                                    </Text>
                                )} 
                          
                            <Text style={{color:'gray', fontSize:11}}>
                                Weight
                            </Text>
                        </View>
                        <View style={styles.verticalBorder}/>
                        <View style={styles.smallBoxView}>
                            <Text style={{fontSize:16}}>
                               { this.props.userData&&this.calculate_age(new Date(this.props.userData.age))}
                            </Text>
                            <Text style={{color:'gray', fontSize:11}}>
                                Age
                            </Text>
                        </View>
                        <View style={styles.verticalBorder}/>
                        
                    </View>
                </View>
                <View style={{
                    marginTop:90,
                    borderRadius:10,
                         borderColor:'lightgray', 
                         borderWidth:0.5,
                        // elevation: 5,
                         shadowColor: '#999666', shadowOffset: {width: 5, height: 5},
                         shadowOpacity: 0.7, shadowRadius: 5,
                         backgroundColor:'white'
                        }}
                         >
                             {
                                 this.state.arrDetails.map((item, index)=>{

                                    return(
                                        <View key={index}>
                                            <TouchableOpacity style={{
                                                height:60,
                                                alignItems:'center',
                                                // justifyContent:'center',
                                                flexDirection:'row'
                                            }}onPress={()=>this.selectedAction(index)}>
                                                <Text style={{
                                                    marginLeft:15,
                                                    width:width-90
                                                }}>
                                                    {item.name}
                                                </Text>
                                                <Image source={item.image}>
                                                </Image>
                                               
                                            </TouchableOpacity>
                                            {/* <BottomBorderView 
                                                    horizontal={0}
                                                    top={0}
                                                    /> */}
                                        </View>
                                    )
                                 })
                             }
                </View>
            </ScrollView>

             
        </View>
        )
        
    }
}

// SideMenu

const mapStateToProps = state =>{
    return{
        userData: state.user.personal,
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        fetchAssesments: () => fetchAssesments(dispatch),
        updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
        wp:(type,userData) => wp(type,userData,dispatch)
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(SideMenu));

const styles = StyleSheet.create({
    mainContainer:{flex:1, marginHorizontal:15, marginVertical:15},
    icon: {
        backgroundColor: '#ccc',
        position: 'absolute',
        right: 0,
        bottom: 0
       },
    profileBoxView:{ top:60, borderRadius:10, minHeight:200,
     borderColor:'lightgray', borderWidth:0.5, zIndex:-90,  elevation: 5,
     shadowColor: '#999666', shadowOffset: {width: 5, height: 5},
     shadowOpacity: 0.7, shadowRadius: 5,
     backgroundColor:'white'},
   
     smallBoxView:{justifyContent:'center', alignItems:'center', width:width/3-11,paddingBottom:10,
     marginTop:20},
    
    verticalBorder:{width:1, backgroundColor:'gray',marginHorizontal:1, marginTop:20}
    
});
