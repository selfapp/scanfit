



import React, { Component } from 'react';
import { View, Text, Dimensions, FlatList, TouchableOpacity, Image,Platform, ScrollView } from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
// import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
// import Video from '../../Component/Video'
import Video from "react-native-video";
import ProgressBar from "react-native-progress/Bar";
import { connect } from 'react-redux';
import { todayworkout, completeExercise, yourActivity } from "../../store/actions/user";
import moment, { months } from 'moment';
import { WebView } from 'react-native-webview';
import { Thumbnail } from 'react-native-thumbnail-video';
import MyActivityIndicator from '../../Component/activity_indicator';
const uri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
const width = Dimensions.get("window").width;
var count = 0
class WelcomePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      error: false,
      empty: false,
      paused: false,
      progress: 0,
      duration: 0,
      focusedScreen:false,
      VideoUri: null
    }
  }
  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', async () =>
   { this.getThem();
    this.setState({ focusedScreen: true })}
    );
    navigation.addListener('willBlur', () =>
    this.setState({ focusedScreen: false })
    );
    
  }
  async getThem() {
    console.log('did mount')
    let Ydate = moment().format('YYYY-MM-DD')
    console.log("date _______________________", moment().format('YYYY-MM-DD'), Ydate)
    await this.props.todayworkout({ "today_date": Ydate });
    setTimeout(() => {
      // console.log("9999999999999999999999------->", this.props.todayWorkout.exercises.length)
    if (this.props.todayWorkout) { if(this.props.todayWorkout.exercises){
        if (this.props.todayWorkout.exercises.warm_up_exercise.length > 0) {
          console.log("----------------------------------------------------------");
          this.setState({
            VideoUri: this.props.todayWorkout.exercises.warm_up_exercise[0].exercise[0].video_url
          })
        }
      }}
    }, 2000);
  }

  handleError = meta => {
    const { error: { code } } = meta;
    // console.log('error',meta);
    let error = "An error has occurred playing this video";
    switch (code) {
      case -11800:
        error = "Could not load video from URL";
        break;
      default:
        error = "Select A video"
    }

    this.setState({
      error,
    });
  };
  handleMainButtonTouch = () => {
    if (this.state.progress >= 1) {
      this.player.seek(0);
    }

    this.setState(state => {
      return {
        paused: !state.paused,
      };
    });
  };

  handleProgressPress = e => {
    const position = e.nativeEvent.locationX;
    const progress = (position / 250) * this.state.duration;
    const isPlaying = !this.state.paused;

    this.player.seek(progress);
  };

  handleProgress = progress => {
    //   console.log(progress);

    this.setState({
      progress: progress.currentTime / this.state.duration,
    });
  };

  handleEnd = () => {
    // this.setState({ paused: true });
  };

  handleLoad = meta => {
    this.setState({
      duration: meta.duration,
    });
  };

  renderItem = ({ item, index }) => {
    return (
      <View >
        <TouchableOpacity
          onPress={() => {
            this.setState({
              VideoUri: `${item.exercise[0].video_url}?id=${item.id}`
            }, () => {
              console.log(this.state.VideoUri)
            })
          }
          }
        >
          <View style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ height: 60, width: 60, alignItems: 'center', justifyContent: 'center' }}>
              <Image
                style={{ height: 60, width: 60, borderRadius: 5 }}
                source={require("../../assets/exercise/1.png")}
              />
                    {/* <Video
                        style={{height: 60, width: 60, borderRadius: 5 }}
                        paused={true}
                        source={{uri: `${item.exercise[0].video_url}` } }
                        ref={(ref) => {
                          this.player = ref
                        }}                                      
                        onBuffer={()=>alert('buffring')}                
                        onError={()=> alert('Error')} 
                        resizeMode="contain"
                         /> */}
                         {/* <Thumbnail url="https://www.youtube.com/watch?v=jFmtXqeE_ig"
                          imageWidth={60}
                          imageHeight={60}
                          iconStyle={{ height: 15, width: 15}}
                         /> */}
            </View>
            <View style={{ flex: 1, marginLeft: 15 }}>
                       {(item.sets&&item.reps_duration)&& <Text style={{ fontSize: 16 }}> {`(${item.sets}  X ${item.reps_duration}) `}{item.exercise[0].title}</Text>}
                       {(!item.sets&&item.reps_duration)&& <Text style={{ fontSize: 16 }}> {`(${item.reps_duration}) `}{item.exercise[0].title}</Text>}
                       {(!item.sets&& !item.reps_duration)&& <Text style={{ fontSize: 16 }}> {item.exercise[0].title}</Text>}
                        
            </View>
            <View>
              {/* <Image
                         source={require('../../assets/options-icon.png')}
                         style={{ tintColor: colors.familyMember}}
                        /> */}
            </View>
          </View>
        </TouchableOpacity>
        <View style={{ height: 0.5, backgroundColor: '#E8E8E8' }} />
      </View>
    );
  }

  empetyList = () => {
    if (count === 0) this.setState({ empty: true })
    count++;
    return (
      <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
        <Text style={{ padding: 20, textAlign: 'center', justifyContent: "center", fontSize: 20 }}>No Exercises Today</Text>
      </View>
    );
  }
  renderSeparater = () => {
    return (
      <View style={{ flex: 1, height: 0.5, backgroundColor: '#f7f7f7' }}></View>
    )
  }

  render() {
    const { width } = Dimensions.get("window");
    const height = width * 0.5625;
    return (
      <View style={{ flex: 1, paddingBottom: 10 }}>

        {this.props.loading ? <MyActivityIndicator /> : null}


        {(this.props.todayWorkout && this.state.focusedScreen)  && (
          this.props.loading ? <View></View> : 
          <View style={{
            flex: 1,
          }}>
            {
              Platform.OS=='ios' ?<WebView
              style={{width: Dimensions.get('window').width,}}
             mediaPlaybackRequiresUserAction={false}
             useWebKit={true}
             originWhitelist={['*']}
             allowsInlineMediaPlayback={true}
             javaScriptEnabled={true}
             domStorageEnabled={true}
             allowsFullscreenVideo={true}
             source={this.state.VideoUri ? { html: `<video playsinline style="width:100%;" controls autoplay src=${this.state.VideoUri} ></video>` } : { html: `<!DOCTYPE html><html lang="en"><body style="background-color:#000;color: white;font-size: 20px;padding-top:30%;"><div style="margin: auto; width: 50%;padding: 10px;" ><h1 style="text-align: center;">Loading...</h1></div></body></html>` }}
           />:
           <WebView
               style={{width: Dimensions.get('window').width,}}
              mediaPlaybackRequiresUserAction={true}
              useWebKit={true}
              originWhitelist={['*']}
              allowsInlineMediaPlayback={true}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              allowsFullscreenVideo={true}
              source={this.state.VideoUri ? { uri: this.state.VideoUri} : { html: `<!DOCTYPE html><html lang="en"><body style="background-color:#000;color: white;font-size: 20px;padding-top:30%;"><div style="margin: auto; width: 50%;padding: 10px;" ><h1 style="text-align: center;">Loading...</h1></div></body></html>` }}
            />
            }
          </View>
        )}
        <ScrollView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <View style={{ height: 50, backgroundColor:'#d3d3d3', justifyContent:'center'}}>
            <Text style={{ fontSize: 18, fontWeight:'700', marginLeft: 15}} >Warm up exercise</Text>
         </View>
          {/* this.props.todayWorkout */}
          {this.props.todayWorkout && this.props.todayWorkout.exercises ? <FlatList
            data={this.props.todayWorkout.exercises.warm_up_exercise}
            // data={[1,2,3]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderItem}
            ListEmptyComponent={this.empetyList}
            ListEmptyComponent={this.empetyList}
            ItemSeparatorComponent={this.renderSeparater}
          />
            :
            <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
              <Text style={{ padding: 20, textAlign: 'center', justifyContent: "center", fontSize: 20 }}>No Exercises Today</Text>
            </View>

          }

          
        <View style={{ height: 50, backgroundColor:'#d3d3d3', justifyContent:'center'}}>
            <Text style={{ fontSize: 18, fontWeight:'700', marginLeft: 15}} >Workout</Text>
         </View>        
         {/* this.props.todayWorkout */}
         {this.props.todayWorkout && this.props.todayWorkout.exercises ? <FlatList
           data={this.props.todayWorkout.exercises.workout}
           // data={[1,2,3]}
           keyExtractor={(item, index) => index.toString()}
           renderItem={this.renderItem}
           ListEmptyComponent={this.empetyList}
           ListEmptyComponent={this.empetyList}
           ItemSeparatorComponent={this.renderSeparater}
         />
           :
           <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
             <Text style={{ padding: 20, textAlign: 'center', justifyContent: "center", fontSize: 20 }}>No Exercises Today</Text>
           </View>

         }

<View style={{ height: 50, backgroundColor:'#d3d3d3', justifyContent:'center'}}>
            <Text style={{ fontSize: 18, fontWeight:'700', marginLeft: 15}} >Cool down</Text>
         </View>        
         {/* this.props.todayWorkout */}
         {this.props.todayWorkout && this.props.todayWorkout.exercises ? <FlatList
           data={this.props.todayWorkout.exercises.cool_down}
           // data={[1,2,3]}
           keyExtractor={(item, index) => index.toString()}
           renderItem={this.renderItem}
           ListEmptyComponent={this.empetyList}
           ListEmptyComponent={this.empetyList}
           ItemSeparatorComponent={this.renderSeparater}
         />
           :
           <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
             <Text style={{ padding: 20, textAlign: 'center', justifyContent: "center", fontSize: 20 }}>No Exercises Today</Text>
           </View>

         }

        </View>
        {this.props.todayWorkout && this.props.todayWorkout.exercises && (

          <Button
            horizontal='8%'
            top={10}
            radius={20}
            backgColor='#69D3A9'
            height={40}
            weight='bold'
            textColor='white'
            titleSize={16}
            title='Completed'
            buttonAction={() => {
              var { todayWorkout } = this.props
              var todayDate = moment().toDate();
              var todayMoment = moment(todayDate);
              const todayDateFormatted = todayMoment.format('YYYY-MM-DD');
              console.log(todayWorkout);
              var exids = [];
              for (let index = 0; index < this.props.todayWorkout.exercises.warm_up_exercise.length; index++) {
                exids.push(this.props.todayWorkout.exercises.warm_up_exercise[index].exercise[0].id);

              }
              for (let index = 0; index < this.props.todayWorkout.exercises.workout.length; index++) {
                exids.push(this.props.todayWorkout.exercises.workout[index].exercise[0].id);

              }
              for (let index = 0; index < this.props.todayWorkout.exercises.cool_down.length; index++) {
                exids.push(this.props.todayWorkout.exercises.cool_down[index].exercise[0].id);

              }
              console.log({
                program_id: todayWorkout.programe.id,
                day_numder: todayWorkout.day_number,
                count_day: todayWorkout.count_days,
                exercise_date: todayDateFormatted,
                workout_data: todayWorkout.workoutData,
                exercises: exids
              })
              this.props.completeExercise({
                programe_id: todayWorkout.programe.id,
                day_number: todayWorkout.day_number,
                count_days: todayWorkout.count_days,
                exercise_date: todayDateFormatted,
                workout_data: todayWorkout.workoutData,
                exercises: exids
              })
            }
            }
          />)}
          </ScrollView>
      </View>
    )

  }
}
const mapStateToProps = state => {
  return {
    todayWorkout: state.user.todayWorkout,
    CompleteExercice: state.user.CompleteExercice,
    YourActivity: state.user.YourActivity,
    loading: state.user.loading
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    todayworkout: (data) => todayworkout(data, dispatch),
    completeExercise: (data) => completeExercise(data, dispatch),
    yourActivity: () => yourActivity(dispatch)

  }
};
export default connect(mapStateToProps, mapDispatchToProps)(WelcomePage);


// import React, { Component } from "react";
// import {
//   AppRegistry,
//   StyleSheet,
//   Text,
//   View,
//   TouchableWithoutFeedback,
//   Dimensions,
// } from "react-native";

// import Video from "react-native-video";
// import ProgressBar from "react-native-progress/Bar";
// // import LightVideo from "./lights.mp4";

// // import Icon from "react-native-vector-icons/FontAwesome";

// function secondsToTime(time) {
//   return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
// }

// export default class rnvideo extends Component {
//   state = {
//     paused: false,
//     progress: 0,
//     duration: 0,
//     VideoUri:'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
//   };

//   handleMainButtonTouch = () => {
//     if (this.state.progress >= 1) {
//       this.player.seek(0);
//     }

//     this.setState(state => {
//       return {
//         paused: !state.paused,
//       };
//     });
//   };

//   handleProgressPress = e => {
//     const position = e.nativeEvent.locationX;
//     const progress = (position / 250) * this.state.duration;
//     const isPlaying = !this.state.paused;

//     this.player.seek(progress);
//   };

//   handleProgress = progress => {
//     this.setState({
//       progress: progress.currentTime / this.state.duration,
//     });
//   };

//   handleEnd = () => {
//     this.setState({ paused: true });
//   };

//   handleLoad = meta => {
//     this.setState({
//       duration: meta.duration,
//     });
//   };

//   render() {
//     const { width } = Dimensions.get("window");
//     const height = width * 0.5625;

//     return (
//       <View style={styles.container}>
//         <View>
//           <Video
//             paused={this.state.paused}
//             source={{ uri: this.state.VideoUri}}
//             style={{ width: "100%", height }}
//             resizeMode="contain"
//             onLoad={this.handleLoad}
//             onProgress={this.handleProgress}
//             onEnd={this.handleEnd}
//             ref={ref => {
//               this.player = ref;
//             }}
//           />
//           <View style={styles.controls}>
//             <TouchableWithoutFeedback onPress={this.handleMainButtonTouch}>
//               {/* <Icon name={!this.state.paused ? "pause" : "play"} size={30} color="#FFF" /> */}
//               <Text>></Text>
//             </TouchableWithoutFeedback>
//             <TouchableWithoutFeedback onPress={this.handleProgressPress}>
//               <View>

//               </View>
//             </TouchableWithoutFeedback>

//             <Text style={styles.duration}>
//               {secondsToTime(Math.floor(this.state.progress * this.state.duration))}
//             </Text>
//           </View>
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     paddingTop: 250,
//   },
//   controls: {
//     backgroundColor: "rgba(0, 0, 0, 0.5)",
//     height: 48,
//     left: 0,
//     bottom: 0,
//     right: 0,
//     position: "absolute",
//     flexDirection: "row",
//     alignItems: "center",
//     justifyContent: "space-around",
//     paddingHorizontal: 10,
//   },
//   mainButton: {
//     marginRight: 15,
//   },
//   duration: {
//     color: "#FFF",
//     marginLeft: 15,
//   },
// });

// AppRegistry.registerComponent("rnvideo", () => rnvideo);
// © 2019 GitHub, Inc.
// Terms
// Privacy
// Security
// Status
// Help
// Contact GitHub
// Pricing
// API
// Training
// Blog
// About
