import React, { Component } from 'react';
import {
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { connect } from 'react-redux';
import Button from '../../Component/Button';
import { updateProfile, wp } from "../../store/actions/user";
const width = Dimensions.get("window").width;
import BottomBorderView from '../../Component/BottomBorderView';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
class wpTellUs extends Component {
  constructor(props){
    super(props);
    this.state={
      subject:null,
      message:null,
      loading:false
    }
  }

  submit=async()=>{
    const {subject,message} = this.state;
    var alertText = null
    if(subject == null){
      alertText="Please enter a subject."
    }else  if(message == null){
      alertText="Please enter a message."
    }else {
      this.setState({loading:true})
      const body={
        subject:subject,
        message:message
      }
      let response = await api.request('/contact-us', 'POST', body);
      console.log(response)
      if (response.status == 200) {
        response.json().then((data) => {
          console.log(data)
          if(data.success){
            this.setState({
              loading:false,
              message:null,
              subject:null
            },async()=>{
              this.messageText.clear();
              this.SubjectText.clear();
              alert(data.message)
            });
            
          }

        })
    }
    else {
      this.setState({loading: false});
      response.json().then((respons) => {
          alert(Object.values(respons.errors));
      })
  }
}
   if(alertText!= null){
    alert(alertText);
   }
  }
 
  render() {
    const {userData}= this.props
    return (
      <View style={{flex:1}}>
        {this.state.loading ? <MyActivityIndicator /> : null}
        <View style={{
          flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
          height: 50, backgroundColor: '#fff', zIndex: 1
        }}>
          <BackButton
            buttonAction={() => this.props.navigation.goBack()}
          />
          <TitleText
            size={20}
            // color='black'
            weight='400'
            alignment='center'
            title='About Us'
          />

          <View style={{ width: 90 }} />
        </View>
            <BottomBorderView 
                horizontal={0}
                top={10}
                />
                <ScrollView contentContainerStyle={{alignItems:'center', justifyContent:'center',padding:10}}>
                    <View style={style.card}>
                        <Text style={{
                            fontFamily: 'Montserrat-Regular',
                            fontWeight:'800',
                            paddingVertical:10
                        }}>Lorem Ipsum</Text>
                         <Text style={{
                            fontFamily: 'Montserrat-Regular',
                            // fontWeight:'800'
                        }}>Lorem Ipsum Lorem Ipsum,</Text>
                        <Text style={{
                            fontFamily: 'Montserrat-Regular',
                            // fontWeight:'800'
                        }}>Lorem Ipsum Lorem Ipsum, 345674</Text>
                        <Text style={{
                            fontFamily: 'Montserrat-Regular',
                            // fontWeight:'800'
                        }}>Lorem Ipsum.</Text>
                        
                        <Text style={{
                            fontFamily: 'Montserrat-Regular',
                            paddingVertical:10,
                            // fontWeight:'800'
                        }}>+123456789</Text>

                   </View>
                    
                    
                </ScrollView>
                <View style={{padding:10}}>
               
                </View>
      </View>
    );
  }
}

const style= StyleSheet.create({
    card:{
     backgroundColor:'white',
     padding:10,
     borderRadius:10,
     width:'90%'
    },

     
})
const mapStateToProps = state =>{
  return{
      userData: state.user.personal,
      wp:state.user.wp
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
      updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
      wp:(type,userData) => wp(type,userData,dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(wpTellUs);