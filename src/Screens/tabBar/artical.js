


import React, { Component } from 'react';
import {
    View, Text, Dimensions, AsyncStorage,
    Image, StyleSheet, TouchableOpacity,WebView
} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import MyActivityIndicator from '../../Component/activity_indicator';
import TitleText from '../../Component/TitleText';
import BackButton from '../../Component/BackButton';
import { ScrollView } from 'react-native-gesture-handler';
import API from '../../../redAPI'
import WebComp from '../../Component/webviewComponent'
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width
export default class Setting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id:null,
            loader: false,
            Blog: '',
            postadeOn: '',
            TitleText: ''
        }
    }

    async componentWillMount() {
        var blogID = await this.props.navigation.getParam("BlogID");
        this.setState({id:blogID})
        this.setState({
            loader: true
        })
        // await API.getBlogDetails(blogID).then((res) => res.json()).then(async jsonRes => {
        //     this.setState({ loader: false });
        //     console.log(JSON.stringify(jsonRes))
        //     if (jsonRes.success) {
        //         this.setState({
        //             Blog: jsonRes.records.description,
        //             postadeOn: jsonRes.records.posted_on,
        //             TitleText: jsonRes.records.title

        //         })
        //     }
        // })
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }





    render() {
        if(this.state.id !== null){
            return(
                <WebComp id={this.state.id}/>
            )
        }
return(
    <View>
    
    </View>
)
//         return (
//             <View style={{ flex: 1 }}>
//                 {this.state.loader ? <MyActivityIndicator /> : null}
//                 <View style={{
//                     flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
//                     height: 50, backgroundColor: '#fff', zIndex: 1
//                 }}>
//                     <BackButton
//                         buttonAction={() => this.backButtonAction()}
//                     />
//                     <TitleText
//                         size={15}
//                         color='black'
//                         weight='400'
//                         alignment='center'
//                         title='ScanFIT'
//                     />

//                     <View style={{ width: 90 }} />
//                 </View>

//                 <BottomBorderView
//                     horizontal={0}
//                     top={0}
//                 />
//                 <ScrollView style={{ margin: 10 }}>

//                     <View>
//                         <Text style={{ fontWeight: 'bold', color: '#525252', fontSize: 18,textAlign:'justify' ,fontFamily: 'Montserrat-Regular'}}>{this.state.TitleText}</Text>
//                         <View style={{ flexDirection: 'row' ,paddingTop:5}}>
//                             <Image source={require('../../assets/time.png')} style={{ height: 10, width: 10, marginRight: 7 }} />
//                             <Text style={{  color: '#525252', fontSize: 10, textAlign: 'right',fontFamily: 'Montserrat-Regular' }}>{this.state.postadeOn}</Text>

//                         </View>

//                     </View>
//                     <View style={{ paddingVertical: 10, }}>
//                 <WebComp body={'<h1>hi!</h1>'}></WebComp>
//                         {/* <Text style={{ textAlign: 'justify',fontFamily: 'Montserrat-Regular',fontSize: 13 }}>
//                             It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)
// </Text> */}
//                     </View>
//                     {/* <Image
//                         style={{ width: '100%', height: 200, resizeMode: 'cover' }}
//                         source={{ uri: 'https://image.freepik.com/free-photo/fitness-woman-doing-exercises-press-top-view-cute-girl-workout-gym_118454-5117.jpg' }}

//                     /> */}
//                 </ScrollView>

//             </View>
//         )
    }

}

const styles = StyleSheet.create({
    mainContainer: { flex: 1, marginHorizontal: 15, marginVertical: 15 },
    titleText: { marginTop: 20, fontSize: 16, marginLeft: 20 },
    textInput: {
        fontSize: 14,
        height: 40,
        marginLeft: 10,
        borderRadius: 5,
        borderWidth: 0.7,
        borderColor: 'gray',
        marginTop: 5,
        padding: 10
    }
});