



import React, {Component} from 'react';
import {View, Text, Dimensions,
     Image, StyleSheet} from 'react-native';
import BottomBorderView from '../../Component/BottomBorderView';
import Button from '../../Component/Button';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
import Moment from "moment";
import BackButton from '../../Component/BackButton';
import {Calendar} from "react-native-calendars";

const width = Dimensions.get("window").width;
export default class CalendarSelection extends Component {

    constructor(props) {
        super(props);
        this.state={
            arrSelectedDays:[],
            loader:false,
            currentDate:'',
            originalMarkedDates:''   
        }
    }

    componentWillMount() {

      var todayDate = Moment().toDate()
      var todayMoment = Moment(todayDate)
      const todayDateFormatted = todayMoment.format('YYYY-MM-DD')

      this.setState({
        date:todayDateFormatted
      });
    }

    setAction(){
      // alert("hat ja")
      // this.props.navigation.navigate('TabNavigator');

    }

    setCalendar(selectedDate){
      let markedDates = {}
    // Create an array with all days from _startDate to _endDate
    var endDateCheck = Moment(selectedDate).add(60, 'days')
    const endDateFormatted = endDateCheck.format('YYYY-MM-DD')
    // console.log("end date formatted", endDateFormatted)
    // let currentDate = Moment(selectedDate)
    let endDate = Moment(endDateFormatted)
    const tapDateFormatted = currentDate.format('YYYY-MM-DD')

      // console.log("selected date value")
      // console.log(currentDate)
      // console.log("selected end value")
      // console.log(endDate)
    var daysValue = this.props.navigation.state.params.selectedDays;
    // console.log("array selected days", daysValue)
    while (currentDate <= endDate) {
        const day = currentDate.day()

            var isWeekend = false;

            if(daysValue.length === 1){
                isWeekend = daysValue[0] === day ? true :false
            }else if(daysValue.length === 2){
              isWeekend = (daysValue[0] === day || daysValue[1] === day) ? true :false
            }else if(daysValue.length === 3){
              isWeekend = (daysValue[0] === day || daysValue[1] === day || daysValue[2] === day) ? true :false
            }else if(daysValue.length === 4){
              isWeekend = (daysValue[0] === day || daysValue[1] === day  || daysValue[2] === day  || daysValue[3] === day) ? true :false
            }else if(daysValue.length === 5){
              isWeekend = (daysValue[0] === day || daysValue[1] === day  || daysValue[2] === day || daysValue[3] === day || daysValue[4] === day) ? true :false
            }else if(daysValue.length === 6){
              isWeekend = (daysValue[0] === day || daysValue[1] === day  || daysValue[2] === day || daysValue[3] === day || daysValue[4] === day || daysValue[5] === day) ? true :false
            }else if(daysValue.length === 7){
              isWeekend = true
            }
            
              const currentDateFormatted = currentDate.format('YYYY-MM-DD')
              let markup = {}
              if(tapDateFormatted === currentDateFormatted){
                markup.selected = true, markup.marked = true, markup.selectedColor ='#34BF83'
              }
              
              if (isWeekend) {
                // markup.selected = true, markup.marked = true,// markup.selectedColor ='#34BF83'
                  // markup.disabled = true
                  markup.marked = true, markup.dotColor= '#34BF83', markup.activeOpacity= 0
                  // markup.dots = []
                  // markup.dots = [color= 'blue', selectedDotColor= 'blue']

              }else{
                // markup.selected = false, markup.marked = false,// markup.selectedColor ='blue'
                // markup.disabled = true
                // markup.dots = []
               markup.marked = true, markup.dotColor= '#ABE773', markup.activeOpacity= 0
              }
      
              markedDates[currentDateFormatted] = markup
              currentDate = Moment(currentDate).add(1, 'days')
    }

    this.setState({
        originalMarkedDates: markedDates,
    })
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    nextButtonAction() {

    }

    getCurrentDateParams(date2) {
        console.log("called params" + date2);
    
        var dateString = Moment(date2).format("L");
    
        const datex = Moment(dateString).format("YYYY-MM-DD");
        this.setCalendar(datex)
        console.log("current time after parse " + datex);
    
        const date = Moment(dateString).format("D MMM YYYY");
        const dateInitail = Moment(dateString).format("DD");
    
        const dateMiddle = Moment(dateString).format("MMMM YYYY");
    
        const dateLast = Moment(dateString).format("dddd"); 
       
      }

    render(){

      // console.log("current date", this.state.date)
        return(
        <View style={{flex:1}}>
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', 
                              height:50, backgroundColor:'#fff', zIndex:1}}>
                        <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        />
            </View>
          
            <BottomBorderView 
                horizontal={0}
                top={0}
                />
            {this.state.loader ? <MyActivityIndicator /> : null}
            
            <Text style={styles.titleText}>
                When do you want to start?
            </Text>
            <View
              style={{
                // backgroundColor: colors.gray01,
                marginLeft: 10,
                marginRight: 10,
                marginTop: 10,
                // borderTopLeftRadius: 15,
                // borderTopRightRadius: 15,
                // borderBottomLeftRadius: 15,
                // borderBottomRightRadius: 15,
                marginBottom: 10,
                elevation: 1,
                // borderWidth: 0.5,
                // borderColor: 'gray'
                // height: 350
              }}
            >
              <Calendar
                style={{
                  // marginHorizontal: 5,
                  // marginVertical: 5
                }}
                theme={{
                  backgroundColor: '#484848',
                  calendarBackground: "#ffffff",
                  textSectionTitleColor: "black",
                  selectedDayBackgroundColor: '#34BF83',
                  selectedDayTextColor: '#484848',
                  todayTextColor: '#F76B1C',
                  dayTextColor: 'black',
                  textDisabledColor: 'lightgray',
                  dotColor: 'green',
                  selectedDotColor: 'black',
                  arrowColor: '#F76B1C',
                  monthTextColor: 'black',

                  textMonthFontWeight: "bold",
                  textDayFontSize: 14,
                  textMonthFontSize: 14,
                  textDayHeaderFontSize: 14
                }}
                
               // markingType={"custom"}
                // markedDates={this.state.originalMarkedDates}
                markedDates={{
                  '2018-09-25': {selected: true, marked: true, selectedColor: 'blue'},
                  '2018-09-26': {marked: true},
                  '2018-09-27': {marked: true, dotColor: 'red', activeOpacity: 0},
                  '2018-09-30': {disabled: true, disableTouchEvent: true}
                }}
                // Initially visible month. Default = Date()
                current={this.state.date}
                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                minDate={this.state.date}//{"2019-08-30"}
                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                //  maxDate={this.state.currentDateForCalender}
                // Handler which gets executed on day press. Default = undefined
                onDayPress={day => {
                  console.log("selected day", day);
                  console.log("m called", day);

                  this.getCurrentDateParams(day.timestamp);
                  console.log("m called", day);
                }}
                // Handler which gets executed on day long press. Default = undefined
                onDayLongPress={day => {
                  console.log("selected day", day);
                }}
                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                monthFormat={"MMMM yyyy"}
                // Handler which gets executed when visible month changes in calendar. Default = undefined
                onMonthChange={month => {
                  console.log("month changed", month);
                }}
                // Hide month navigation arrows. Default = false
                // hideArrows={false}
                //  renderArrow={direction => <Arrow />}
                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                // renderArrow={direction => <Arrow />}
                // Do not show days of other months in month page. Default = false
                // hideExtraDays={true}
                // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                // day from another month that is visible in calendar page. Default = false
                disableMonthChange={false}
                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                firstDay={1}
                // Hide day names. Default = false
                // hideDayNames={false}
                // Show week numbers to the left. Default = false
                // showWeekNumbers={false}
                // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                onPressArrowLeft={substractMonth => substractMonth()}
                // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                onPressArrowRight={addMonth => addMonth()}
              />
            </View>
            <Button
                horizontal = '8%'
                top = {50}
                radius = {20}
                backgColor = '#69D3A9'
                height = {40}
                textColor = 'white'
                titleSize = {16}
                title = 'All set'
                buttonAction = {()=>this.setAction()}
                />
        </View>
        )}
            
}

const styles = StyleSheet.create({
    titleText:{marginTop:20, fontSize:16, marginHorizontal:'10%', textAlign:'center', fontWeight:'400'},
});