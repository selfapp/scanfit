import React from "react";
import {View, Text, Dimensions,TextInput,Image,Platform,TouchableOpacity,Alert} from 'react-native';
import { ChatManager, TokenProvider, } from '@pusher/chatkit-client';
import { connect } from 'react-redux';
import { GetChatingUsers, FindScanfitContacts } from "../../../store/actions/user";
import { CreateNewRoom , ChatRoomLeft} from "../../../store/actions/chat";
import { GiftedChat } from "react-native-gifted-chat";
import ActionSheet from 'react-native-actionsheet'
import BottomBorderView from '../../../Component/BottomBorderView';
import API from '../../../../redAPI';
import ImagePicker from 'react-native-image-crop-picker';
    const CHATKIT_TOKEN_PROVIDER_ENDPOINT = 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/9e36562c-69f8-4389-bb39-8e80bef6860c/token';
    const CHATKIT_INSTANCE_LOCATOR = 'v1:us1:9e36562c-69f8-4389-bb39-8e80bef6860c';
    const CHATKIT_ROOM_ID = 'f677b254-45a3-42a3-ae00-cece0e4e38ba';
class MyChat extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      messages: [],
      otherParty:null,
      otherPartPresence:'offline',
      receiverid:null,
      loading:false,
      image_url:null

    };
  }
      

      componentWillUnmount(){
        this.props.ChatRoomLeft();
      }
     async componentDidMount() {
    this.ChatConfig();
      }
      onDelete=(currentUser)=>{
        alert(this.props.RoomID);
      }
      ChatConfig= async()=>{
        var userIDCurrent = this.props.userData.mobile
    var currentRoom = this.props.RoomID
    console.log(currentRoom);
        // var user =  await this.props.navigation.getParam('HomeView')
        // console.log("user name",this.props.navigation.getParam('data'));
        this.setState({
            otherParty:this.props.navigation.getParam('data'),
            otherimage:this.props.navigation.getParam('picOther')
        })
        const tokenProvider = new TokenProvider({
          url: CHATKIT_TOKEN_PROVIDER_ENDPOINT,
        });
    
        const chatManager = new ChatManager({
          instanceLocator: CHATKIT_INSTANCE_LOCATOR,
          userId:userIDCurrent ,
          tokenProvider: tokenProvider,
        });
    
        chatManager
          .connect()
          .then(currentUser => {
            this.currentUser = currentUser;
            this.currentUser.subscribeToRoom({
              roomId: currentRoom,
              hooks: {
                // onMessage: message => (alertmessage.text),
                onMessage: this.onReceive,
                onMessageDeleted: this.onDelete,
                 onPresenceChanged: this.onchangePresence,
              },
            });
          })
          .catch(err => {
            alert(err)
            this.props.navigation.pop();
            console.log(err);
          });
      }

      onchangePresence= async(state,user)=>{
       var presenceStoreLocal =user.presenceStore;
        var listuser = await Object.keys(user.presenceStore)
         console.log(user)
        if(listuser[0]!==this.props.userData.mobile){
          var user=listuser[1];
          var statusme= (presenceStoreLocal[user])
          this.setState({receiverid:listuser[0],otherPartPresence:statusme},()=>{
            console.log(this.state)
          })
        }else{
          console.log(listuser[1])
          var user=listuser[1];
          var statusme= (presenceStoreLocal[user])
          this.setState({receiverid:user,otherPartPresence:statusme},()=>{
            console.log(this.state)
          })
        }
        // for (let i = 0; i < listuser.length; i++) {
        //  if(listuser[i]===this.props.userData.mobile){
        //  }else{
        //    console.log(user.presenceStore.listuser[i])
        //  }
          
        // }
        // console.log(JSON.stringify(user.presenceStore))
      }
      onReceive = async(data) => {
         console.log("data-------------------------", data);
        var incomingMessage ={};
        const { id, senderId, text, createdAt,attachment } = data;
        if(attachment){
          if(attachment.type==="image"){
            incomingMessage = {
              _id: id,
              image: attachment.link,
              createdAt: new Date(createdAt),
              user: {
                _id: senderId,
                name: senderId,
                avatar:this.state.otherimage
              },
            };

          }
        }else{
          incomingMessage = {
            _id: id,
            text: text,
            createdAt: new Date(createdAt),
            user: {
              _id: senderId,
              name: senderId,
              avatar:this.state.otherimage
            },
          };
        }
        
    
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, incomingMessage),
        }));
      };
      onSend = (messages = []) => {
        console.log("000000000----",messages[0])
        var currentRoom = this.props.RoomID

        messages.forEach(message => {
          if(this.state.image_url){
            this.currentUser.sendMultipartMessage({
              roomId: currentRoom,
              parts: [
                {
                  type: "image/gif",
                  url: this.state.image_url,
                },
              ],
            })
            .then(messageId => {
              this.setState({
                image_url:null
              });
              console.log(`Added message to--------------------------- ${currentRoom}`)
            })
            .catch(err => {
              console.log(`Error adding message to ${myRoom.name}: ${err}`)
            })
          }else{
            this.currentUser
            .sendMessage({
              text: message.text,
              roomId: currentRoom,
            })
            .then(() => {})
            .catch(err => {
              console.log(err);
            });
          }
         
        });
      };
  showActionSheet=()=> {
        console.log("Show action sheet called l..........")
        this.ActionSheet.show()
      }

      takeImageAction(index){
        console.log("index for selected option", index);
    
        if(index === 0){
            ImagePicker.openCamera({
                // width: 300,
                // height: 400,
                cropping: false,
                compressImageQuality:0.2,
                includeBase64: true,
                mediaType: 'photo'
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                this.UploadImage(data)
                // this.props.navigation.navigate('AddWquipment', {image:data})
                console.log(image);
              });
        }else if(index === 1){
            ImagePicker.openPicker({
                // width: 300,
                // height: 400,
                cropping: false,
                includeBase64: true,
                compressImageQuality:0.2,
                mediaType: 'photo'
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                this.UploadImage(data)
                // this.props.navigation.navigate('AddWquipment', {image:data})
                console.log(image);
              });
        }
    
    }
    UploadImage=async(data)=>{
      this.setState({loading:true});
      let response = await API.UploadImage({ "image": data});
      console.log(response)
      response.json().then((data) => {
        console.log(data)
        this.setState({loading:false});
      if(data.success) { 
       this.setState({
         image_url:data.image_url
       },()=>{
        this._giftedChat.onSend({text:'hi'})
       })
         }
    }); 
    }
    

    deleteMessage =async (Message,contaxt)=>{
      this.setState({loading:true})
      console.log(Message,"---------------",contaxt)
      var Body ={
        message_id : contaxt._id.toString(),
  room_id : this.props.RoomID
      }
      console.log(Body);
      let response = await API.DeleteMessage(Body);
      response.json().then((data) => {
        console.log(data)
        this.setState({loading:false});
      if(data.success) { 
        this.ChatConfig();
         }
    }); 
    }

      render() {
       
        var userIDCurrent = this.props.userData.mobile
        return (
          <View style={{flex:1}}>
             <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Choose option for image'}
                    options={['Camera', 'Gallery', 'Cancel']}
                    cancelButtonIndex={2}
                    destructiveButtonIndex={2}
                    onPress={(index) => { this.takeImageAction(index)}}
                />
                 {this.state.loading ? <MyActivityIndicator /> : null}

              <View style={{
          flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
          height: 50, backgroundColor: '#fff', zIndex: 1
        }}>
          <BackButton
            buttonAction={() => this.props.navigation.pop()}
          />
          <TitleText
            size={20}
            // color='black'
            weight='400'
            alignment='center'
            title={this.state.otherParty}
          />

          <View style={{ width: 90 }} />
        </View>
        <BottomBorderView horizontal={0} top={10}/>
                <GiftedChat messages={this.state.messages} 
                isLoadingEarlier={true}
                onLongPress={(message,contaxt)=>{Alert.alert("Delete this Message","Once deleted, message can not be recoverd",[
                  { text: 'Cancel', onPress: () => {
                    this.setState({
                        selected:null,
                    })
                   }},
                { text: 'OK', onPress: () => {
                  this.deleteMessage(message,contaxt);
                 }} 
              ],
              { cancelable: false },)}}
                 ref={component => this._giftedChat = component}
        onSend={messages => this.onSend(messages)}
             user={{
               _id: userIDCurrent
             }}
             renderActions={()=>{ return(
              <View style={{height:50,alignItems:'center',justifyContent:'center',padding:10,elevation:3,}}>
                  <TouchableOpacity 
              onPress={()=>this.showActionSheet()} 
              ><Image source={require('../../../assets/attach.png')} style={{tintColor:'green',height:25,width:25}}/></TouchableOpacity>
             
              </View>
              )}}
        />
        
          </View>
        );
      }
    }
    const mapStateToProps = state =>{
      return{
          Contacts: state.user.Contacts,
          chatLoader: state.user.chatLoader,
          chattingLoading: state.chat.chattingLoading,
          userData:state.user.personal,
          RoomID: state.chat.roomId
      }
    };
    
    const mapDispatchToProps = (dispatch) =>{
      return{
          GetChatingUsers:(searchTerm) => GetChatingUsers(searchTerm,dispatch),
          FindScanfitContacts:(Contacts) => FindScanfitContacts(Contacts,dispatch),  
          CreateNewRoom:(body,name,navigation) => CreateNewRoom(body,name,navigation,dispatch) ,
          ChatRoomLeft:()=>ChatRoomLeft(dispatch)
      }
    };
    export default connect(mapStateToProps, mapDispatchToProps)(MyChat);