

import React, {Component} from 'react';
import {View, Text, Dimensions,TextInput,Image,Platform,FlatList, TouchableOpacity} from 'react-native';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import MyActivityIndicator from '../../../Component/activity_indicator';
import api from '../../../../api';
import { connect } from 'react-redux';
import { GetChatingUsers, FindScanfitContacts } from "../../../store/actions/user";
import { CreateNewRoom } from "../../../store/actions/chat";
import Icon from 'react-native-vector-icons/MaterialIcons';
import Contacts from 'react-native-contacts';


const width = Dimensions.get("window").width;

class userList extends Component {

    constructor(props) {
        super(props);
        this.state={
            loader:false,
            contactList:[],
            serchText:'',
            MatchingContacts:[]
        }
    }
    componentDidMount(){
      this.setState({
        MatchingContacts:this.props.Contacts
      })
    }

   
    _renderItem=({item,index})=>{
    return(
        <TouchableOpacity onPress={()=>{
            this.props.CreateNewRoom({
            sender_id:this.props.userData.mobile,
            receiver_id:item.new_mobile,
            sf_status:item.sf_status
            },
            item.first_name + ' ' + item.last_name,
            this.props.userData.mobile,
            item.picture!= null?item.picture : 'http://18.219.161.44/uploads/profile/default-user-pic.png',
            this.props.navigation)
            // this.props.navigation.navigate('ChatPage',{data:item.first_name + ' ' + item.last_name})
        }
            } key={item.mobile}>

        <View style={{ flexDirection: 'row', width: '100%', alignSelf: 'center', height: 70, justifyContent: 'space-between', paddingHorizontal: 10,}}>
           {
               (item.picture!= null && !item.picture=="" )?
            <Image style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'black', alignSelf: 'center', }} source={{uri:item.picture}}/>
            :
             <View style={{ height:50, width:50, borderRadius: 25, backgroundColor: 'rgba(0,0,0,0.1)', alignSelf: 'center',overflow:'hidden'}}>
            <Image style={{ height: 50, width: 50, borderRadius: 25, alignSelf: 'center',resizeMode:'contain'}} source={require('../../../assets/user-profile.png')} borderRadius={25}/>
           </View>
           }
           
            <View style={{flexDirection:'row'}}>
            <View style={{ height: 90, width: '75%', alignSelf: 'center', marginLeft: 10, justifyContent:'center'}}>

<Text style={{fontWeight:"400", fontSize:15,maxWidth:'75%', fontFamily:'Montserrat-Regular',paddingVertical:5}} numberOfLines={1}>
{item.first_name} { item.last_name}
</Text>
<Text style={{fontWeight:"400", fontSize:12,maxWidth:'75%', fontFamily:'Montserrat-Regular',paddingVertical:5}} numberOfLines={1}>
{item.new_mobile}
</Text>
</View>
<View style={{height: 90,width: '15%', alignItems: 'center',justifyContent:'center' }} >
    {
        item.sf_status?<Image source={require('../../../assets/message.png')} style={{tintColor:'green'}}/>:<Text style={{fontSize:15,color:'green'}}>Invite</Text>
    }
    </View>

            </View>
        </View>
        <BottomBorderView horizontal={'2.5%'} top={10} />
    </TouchableOpacity>
    )

}
SearchedText= async (serchText)=>{
this.setState({serchText})
if(this.SearchedText.length>0){
    
var serchResult = await this.props.Contacts.filter((item)=>{
if(item.first_name.toLowerCase().includes(serchText.toLowerCase())){
    return item
} 
})
this.setState({
    MatchingContacts:[]
})
this.setState({
    MatchingContacts:serchResult.length>0?serchResult:this.props.Contacts
})
}
else{
    this.setState({
        MatchingContacts:this.props.Contacts
      })
}
}
    render(){
if(this.props.Contacts.length>0){
    return(

        <View style={{flex:1}}>
             <View style={{
          flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
          height: 50, backgroundColor: '#fff', zIndex: 1
        }}>
          <BackButton
            buttonAction={() => this.props.navigation.pop()}
          />
          <TitleText
            size={20}
            // color='black'
            weight='400'
            alignment='center'
            title='Contacts'
          />

          <View style={{ width: 90 }} />
        </View>
         {/* <TitleText
                        size = {20}
                        // color = 'black'
                        weight = '400'
                        width = {width-135}
                        alignment = 'center'
                        height = {50}
                        title = 'Contacts'
                        top = {10}
                        /> */}
           
            <BottomBorderView horizontal={0} top={10}/>
                <View style={{borderColor:'lightgray', borderRadius:25,borderWidth:1,height:50,width:'90%',alignSelf:'center', marginVertical:15,borderColor:'lightgray', borderWidth:0.5, zIndex:-90,  elevation: 5,
     shadowColor: '#999666', shadowOffset: {width: 1, height: 1},
     shadowOpacity: 0.7, shadowRadius: 2,
     backgroundColor:'white',flexDirection:"row",alignItems:'center',padding:10, marginBottom:20}}>
 <Icon name='search' size={30} color='lightgray'/>
 <TextInput placeholder="search..." style={{padding:3, width:'85%'}} onChangeText={(text)=>this.SearchedText(text)}/>
                </View>  
                    {/* <Text>{this.state.serchText} {this.state.serchText.length} {JSON.stringify(this.state.MatchingContacts)}</Text> */}
   
      { this.state.MatchingContacts.length>0 && <FlatList 
        data={this.state.MatchingContacts}
        renderItem = {this._renderItem }
        />}
    
  
                {this.state.loader || this.props.chatLoader ||this.props.chattingLoading ? <MyActivityIndicator /> : null}
             
        </View>
       
       )
}
    return(
        <View>
             
        </View>
    )
        
        
    }
}
const mapStateToProps = state =>{
    return{
        Contacts: state.user.Contacts,
        chatLoader: state.user.chatLoader,
        chattingLoading: state.chat.chattingLoading,
        userData:state.user.personal
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        GetChatingUsers:(searchTerm) => GetChatingUsers(searchTerm,dispatch),
        FindScanfitContacts:(Contacts) => FindScanfitContacts(Contacts,dispatch),  
        CreateNewRoom:(body,name,myid,urpic,navigation) => CreateNewRoom(body,name,myid,urpic,navigation,dispatch) 
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(userList);