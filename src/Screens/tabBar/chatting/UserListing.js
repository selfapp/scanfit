


import React, {Component} from 'react';
import {View, Text, Dimensions,TextInput,Image,Platform,TouchableOpacity,FlatList,Alert} from 'react-native';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import MyActivityIndicator from '../../../Component/activity_indicator';
import api from '../../../../api';
import { connect } from 'react-redux';
import { GetChatingUsers, FindScanfitContacts,  } from "../../../store/actions/user";
import { GetChatRoom, ChatRoomSelected } from '../../../store/actions/chat';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Contacts from 'react-native-contacts';
import { PermissionsAndroid } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import { ChatManager, TokenProvider, } from '@pusher/chatkit-client';
import API from '../../../../redAPI'
const CHATKIT_TOKEN_PROVIDER_ENDPOINT = 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/9e36562c-69f8-4389-bb39-8e80bef6860c/token';
    const CHATKIT_INSTANCE_LOCATOR = 'v1:us1:9e36562c-69f8-4389-bb39-8e80bef6860c';

const width = Dimensions.get("window").width;

class userList extends Component {

    constructor(props) {
        super(props);
        this.state={
            loader:false,
            contactList:[],
            serchText:null,
            MatchingContacts:[],
            selected:null
        }
    }
    componentDidMount(){
       this.chat();
        this.props.GetChatRoom(this.props.userMobile);

        this.setState({
            loader:true
        })
        console.log("os", Platform.OS)
        if(Platform.OS == 'ios'){
        Contacts.checkPermission((error,result)=>{
            console.log(error);
            console.log(result); 
            if(result === 'denied'){
                alert('Please provide permission to access contacts');
                this.setState({
                    loader:false
                })
                // Contacts.requestPermission((err, permission) => {
                //     console.log(err);
                //     console.log(permission); 
                //   })
            }else{
                Contacts.getAll((err, contacts) => {
                        this.setState({
                            loader:false
                        })
                        console.log(err)
                        if (err) {
                          throw err;
        
                        }
                        this.filterContacts(contacts)
                        // this.setState({
                        //     contactList:contacts
                        // },()=>{console.log(this.state.contactList)})
                      })
            }
        })
        }else{
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                  'title': 'Contacts',
                  'message': 'This app would like to view your contacts.'
                }
              ).then(() => {
              
            Contacts.getAll((err, contacts) => {
                this.setState({
                    loader:false
                })
                console.log(err)
                if (err) {
                  throw err;

                }
                this.filterContacts(contacts)
                // this.setState({
                //     contactList:contacts
                // },()=>{console.log(this.state.contactList)})
              })
              })

        }
    }

    filterContacts= async (rawData)=>{
        this.setState({
            loader:false
        })
await this.selectContacts(rawData,(data)=>{
    console.log(data)
 this.props.FindScanfitContacts(data)
// /chatkit/get-sf-users-from-contacts

})
    }

    selectContacts=async(data,callBack)=>{
        console.log(':::::::::::::::::::::::::',data)
        arrayOfContacts=[]
        for (let i = 0; i <  data.length; i++) {
            for (let j = 0; j < data[i].phoneNumbers.length; j++) {
               arrayOfContacts.push({number:data[i].phoneNumbers[j].number,name:data[i].displayName?data[i].displayName:data[i].givenName});
            }
            
        }
        callBack(arrayOfContacts);
    }
    _renderItem=({item,index})=>{
    return(
        <TouchableOpacity 
        onPress={()=>{
             this.props.ChatRoomSelected(
                 item.id,
                 this.props.userMobile === item.custom_data.sender.id ?item.custom_data.receiver.name:item.custom_data.sender.name,
                 this.props.userMobile === item.custom_data.sender.id ?item.custom_data.receiver.avatar_url :item.custom_data.sender.avatar_url,
                 this.props.navigation

             )
        }}
        onLongPress={()=>{
            this.setState({
                selected:item.id,
            }),
            Alert.alert("Delete this Chat","Once deleted, Chat can not be recoverd",[
                { text: 'Cancel', onPress: () => {
                    this.setState({
                        selected:null,
                    })
                   }},
                { text: 'OK', onPress: () => {
                  this.DeleteRoom();
                 }} 
                ])
        }}
        >
<View style={[this.state.selected===item.id && { backgroundColor:'lightgray'},{ flexDirection: 'row', width: '95%', alignSelf: 'center', height: 70, justifyContent: 'center', paddingHorizontal: 20, alignContent: 'space-around' }]}>
           {
                this.props.userMobile === item.custom_data.sender.id ?
                <Image style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'black', alignSelf: 'center', }} source={{uri:item.custom_data.receiver.avatar_url}}/>:
                
                <Image style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'black', alignSelf: 'center', }} source={{uri:item.custom_data.sender.avatar_url}}/>
           }
           
            <View style={{ height: 90, width: '80%', alignSelf: 'center', marginLeft: 10, justifyContent:'center'}}>

                <Text style={{fontWeight:"400", fontSize:15,maxWidth:'80%', fontFamily:'Montserrat-Regular',paddingVertical:5}} numberOfLines={1}>
                {this.props.userMobile === item.custom_data.sender.id ?item.custom_data.receiver.name:item.custom_data.sender.name
                }
                </Text>
                <Text style={{fontWeight:"400", fontSize:12,maxWidth:'80%', fontFamily:'Montserrat-Regular',paddingVertical:5}} numberOfLines={1}>
                {this.props.userMobile === item.custom_data.sender.id ?
                item.custom_data.receiver.id:item.custom_data.sender.id
            }
                </Text>
            </View>

        </View>
        <BottomBorderView horizontal={'2.5%'} top={10} />
    </TouchableOpacity>
    )

}

chat=()=>{
    const tokenProvider = new TokenProvider({
        url: CHATKIT_TOKEN_PROVIDER_ENDPOINT,
      });
      const chatManager = new ChatManager({
        instanceLocator: CHATKIT_INSTANCE_LOCATOR,
        userId:this.props.userMobile ,
        tokenProvider: tokenProvider,
      });

      chatManager.connect({
        onAddedToRoom: room => {
            this.props.GetChatRoom(this.props.userMobile);
        }
      })


      
      chatManager
      .connect()
      .then(currentUser => {
        this.currentUser = currentUser;
        currentUser.getJoinableRooms().then(rooms => {
        })
        .catch(err => {
          console.log(`Error getting joinable rooms: ${err}`)
        })
        })
        .catch(err => {
          console.log(err);
        });
}
SearchedText=(serchText)=>{
    console.log(serchText)
    this.setState({serchText})
    // MatchingContacts first_name, last_name mobile
    console.log(this.props.ChatRoomsList)
    var serchResult =this.props.ChatRoomsList.filter((item)=>{
        if(this.props.userMobile!==item.custom_data.sender.id){
            var name = item.custom_data.sender.name.toLowerCase()
            if(name.includes(serchText.toLowerCase())){
                return item
            }
            if(item.custom_data.sender.id.includes(serchText)){
                return item
            }
        }else if(this.props.userMobile!==item.custom_data.receiver.id){
            var name = item.custom_data.receiver.name.toLowerCase()
            if(name.includes(serchText.toLowerCase())){
                return item
            }
            if(item.custom_data.receiver.id.includes(serchText)){
                return item
            }
        }
    // if(item.first_name.includes(serchText)){
    //     return item
    // } else if(item.last_name.includes(serchText)){
    //     return item
    // } else if (item.mobile.includes(serchText)){
    //     return item
    // }
    })
    this.setState({
        MatchingContacts:serchResult
    })
    console.log("SearchResult",serchResult)
    
    }
    DeleteRoom=async()=>{
        if(this.state.selected){
         var response= await   API.DeleteChatRoom({room_id:this.state.selected});
         response.json().then((data) => {
            console.log(data)
            this.setState({loading:false});
          if(data.success) { 
            setTimeout(() => {
                this.props.GetChatRoom(this.props.userMobile);
            }, 5000);
            this.setState({
                selected:null,
            })
             }
        }); 
        }
    }
    render(){

        return(
        <View style={{flex:1}}>
        <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
            <View style={{height:25,width:30}}></View>
        <TitleText
                        size = {20}
                        // color = 'black'
                        weight = '400'
                        width = {width-135}
                        alignment = 'center'
                        height = {50}
                        title = 'Chat'
                        top = {10}
                        />
           <TouchableOpacity style={{marginRight:7,marginTop:10,height:30,width:30,backgroundColor:'green', borderRadius:25,alignItems:'center',justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("MyContacts")}>
<Image source={require('../../../assets/message.png')} style={{tintColor:'white'}}/>
</TouchableOpacity> 
        </View>
            <BottomBorderView horizontal={0} top={10}/>
                <View style={{borderColor:'lightgray', borderRadius:25,borderWidth:1,height:50,width:'90%',alignSelf:'center', marginVertical:15,borderColor:'lightgray', borderWidth:0.5, zIndex:-90,  elevation: 5,
     shadowColor: '#999666', shadowOffset: {width: 1, height: 1},
     shadowOpacity: 0.7, shadowRadius: 2,
     backgroundColor:'white',flexDirection:"row",alignItems:'center',padding:10, marginBottom:20}}>
 <Icon name='search' size={30} color='lightgray'/>
 <TextInput placeholder="search..." style={{padding:3, width:'85%'}} onChangeText={(text)=>this.SearchedText(text)}/>


                </View>
                {
                    (this.state.serchText===null || this.state.serchText.length==0)?
                    <FlatList 
data={this.props.ChatRoomsList}
renderItem = {this._renderItem} 
ListHeaderComponent={<BottomBorderView horizontal={'2.5%'} top={0} />}
/>
:
<FlatList 
data={this.state.MatchingContacts}
renderItem = {this._renderItem} 
ListHeaderComponent={<BottomBorderView horizontal={'2.5%'} top={0} />}
/>

                }

<View style={{position:'absolute',bottom:10,right:10,zIndex:90}}>

    </View> 
                {this.state.loader || this.props.chatLoader || this.props.chatingWait || this.props.contactLoader? <MyActivityIndicator /> : null}
             
        </View>
        )
        
    }
}
const mapStateToProps = state =>{
    return{
        ChatUserList: state.user.ChatUserList,
        chatLoader: state.user.chatLoader,
        userMobile: state.user.personal.mobile,
        ChatRoomsList : state.chat.chatRooms,
        chatingWait : state.chat.chattingLoading,
        contactLoader: state.user.contactLoader
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        GetChatingUsers:(searchTerm) => GetChatingUsers(searchTerm,dispatch),
        FindScanfitContacts:(Contacts) => FindScanfitContacts(Contacts,dispatch),   
        GetChatRoom:(id) => GetChatRoom(id,dispatch),
        ChatRoomSelected:(id,name,urpic,navigation) => ChatRoomSelected (id,name,urpic,navigation,dispatch)
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(userList);


