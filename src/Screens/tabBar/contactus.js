import React, { Component } from 'react';
import {
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { connect } from 'react-redux';
import Button from '../../Component/Button';
import { updateProfile, wp } from "../../store/actions/user";
const width = Dimensions.get("window").width;
import BottomBorderView from '../../Component/BottomBorderView';
import MyActivityIndicator from '../../Component/activity_indicator';
import api from '../../../api';
class wpTellUs extends Component {
  constructor(props){
    super(props);
    this.state={
      subject:null,
      message:null,
      loading:false
    }
  }

  submit=async()=>{
    const {subject,message} = this.state;
    var alertText = null
    if(subject == null){
      alertText="Please enter a subject."
    }else  if(message == null){
      alertText="Please enter a message."
    }else {
      this.setState({loading:true})
      const body={
        subject:subject,
        message:message
      }
      let response = await api.request('/contact-us', 'POST', body);
      console.log(response)
      if (response.status == 200) {
        response.json().then((data) => {
          console.log(data)
          if(data.success){
            this.setState({
              loading:false,
              message:null,
              subject:null
            },async()=>{
              this.messageText.clear();
              this.SubjectText.clear();
              alert(data.message)
            });
            
          }

        })
    }
    else {
      this.setState({loading: false});
      response.json().then((respons) => {
          alert(respons.errors);
      })
  }
}
   if(alertText!= null){
    alert(alertText);
   }
  }
 
  render() {
    const {userData}= this.props
    return (
      <View style={{flex:1}}>
        {this.state.loading ? <MyActivityIndicator /> : null}
        <View style={{
          flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
          height: 50, backgroundColor: '#fff', zIndex: 1
        }}>
          <BackButton
            buttonAction={() => this.props.navigation.goBack()}
          />
          <TitleText
            size={20}
            // color='black'
            weight='400'
            alignment='center'
            title='Contact Us'
          />

          <View style={{ width: 90 }} />
        </View>
            <BottomBorderView 
                horizontal={0}
                top={10}
                />
                <ScrollView contentContainerStyle={{alignItems:'center', justifyContent:'center',padding:10}}>
                    <View style={style.card}>
                    <View style={style.header}>
                 <Image style={{ height: 25,  marginRight:5}} source={require('../../assets/name.png')} resizeMode='contain' />
                              <Text style={style.headerText}>Subject</Text>
                          <TextInput style={[style.detailText,{paddingLeft:5,minWidth:170}]} placeholder={""} ref={input => { this.SubjectText = input }}underlineColorAndroid='transparent' autoCorrect={false}              onChangeText={subject => this.setState({subject})}/>
                          </View>
                          <BottomBorderView horizontal={15} top={0} />
                          <View style={style.header}>
                 <Image style={{ height: 25,  marginRight:5}} source={require('../../assets/name.png')} resizeMode='contain' />
                              <Text style={style.headerText}>Message</Text>
                          </View>
                          <View style={{paddingHorizontal:15,height:180}}>
                          <TextInput 
                          style={{height:150,flex:1}}
                          placeholder={"Type here..."} 
                          multiline = {true}  
                          ref={input => { this.messageText = input }}
                          underlineColorAndroid='transparent' 
                          autoCorrect={false} 
                          numberOfLines={7} 
                          onChangeText={message => this.setState({message})}
                          />
                          </View>
                    </View>

                    
                </ScrollView>
                <View style={{padding:10}}>
               <Button
                 horizontal='8%'
                 top={10}
                 radius={20}
                 backgColor='#69D3A9'
                 height={40}
                 weight='bold'
                 textColor='white'
                 titleSize={16}
                 title='Submit'
                 buttonAction={() => this.submit()}
               />
                </View>
      </View>
    );
  }
}

const style= StyleSheet.create({
    card:{
        borderColor:'lightgray', borderWidth:0.5, zIndex:-90,  elevation: 5,
     shadowColor: '#999666', shadowOffset: {width: 5, height: 5},
     shadowOpacity: 0.7, shadowRadius: 5,
     backgroundColor:'white',
     padding:10,
     borderRadius:10,
     width:'90%'
    },
    header: {
        padding: 15,
        flexDirection:'row',
        height:50,
        alignItems:'center',
        // backgroundColor:'red'
      },
      headerText: {
        fontSize: 13,
        fontWeight: '300',
        marginRight:7,
        marginLeft:7,
      },
      detailText: {
        fontSize: 13,
        minWidth:50,
        maxWidth:'70%',
        height:40
      },
      separator: {
        height: 0.5,
        backgroundColor: '#808080',
        marginLeft: 30,
        marginRight: 30,
      },
})
const mapStateToProps = state =>{
  return{
      userData: state.user.personal,
      wp:state.user.wp
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
      updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
      wp:(type,userData) => wp(type,userData,dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(wpTellUs);