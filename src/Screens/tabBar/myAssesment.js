import React, { Component } from 'react';
// import TitleText from '../../Component/TitleText';
import BackButton from '../../Component/BackButton';
import { AlertIOS, TextInput, AppRegistry, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView, Image, Picker, ActivityIndicator } from 'react-native';
import Video from 'react-native-video';
// import { FloatingAction } from "react-native-floating-action";
import Accordion from 'react-native-collapsible/Accordion';
import BottomBorderView from '../../Component/BottomBorderView';
import TitleText from '../../Component/TitleText';
import Button from '../../Component/Button';
import { connect } from 'react-redux';
import { fetchAssesments, PostAssesment } from "../../store/actions/user";
import MyActivityIndicator from '../../Component/activity_indicator';
// 15 x (Maximum heart rate/Resting heart rate)

const WindowSize = Dimensions.get("window");
const SECTIONS = [
  {
    title: 'General',
    uid: 'gen',
    content: [{ Title: "Resting heart rate", icon: require('../../assets/Heart.png'), unit: 'BPM' }, { Title: "Max Heart Rate", icon: require('../../assets/gym.png'), unit: 'BPM' }, { Title: "VO2 max", icon: require('../../assets/weight-scale.png'), unit: 'Kg/Min' }, { Title: "Body fat", icon: require('../../assets/body.png'), unit: 'BPM' }],
  },
  {
    title: 'Upper Body Assesment',
    uid: 'uba',
    content: [{ Title: "Compound / Chest", icon: require('../../assets/gym.png'), unit: 'BPM' }, { Title: "Compound / Back", icon: require('../../assets/gym.png'), unit: 'BPM' }, { Title: "Compound / Triceps", icon: require('../../assets/gym.png'), unit: 'BPM' }],
  },
  {
    title: 'Lower Body Assesment',
    uid: 'lba',
    content: [{ Title: "Compound / Quads", icon: require('../../assets/gym.png'), unit: 'BPM' }, { Title: "Compound / Glutes", icon: require('../../assets/gym.png'), unit: 'BPM' }, { Title: "Compound / Homstrings", icon: require('../../assets/weight-scale.png'), unit: 'Kg/Min' }],
  },
];

const actions = [

  {
    text: "1x",
    // icon: require("../../assets/arrow.png"),
    color: '(rgba(255,255,255,0))',
    name: "bt_language",
    position: 1
  },
  {
    //   text: "Location",
    color: '(rgba(255,255,255,0.5))',
    icon: require("../../assets/arrow.png"),
    name: "bt_room",
    position: 3
  },
  {
    //   text: "Video",
    icon: require("../../assets/arrow.png"),
    name: "bt_videocam",
    position: 4,
    color: '(rgba(255,255,255,0.5))',
  }
];

class Assesments extends Component {
  // static navigationOptions  = ({ navigation }) =>({header:null}),});
  constructor(props) {
    super(props);
    this.onLoad = this.onLoad.bind(this);
    this.onProgress = this.onProgress.bind(this);
    this.onBuffer = this.onBuffer.bind(this);
  }
  state = {
    rate: 1,
    volume: 1,
    muted: false,
    resizeMode: 'contain',
    duration: 0.0,
    currentTime: 0.0,
    controls: false,
    paused: true,
    skin: 'custom',
    ignoreSilentSwitch: null,
    isBuffering: false,
    activeSections: [],
    generalInput: [],
    upperBodyInput: [],
    lowerBodyInput: [],
    weightType: "Lbs",
    showControlls: true,
    loader: false,
    videoUrl: "",
    videoIndex:0
  };
  componentWillUnmount() {

  }

  onLoad(data) {
    this.setState({ duration: data.duration });
  }
  onProgress(data) {
    this.setState({ currentTime: data.currentTime });
  }
  onBuffer({ isBuffering }) {
    this.setState({ isBuffering });
  }
  getCurrentTimePercentage() {
    if (this.state.currentTime > 0) { return parseFloat(this.state.currentTime) / parseFloat(this.state.duration); } else { return 0; }
  }
  renderSkinControl(skin) {
    const isSelected = this.state.skin == skin;
    const selectControls = skin == 'native' || skin == 'embed';
    return (
      <TouchableOpacity onPress={() => {
        this.setState({
          controls: selectControls,
          skin: skin
        })
      }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
          {skin}
        </Text>
      </TouchableOpacity>
    );
  }
  renderRateControl(rate) {
    const isSelected = (this.state.rate == rate);
    return (
      <TouchableOpacity onPress={() => { this.setState({ rate: rate }) }}><Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
        {rate}x
</Text>
      </TouchableOpacity>
    )
  }
  renderResizeModeControl(resizeMode) {
    const isSelected = (this.state.resizeMode == resizeMode);
    return (
      <TouchableOpacity onPress={() => { this.setState({ resizeMode: resizeMode }) }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
          {resizeMode}
        </Text>
      </TouchableOpacity>
    )
  }
  renderVolumeControl(volume) {
    const isSelected = (this.state.volume == volume);
    return (
      <TouchableOpacity onPress={() => { this.setState({ volume: volume }) }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
          {volume * 100}%
</Text>
      </TouchableOpacity>
    )
  }
  renderIgnoreSilentSwitchControl(ignoreSilentSwitch) {
    const isSelected = (this.state.ignoreSilentSwitch == ignoreSilentSwitch);
    return (
      <TouchableOpacity onPress={() => { this.setState({ ignoreSilentSwitch: ignoreSilentSwitch }) }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
          {ignoreSilentSwitch}
        </Text>
      </TouchableOpacity>
    )
  }
  renderCustomSkin() {
    const flexCompleted = this.getCurrentTimePercentage() * 100;
    const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.fullScreen}
          onPress={() => {

            this.setState({ showControlls: !this.state.showControlls })
          }
          }

        >
          {
            this.state.showControlls &&
            <View style={[styles.fullScreen, { backgroundColor: 'rgba(0,0,0,0.7)', zIndex: 34, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
              <View style={{ padding: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', width: '100%' }}>
                <TouchableOpacity style={styles.playercontrols} 
                   onPress={() =>{
                    if(this.state.videoIndex > 0){
                    this.setState({ videoUrl: this.props.MyAssesment.videos[this.state.videoIndex-1], videoIndex: this.state.videoIndex-1 })
                  }
                   }
                  }
                >
                  <Image source={require('../../assets/skip_next.png')} style={{ transform: [{ rotate: '180deg' }], }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.playercontrols} onPress={() => {
                  this.setState({ paused: !this.state.paused }, () => {
                    this.setState({ showControlls: !this.state.showControlls })
                  })
                }}>
                  {this.state.paused ? <Image source={require('../../assets/play.png')} /> : <Image source={require('../../assets/pause.png')} />}
                </TouchableOpacity>
                <TouchableOpacity style={styles.playercontrols} onPress={() => {
                  if(this.state.videoIndex < (this.props.MyAssesment.videos.length-1)){
                      var temp = (this.state.videoIndex === (this.props.MyAssesment.videos.length-1)) ? this.state.videoIndex : this.state.videoIndex+1
                    this.setState({ videoUrl: this.props.MyAssesment.videos[temp], videoIndex: temp })
                  }
                   }
                }
                >
                  <Image source={require('../../assets/skip_next.png')} />
                </TouchableOpacity>
              </View>
            </View>
          }

          <Video
            source={{ uri: this.state.videoUrl }}

            style={styles.fullScreen}
            rate={this.state.rate}
            paused={this.state.paused}
            volume={this.state.volume}
            muted={this.state.muted}
            ignoreSilentSwitch={this.state.ignoreSilentSwitch}
            resizeMode={this.state.resizeMode}
            onLoad={this.onLoad}
            onBuffer={this.onBuffer}
            onProgress={this.onProgress}
            // onEnd={() => { AlertIOS.alert('Done!') }}
            repeat={true}
            ref={component => this.player = component}
          />
        </TouchableOpacity>
        <View style={styles.controls}>
          <View style={styles.generalControls}>
          </View>
          <View style={styles.generalControls}>
            {/* <View style={styles.rateControl}>
{this.renderRateControl(0.5)}
{this.renderRateControl(1.0)}
{this.renderRateControl(2.0)}
</View> */}
            {/* volume controll percentage */}
            {/* <View style={styles.volumeControl}>
{this.renderVolumeControl(0.5)}
{this.renderVolumeControl(1)}
{this.renderVolumeControl(1.5)}
</View> */}


            {/* <View style={styles.resizeModeControl}>
{this.renderResizeModeControl('cover')}{this.renderResizeModeControl('contain')}{this.renderResizeModeControl('stretch')}
</View> */}
          </View>
          <View style={styles.trackingControls}>
            <TouchableOpacity style={{ height: 15, width: 20, left: 0, bottom: 0 }} onPress={() => {
              this.setState({ muted: !this.state.muted })
            }}>
              {
                this.state.muted ?
                  <Image source={require('../../assets/volume_down.png')} style={{ height: 15, width: 20, resizeMode: 'contain', position: 'absolute', zIndex: 90 }} />
                  :
                  <Image source={require('../../assets/volume_up.png')} style={{ height: 15, width: 20, resizeMode: 'contain', position: 'absolute', zIndex: 90 }} />
              }
            </TouchableOpacity>
            <View style={styles.progress}>
              <View style={[styles.innerProgressCompleted, { flex: flexCompleted }]} />
              <View style={[styles.innerProgressRemaining, { flex: flexRemaining }]} />

            </View>
            {/* <View style={{height:15,width:"20%",backgroundColor:'red',flexDirection:'row'}}> */}

            {/* <View style={{width:'30%'}}>

<FloatingAction
 
 color={'rgba(255,255,255,0.4)'}
 distanceToEdge={0}
 margin={2}
  buttonSize={30}
//   position={'left'}
  actionsPaddingTopBottom={2}
     actions={actions}
     onPressItem={name => {
     }}
   />
</View> */}
            {/* <FloatingAction
 
 color={'rgba(255,255,255,0.4)'}
 distanceToEdge={0}
 margin={2}
  buttonSize={30}
//   position={'left'}
  actionsPaddingTopBottom={2}
     actions={actions}
     onPressItem={name => {
     }}
   /> */}
            {/* </View> */}
          </View>
        </View>
      </View>);

  }


  // accordian
  _renderSectionTitle = section => {
    return (
      <View style={styles.content}>
        {/* <Text>78</Text> */}
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View >
        <BottomBorderView
          horizontal={0}
          top={0}
        />
        {/* <Text>{section.title}</Text> */}
        <View style={{
          flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
          height: 50, backgroundColor: '#fff', zIndex: 1, paddingHorizontal: 10
        }}>
          {/* <BackButton
                        buttonAction = {()=> this.backButtonAction()}   
                        /> */}
          <TitleText
            size={15}
            color='black'
            weight='400'
            alignment='center'
            title={section.title}
          />

          <View style={{ width: 90 }} />
        </View>

        <BottomBorderView
          horizontal={0}
          top={0}
        />
      </View>
    );
  };
  handelTextInput = async (value, id, index, type) => {
    const { lowerBodyInput, upperBodyInput, generalInput } = this.state

    if (id === 'gen') {
      if (index != 2) {
        var gen = generalInput;
        gen[index] = value;
        if (gen[0] & gen[1]) {
          if (gen[0] !== 0 & gen[1] !== 0) {
            var vo2 = 15 * (gen[1] / gen[0]);
            gen[2] = vo2.toFixed(2)
          }
        }
        this.setState({ generalInput: gen }, () => {
        })
      } else {

      }
    }
    if (id === 'uba') {
      var uba = upperBodyInput;
      if (type === 0) {
        uba[index].weight = value;
      } else {
        uba[index].reps = value;
      }
      this.setState({ upperBodyInput: uba }, () => {
        console.log("upper",this.state.upperBodyInput)

      })
    }
    if (id === 'lba') {
      var lba = lowerBodyInput;
      if (type === 0) {
        lba[index].weight = value;
      } else {
        lba[index].reps = value;
      }
      // lba[index]=value;
      this.setState({ lowerBodyInput: lba }, () => {
        console.log("lower",this.state.lowerBodyInput)
      })
    }
  }
 
  async componentDidMount() {
    this.props.fetchAssesments()
    var gen = []; var lba = []; var uba = [];
    setTimeout(() => {
      this.props.AssesmentList.map((section) => {
        var sid = section.uid;
        section.content.map((item, index) => {
          if (sid === 'gen') {
              if('my_assessment' in this.props.MyAssesment && 'general' in  this.props.MyAssesment.my_assessment && this.props.MyAssesment.my_assessment.general.length> 0){
                gen.push(this.props.MyAssesment.my_assessment.general[index]);
              }else{
                gen.push(null);
              }
            this.setState({
              generalInput: gen
            })
          }
          if (sid === 'lba') {
  
            if( 'my_assessment' in this.props.MyAssesment && 'lower_input' in this.props.MyAssesment.my_assessment && this.props.MyAssesment.my_assessment.lower_input.length > 0){
              lba.push(this.props.MyAssesment.my_assessment.lower_input[[index]]);
            }else{
              lba.push({ weight: 0, reps: 0 });
             }            
            this.setState({
              lowerBodyInput: lba
            })
          }
          if (sid === 'uba') {
            if( 'my_assessment' in this.props.MyAssesment && 'upper_input' in this.props.MyAssesment.my_assessment && this.props.MyAssesment.my_assessment.upper_input.length > 0){
              uba.push(this.props.MyAssesment.my_assessment.upper_input[[index]]);
            }else{
              uba.push({ weight: 0, reps: 0 });
             } 
           
            this.setState({
              upperBodyInput: uba
            })
          }
        })
      })
    }, 1000);
    if(this.props.MyAssesment && this.props.MyAssesment.videos.length > 0){
      this.setState({videoUrl: this.props.MyAssesment.videos[this.state.videoIndex]})
    }
  }
  
  
  _renderContent = section => {
    return (
      <View>
        {section.uid!=='gen'
      &&
      <View>
      <View style={{flexDirection:'row',height:50,width:WindowSize.width,paddingHorizontal:10,justifyContent:'space-between'}}>
           {/* <View style={{paddingHorizontal:5,alignItems:'center',justifyContent:'center',width:30,height:40}} /> */}
             <View style={{height:40,justifyContent:'center',width:WindowSize.width-210}} />
             <View style={{alignItems:'center',justifyContent:'center',marginRight:10}}> 
             <TouchableOpacity style={styles.topuchableType }  onPress={() =>  
              {if(this.state.weightType === 'Lbs'){
                this.setState({weightType: "Kg"}) 
              }else{
                this.setState({weightType: "Lbs"})
              }
            }
              }>
              <Text>{this.state.weightType}</Text>
            </TouchableOpacity>
             </View>
             <View style={{alignItems:'center',justifyContent:'center',}}> 
                 <View style={styles.topuchableType }>
                  <Text style={{textAlign:"center"}}>Reps.</Text>
              </View>
          
             </View>
           </View>
         </View>
      }
        {section.content.map((item,index)=>{
          return(
           <View>
             <View style={{flexDirection:'row',height:50,width:WindowSize.width,paddingHorizontal:10,justifyContent:'space-between'}}>
            {
              section.uid === 'gen' &&  <View style={{paddingHorizontal:5,alignItems:'center',justifyContent:'center',width:30,height:40}}>
              <Image source={item.icon}></Image>
                </View>
            }
               <View style={{height:40,justifyContent:'center',width:WindowSize.width-210}}>
             <Text style={{textAlign:'left'}}>{item.Title}</Text>
               </View>
               <View style={{alignItems:'center',justifyContent:'center',marginRight:10}}> 
            { section.uid === 'gen' && (index === 2)?
            // <TextInput
            // keyboardType={'decimal-pad'}
            // value={this.state.generalInput[index]}
            //  onChangeText={(e)=>{this.handelTextInput(e,section.uid,index,0)}}
            //  style={
            //   {height:37,alignItems:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
            //  }
            //  >
  
            //  </TextInput>
             <View  style={
                 {height:37,justifyContent:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
                }>
  <Text
             >{this.state.generalInput[2]}</Text>
              </View>
  
             :
            (
              section.uid==='gen' ? <TextInput
              keyboardType={'decimal-pad'}
              onChangeText={(e)=>{this.handelTextInput(e,section.uid,index,0)}}
              value={this.state.generalInput[index]}
              style={
               {height:37,alignItems:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
              }
              />:
              (
                section.uid === 'lba'?
                <TextInput
              keyboardType={'decimal-pad'}
              onChangeText={(e)=>{this.handelTextInput(e,section.uid,index,0)}}
              //value={this.state.lowerBodyInput[index]&& this.state.lowerBodyInput[index].weight}

            value={this.state.lowerBodyInput[index]&& this.state.lowerBodyInput[index].weight}
              style={
               {height:37,alignItems:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
              }
              />:
              <TextInput
              keyboardType={'decimal-pad'}
              onChangeText={(e)=>{this.handelTextInput(e,section.uid,index,0)}}
              value={this.state.upperBodyInput[index]&& this.state.upperBodyInput[index].weight}

            //  value={this.state.generalInput[index]}
              style={
               {height:37,alignItems:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
              }
              />
              )
            )
             }
               </View>
               {section.uid !== 'gen' && <View style={{alignItems:'center',justifyContent:'center',}}> 
             {section.uid ==='lba'? <TextInput
             keyboardType={'decimal-pad'}
             value={this.state.lowerBodyInput[index]&& this.state.lowerBodyInput[index].reps}
  
            // value={section.uid ==="uba"?this.state.upperBodyInput[index].reps : this.state.lowerBodyInput[index].reps}
             onChangeText={(e)=>{this.handelTextInput(e,section.uid,index,1)}}
             style={
              {height:37,alignItems:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
             }
             />:
             <TextInput
             keyboardType={'decimal-pad'}
             value={this.state.upperBodyInput[index]&& this.state.upperBodyInput[index].reps}
             onChangeText={(e)=>{this.handelTextInput(e,section.uid,index,1)}}
             style={
              {height:37,alignItems:'center',justifySelf:'flex-end',borderWidth:1,borderColor:'#d3d3d3',maxWidth:70,width:70,paddingHorizontal:1}
             }
             />
             }
               </View>}
             </View>
           </View>
          )
        })}
        {/* <Text>{JSON.stringify(section.content)}</Text> */}
      </View>
    );
  };
  _updateSections = activeSections => {
    this.setState({ activeSections });
  };



  submitData = async () => {
    var falg = false

    for (var i = 0; i < this.state.generalInput.length; i++) {
      if (this.state.generalInput[i] === null) {
        alert("All fields are required")
        break;
      } else {

        if (i === 3) {
          // alert(i)
          var body = {
            generalInput: this.state.generalInput,
            upperBodyInput: this.state.upperBodyInput,
            lowerBodyInput: this.state.lowerBodyInput,
            all_upper: this.props.MyAssesment.assessments.all.upper,
            all_lower: this.props.MyAssesment.assessments.all.lower,

          }
          console.log(body)
          this.props.PostAssesment(body)
          //  this.setState({
          //    loader:true
          //  })
          //  setTimeout(()=>{
          //    alert("Assesment recorded succesfully!");
          //    this.setState({
          //      loader:false
          //    })
          //  },500)
        }
      }
    }





  }
  render() {

    return (
      <View style={{
        flex: 1,
      }}>
        {this.state.loader ? <View style={{
          alignItems: 'center', justifyContent: 'center', position: 'absolute', backgroundColor: 'rgba(0,0,0, 0.4)',
          zIndex: 1007, top: 0, bottom: 0, left: 0, right: 0
        }}>
          <ActivityIndicator size="large" color="red" />
        </View> : null}


        <ScrollView style={{ flex: 1, width: '100%', }}>
        <View style={{ height: Dimensions.get('window').height * 0.4 }}>
          {this.renderCustomSkin()}
        </View>
          <Accordion
            sections={this.props.AssesmentList}
            activeSections={this.state.activeSections}
            renderSectionTitle={this._renderSectionTitle}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
            onChange={this._updateSections}
          />
  <Button
          horizontal='8%'
          top={25}
          radius={20}
          backgColor='#69D3A9'
          height={40}
          weight='bold'
          textColor='white'
          titleSize={16}
          title='submit'
          buttonAction={() => this.submitData()}
        />
        </ScrollView>
        {this.props.loading ? <MyActivityIndicator /> : null}
      </View>

    )
  }
}

const mapStateToProps = state => {
  return {

    loading: state.user.loading,
    MyAssesment: state.user.MyAssesment,
    AssesmentList: state.user.AssesmentList
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchAssesments: () => fetchAssesments(dispatch),
    PostAssesment: (body) => PostAssesment(body, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Assesments);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black'
  },
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  controls: {
    backgroundColor: "transparent",
    borderRadius: 5,
    position: 'absolute',
    bottom: 5,
    left: 4,
    right: 4,
  },
  progress: {
    flex: 1,
    // width:'75%',
    marginHorizontal: 5,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  innerProgressCompleted: {
    height: 20,
    backgroundColor: 'red'
  },
  innerProgressRemaining: {
    height: 20,
    backgroundColor: '#2C2C2C',
  },
  generalControls: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    paddingBottom: 10,
  },
  skinControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  volumeControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  ignoreSilentSwitchControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  controlOption: {
    alignSelf: 'center',
    fontSize: 11,
    color: "white",
    paddingLeft: 2,
    paddingRight: 2,
    lineHeight: 12,
  },
  nativeVideoControls: {
    top: 184,
    height: 300
  },
  trackingControls: {
    flex: 1,
    flexDirection: 'row'
  },
  topuchableType: {
    height: 37,
    alignItems: 'center',
    // justifySelf:'flex-end',
    justifyContent: 'center',
    borderWidth: 1,
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderColor: '#d3d3d3',
    maxWidth: 70,
    width: 70,
    paddingHorizontal: 1
  },
  playercontrols: { height: 50, width: 50, alignItems: 'center', justifyContent: 'center' }
});

