import React, {Component} from 'react';
import {View, Text, Dimensions,
     Image, StyleSheet,AsyncStorage} from 'react-native';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import MyActivityIndicator from '../../../Component/activity_indicator';
import api from '../../../../api';
import Moment from "moment";
import BackButton from '../../../Component/BackButton';
import {Calendar} from "react-native-calendars";
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import { updateProfile, wp, daysF,saveuser } from "../../../store/actions/user"; 
import moment from 'moment';
import { ScrollView } from 'react-native-gesture-handler';

const width = Dimensions.get("window").width;
class wpAllset extends Component {

  constructor(props) {
    super(props);
    this.state = {
      arrSelectedDays: [],
      loader: false,
      currentDate: '',
      originalMarkedDates: '',
      selectedDate: undefined,
      dtex: '',
      calRefresh:false
    }
  }

  componentWillMount() {
    var todayDate = Moment().toDate()
    var todayMoment = Moment(todayDate)
    const todayDateFormatted = todayMoment.format('YYYY-MM-DD')
    this.setState({
      date: todayDateFormatted
    });
  }
  componentDidMount() {
    var D_date = new Date();
    var f_date = moment().format('YYYY-MM-DD');
    var TimestampToday = Date.parse(f_date);
    console.log("selected dayvvvvvvvvvv", D_date, f_date, Date.parse(f_date));
    this.getCurrentDateParams(TimestampToday);
    setTimeout(() => {
      this.setState({calRefresh:true})
    }, 200);
  }

  updateProfileL= async()=> {
     
    let body ={
      gender:this.props.wpData.gender,
      whatyourendgoal_id:this.props.wpData.endgoal.id,
      whereareyouat_id:this.props.wpData.WhereAreYou.id,
      height:this.props.wpData.tellus[0].height,
      weight:this.props.wpData.tellus[1].weight,
      age:this.props.wpData.tellus[2].age,
      current_lifestyle:this.props.wpData.tellus[3].lifestyle,
      current_experience:this.props.wpData.tellus[4].experience,
    }
    try {
      console.log("wp-------------------------------------------------",body);
      let response = await api.request('/update-profile', 'post', body);

      if (response.status === 200) {
          console.log("enter into 200 .,....")

          console.log("response for registration ...")
          console.log(response)
          response.json().then(async (respons) => {
              console.log("final registration response ,,,,,")
              console.log(respons)
              let jRes = await JSON.stringify(respons.user)
              this.props.saveuser(respons.user);
              console.log("jres-------------------------",jRes);
              this.saver(jRes);
              console.log("response after saving-------------------------"); 
          });
          this.setState({loader: false});
      }
      else if (response.status === 401) {
          console.log("enter into 401 .,....")

          this.setState({loader: false});
          alert('Invalid credentials');
      }
      else {
          console.log("enter into else .,....")

          this.setState({loader: false});
          response.json().then((response) => {
              console.log("sjkdsdfh")
              console.log(response)
              if(response.errors){
                  // if(response.errors.mobile){
                  //     alert(response.errors.mobile);
                  // }
              }
              
          })
      }
  } catch (error) {
      console.log("enter into error .,....")
      this.setState({loader: false});
      console.log("qwqwqwq")

      alert(error.message);
  }
}

  saver = async (jdata)=>{
    console.log('saver',jdata);
    const popAction = StackActions.pop({
      n: 7,
    });
    
  await   this.props.navigation.dispatch(popAction);
   await this.props.navigation.navigate('HOME');

    // this.props.navigation.state.params.onGoBack();
    // this.props.navigation.goBack()
    }


  async setAction(){
  
         console.log("allset .......")
  
      console.log(this.state.selectedDate);
      if(this.state.selectedDate) {
        this.setState({loader:true})
        const formatedDate = Moment(this.state.selectedDate).format("DD-MM-YYYY");
      console.log(formatedDate);
       let body ={
         start_date:formatedDate
       }
          console.log("body is ")
          console.log(body)
  
          try {
              console.log(body);
              let response = await api.request('/add-start-date', 'post', body);
  
              if (response.status === 200) {
                  console.log("enter into 200 .,....")
                  this.updateProfileL();
                  console.log("response for registration ...")
                  console.log(response)
                  response.json().then(async (respons) => {
                    console.log(respons)
                     if(respons.success){
                      console.log("submit date response ,,,,,")
                      console.log(respons)
                      console.log("-----------------------------------",respons.trainings.start_date,respons.trainings.days);
                      this.props.daysF(false,respons.trainings.days)
                      // let jRes = await JSON.stringify(respons.days)
                      // console.log("jres-------------------------",jRes); 
                      // AsyncStorage.setItem('training',jRes);
                      // alert(respons.message);
                      // AsyncStorage.setItem('startDate',respons.start_date);
                      // AsyncStorage.setItem('endDate',respons.end_date);
                      
                     }
                  }
                  );
                  this.setState({loader: false});
              }
              else if (response.status === 401) {
                  console.log("enter into 401 .,....")
  
                  this.setState({loader: false});
                  alert('Invalid credentials');
              }
              else {
                  console.log("enter into else .,....")
  
                  this.setState({loader: false});
                  response.json().then((response) => {
                      console.log("sjkdsdfh")
                      console.log(response)
                      if(response.errors){
                          // if(response.errors.mobile){
                          //     alert(response.errors.mobile);
                          // }
                      }
                      
                  })
              }
          } catch (error) {
              console.log("enter into error .,....")
              this.setState({loader: false});
              console.log("qwqwqwq")
  
              alert(error.message);
          }
      }
  }

  // async setAction() {
  //   console.log("allset .......")
  //   console.log(this.state.selectedDate);
  //   if (this.state.selectedDate) {
  //     this.setState({ loader: true })
  //     const formatedDate = Moment(this.state.selectedDate).format("DD-MM-YYYY");
  //     console.log(formatedDate);
  //     let body = {
  //       start_date: formatedDate
  //     }
  //     console.log("body is ")
  //     console.log(body)
  //     try {
  //       console.log(body);
  //       let response = await api.request('/add-start-date', 'post', body);
  //       if (response.status === 200) {
  //         console.log("enter into 200 .,....")
  //         console.log("response for registration ...")
  //         console.log(response)
  //         response.json().then(async (respons) => {
  //           console.log(respons)
  //           if (respons.success) {
  //             console.log("submit date response ,,,,,")
  //             console.log(respons)
  //             let jRes = await JSON.stringify(respons.days)
  //             console.log("jres-------------------------", jRes);
  //             this.props.navigation.navigate('HOME');
  //           }
  //         }
  //         );
  //         this.setState({ loader: false });
  //       }
  //       else if (response.status === 401) {
  //         this.setState({ loader: false });
  //         alert('Invalid credentials');
  //       }
  //       else {
  //         this.setState({ loader: false });
  //         response.json().then((response) => {
  //           console.log("sjkdsdfh")
  //           console.log(response)
  //           if (response.errors) {
  //           }
  //         })
  //       }
  //     } catch (error) {
  //       console.log("enter into error .,....")
  //       this.setState({ loader: false });
  //       console.log("qwqwqwq")

  //       alert(error.message);
  //     }
  //   }
  // }

  setCalendar = (selectedDate) => {
    let markedDates = {}
    var endDateCheck = Moment(selectedDate).add(84, 'days')
    const endDateFormatted = endDateCheck.format('YYYY-MM-DD')
    let currentDate = Moment(selectedDate)
    let endDate = Moment(endDateFormatted)
    const tapDateFormatted = currentDate.format('YYYY-MM-DD')
    var daysValue = this.props.Daydata;

    // #looping 
    while (currentDate <= endDate) {
      const day = currentDate.day()
      console.log(day,daysValue)
      var isWeekend = false;

      if (daysValue.length === 1) {
        isWeekend = parseInt(daysValue[0]) === day ? true : false
      } else if (daysValue.length === 2) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day) ? true : false
      } else if (daysValue.length === 3) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day) ? true : false
      } else if (daysValue.length === 4) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day || parseInt(daysValue[3]) === day) ? true : false
      } else if (daysValue.length === 5) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day || parseInt(daysValue[3]) === day ||parseInt( daysValue[4]) === day) ? true : false
      } else if (daysValue.length === 6) {
        isWeekend = (parseInt(daysValue[0]) === day || parseInt(daysValue[1]) === day ||parseInt( daysValue[2]) === day || parseInt(daysValue[3]) === day ||parseInt( daysValue[4]) === day || parseInt(daysValue[5]) === day) ? true : false
      } else if (daysValue.length === 7) {
        isWeekend = true
      }
      	

// alert(todayDate);
      const currentDateFormatted = currentDate.format('YYYY-MM-DD')
      let markup = {}
      if (tapDateFormatted === currentDateFormatted) {
        markedDates[currentDateFormatted]={
          customStyles: {
            container: {
              backgroundColor:'#e6f5ba'
              // backgroundColor: '#34BF83'
            },
            // text: {
            //   color: 'black',
            // },
          },
        }
      }
    
       if (isWeekend) {
        markedDates[currentDateFormatted]={
          customStyles: {
            container: {
             
               backgroundColor: '#62c795'
            },
            // text: {
            //   color: 'black',
            // },
          },
        }
         markup.marked = true, markup.dotColor = '#34BF83', markup.activeOpacity = 0
       } else {
        markedDates[currentDateFormatted]={
          customStyles: {
            container: {
              backgroundColor: '#b8ea86'
            },
            // text: {
            //   color: 'black',
            // },
          },
        }
         markup.marked = true, markup.dotColor = '#ABE773', markup.activeOpacity = 0
      }
      //  markedDates[currentDateFormatted] = markup
      currentDate = Moment(currentDate).add(1, 'days')
    }

   // #looping end
   
   var today = new Date(); 
var todayDate = Moment(today).format('YYYY-MM-DD');
   markedDates[todayDate]={
    customStyles: {
      container: {
        backgroundColor: '#62c760'
      },
      // text: {
      //   color: 'red',
      // },
    },
  }
  console.log(markedDates);
    this.setState({
      originalMarkedDates: markedDates,
    },()=>{
      console.log("markedDates",markedDates);
    })
  }

  backButtonAction() {
    this.props.navigation.pop()
  }

  nextButtonAction() {

  }

  async getCurrentDateParams(date2) {

var offset = new Date().getTimezoneOffset();
var datex;
console.log(offset)
   console.log("timestamp,",moment().utcOffset(),date2);
    var dateString = Moment(date2).format("YYYY-MM-DD");
    console.log(dateString);
    // if(offset = -330){
    //    datex = Moment(dateString).format("YYYY-MM-DD");
     
    // }else{
       datex = Moment(dateString).add(1,'day').format("YYYY-MM-DD");
    // }
    
    this.setState({
      selectedDate: datex
    })
    this.setCalendar(datex)
    console.log("current time after parse " + datex);
    const date = Moment(dateString).format("D MMM YYYY");
    const dateInitail = Moment(dateString).format("DD");
    const dateMiddle = Moment(dateString).format("MMMM YYYY");
    const dateLast = Moment(dateString).format("dddd");
  }

  render() {
    return (
      <View style={{ flex: 1,paddingBottom:5 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 50, backgroundColor: '#fff', zIndex: 1 }}>
          <BackButton buttonAction={() => this.backButtonAction()} />
        </View>

        <BottomBorderView horizontal={0} top={0} />
        {this.state.loader ? <MyActivityIndicator /> : null}

       <ScrollView>
       <Text style={styles.titleText}> When do you want to start? </Text>
        <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 10, elevation: 1,}}>
          {/* <Calendar
            markedDates={this.state.originalMarkedDates}
            current={this.state.currentDateForCalender}
            minDate={this.state.date}//{"2019-08-30"}
            onDayPress={day => { this.getCurrentDateParams(day.timestamp); }}
            onDayLongPress={day => { console.log("selected day", day); }}
            monthFormat={"MMMM yyyy"}
            onMonthChange={month => { console.log("month changed", month); }}
            disableMonthChange={false}
            firstDay={1}
            onPressArrowLeft={substractMonth => substractMonth()}
            onPressArrowRight={addMonth => addMonth()}
          /> */}
   {    this.props.Daydata  && <Calendar
   theme={{
    backgroundColor: '#ffffff',
    calendarBackground: '#ffffff',
    textSectionTitleColor: '#b6c1cd',
    selectedDayBackgroundColor: '#00adf5',
    selectedDayTextColor: '#ffffff',
    // todayTextColor: '#00adf5',
    dayTextColor: '#2d4150',
    textDisabledColor: '#bbbbbb',
    dotColor: '#00adf5',
    selectedDotColor: '#ffffff',
    arrowColor: 'orange',
    // monthTextColor: '',
    indicatorColor: 'blue',
    // textDayFontFamily: 'monospace',
    // textMonthFontFamily: 'monospace',
    // textDayHeaderFontFamily: 'monospace',
    textDayFontWeight: '300',
    textMonthFontWeight: 'bold',
    textDayHeaderFontWeight: '300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16
  }}
 ref={component => this.Calendar = component} // Date marking style [simple/period/multi-dot/single]. Default = 'simple'
  markingType={'custom'}
  onPressArrowRight={addMonth => addMonth()}
  onPressArrowLeft={substractMonth => substractMonth()}
  current={this.state.currentDateForCalender}
  onDayPress={day => { this.getCurrentDateParams(day.timestamp); }}
  onDayLongPress={day => { console.log("selected day", day); }}
  firstDay={1}
  monthFormat={"MMMM yyyy"}
  minDate={this.state.date}//{"2019-08-30"}
  markedDates={this.state.originalMarkedDates}
  refreshing={this.state.calRefresh}
/>}
        </View>
       </ScrollView>
        <Button
          horizontal='8%'
          top={20}
          radius={20}
          backgColor='#69D3A9'
          height={40}
          textColor='white'
          titleSize={16}
          title='All set'
          buttonAction={() => this.setAction()}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  titleText: { marginTop: 20, fontSize: 16, marginHorizontal: '10%', textAlign: 'center', fontWeight: '400' },
});

const mapStateToProps = state =>{
  return{
      userData: state.user.personal,
      wpData:state.user.wp,
      Daydata:state.user.days
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
      updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
      wp:(type,userData) => wp(type,userData,dispatch),
      daysF:(type,userData)=> daysF(type,userData,dispatch),
      saveuser:(data)=>saveuser(data,dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(wpAllset);