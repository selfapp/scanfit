


import React, {Component} from 'react';
import {View, Text, Dimensions, TouchableOpacity,AsyncStorage,
     Image, StyleSheet, ScrollView} from 'react-native';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import MyActivityIndicator from '../../../Component/activity_indicator';
import api from '../../../../api';
import BackButton from '../../../Component/BackButton';
import CheckBox from "react-native-checkbox";
import { connect } from 'react-redux';
import { updateProfile, wp, daysF } from "../../../store/actions/user";
let count = 0;
const WindowSize = Dimensions.get("window");
const days = ['Su','Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',]
class wpDays extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            Sunday:false,
            Monday:false,
            Tuesday:false,
            Wednesday:false,
            Thursday:false,
            Friday:false,
            Saturday:false,
        }
    }

    componentDidMount(){
        let {Daydata}=this.props;
        Daydata.map((item,index)=>{
            if(item==='0'){
                this.setState({
                    Sunday:true
                })
            }
            if(item==='1'){
                this.setState({
                    Monday:true
                })
            }
            if(item==='2'){
                this.setState({
                    Tuesday:true
                })
            }
            if(item==='3'){
                this.setState({
                    Wednesday:true
                })
            }
            if(item==='4'){
                this.setState({
                    Thursday:true
                })
            }
            if(item==='5'){
                this.setState({
                    Friday:true
                })
            }
            if(item==='6'){
                this.setState({
                    Saturday:true
                })
            }

        })
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }
dayPush=async()=>{
    const {Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday} = this.state;
    let selectionList=[];
    if(Sunday){
        selectionList.push('0')
    }
    if(Monday){
        selectionList.push('1')
    }
    if(Tuesday){
        selectionList.push('2')
    }
    if(Wednesday){
        selectionList.push('3')
    }
    if(Thursday){
        selectionList.push('4')
    }
    if(Friday){
        selectionList.push('5')
    }
    if(Saturday){
        selectionList.push('6')
    }
    
    this.nextButtonAction(selectionList);
}
nextButtonAction = async (selectionList) => {
   
    console.log("arrSelectedDays")
    console.log(selectionList)
    // this.props.navigation.navigate('CalendarSelection', {selectedDays:this.state.arrSelectedDays})
    if(selectionList.length > 2 && selectionList.length <6){

        this.setState({
            loader: true
        })
        try{
            let body = {
                    "days": selectionList,
            };
            console.log("body for workout selection ")
            console.log(body)

            let response = await api.request('/training-days', 'post', body, null);
            console.log("workout day selection response .")
            console.log(response)
            if (response.status == 200) {
                response.json().then((data) => {
                    console.log("workout day data response ......")
                    console.log(data);
                    console.log(data.records.days)
                    this.props.daysF(false,data.records.days);
                    setTimeout(() => {
                        this.props.navigation.navigate('wpAllset', {selectedDays:this.state.arrSelectedDays})
                    this.setState({
                        loader: false
                    })
                    }, 200);
                })
            }
            else {
                this.setState({loader: false});
                response.json().then((respons) => {
                    console.log("error repsosne ")
                    console.log(respons)
                    alert(respons.errors);
                })
            }
        }catch (error) {
            this.state.loader(false);
            alert(error.message);
        }
    } else {
        alert("You should select workout for minimum 3 days to maximum 5 days");
    }
}


    render() {
const {Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday} = this.state;
        return (
            <View style={{ flex: 1 }}>
                <View style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                    height: 50, backgroundColor: '#fff', zIndex: 1
                }}>
                    <BackButton
                        buttonAction={() => this.backButtonAction()}
                    />
                </View>

                <BottomBorderView
                    horizontal={0}
                    top={0}
                />
                {this.state.loader ? <MyActivityIndicator /> : null}

                <Text style={styles.titleText}>
                    How many days a week do you want to train? (3 to 5)
            </Text>
                <View>
                    <ScrollView contentContainerStyle={styles.containerstyle} style={styles.mainContainer} horizontal={true}>

                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'Su'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Sunday:!Sunday})}>
                                    {
                                        Sunday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'Mo'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Monday:!Monday})}>
                                    {
                                        Monday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                        

                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'Tu'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Tuesday:!Tuesday})}>
                                    {
                                        Tuesday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'We'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Wednesday:!Wednesday})}>
                                    {
                                        Wednesday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'Th'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Thursday:!Thursday})}>
                                    {
                                        Thursday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>


                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'Fr'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Friday:!Friday})}>
                                    {
                                        Friday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ width: WindowSize.width*0.08, height: 50, marginLeft: 10, marginTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray', fontSize: 13 }}>
                                    {'Sa'}
                                </Text>
                            </View>
                            <BottomBorderView horizontal={0} top={10} />
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                                <TouchableOpacity style={styles.Ttouchable} onPress={()=>this.setState({Saturday:!Saturday})}>
                                    {
                                        Saturday?<View style={styles.selected} />: <View style={styles.unselect} />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>

                    </ScrollView>
                </View>
                <View style={styles.boxView}>
                <View style={styles.selected} />
                    <Text style={styles.boxText}>
                        Training days
                </Text>
                </View>
                <View style={styles.boxView}>
                <View style={styles.unselect} />
                    <Text style={styles.boxText}>
                        Active rest
                </Text>
                </View>

                <Button
                    horizontal='8%'
                    top={50}
                    radius={20}
                    backgColor='#69D3A9'
                    height={40}
                    textColor='white'
                    titleSize={16}
                    title='Next'
                    buttonAction={() => this.dayPush()}
                />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    titleText: { marginTop: 20, fontSize: 16, marginHorizontal: '10%', textAlign: 'center', fontWeight: '400' },
    mainContainer: { marginHorizontal: 15, height: 120, marginTop: 10, },
    Tview: {
        position: 'absolute',
        backgroundColor: 'transparent'
    },
    Timage: {

    },
    Ttouchable: {
        alignItems: 'center',
        height:30,
        width:30,
        borderRadius:15,
        justifyContent: 'center',
    },
    Ttext: {
        // color: colors.button,
        fontSize: 18,
        textAlign: 'center'
    },
    unselect:{height:WindowSize.width*0.08,width:WindowSize.width*0.08,borderRadius:15, backgroundColor:'#b8ea86'},
    selected:{height:WindowSize.width*0.08,width:WindowSize.width*0.08,borderRadius:15, backgroundColor:'#62c795'},
    boxView: { marginHorizontal: 15, marginTop: 20, flexDirection: 'row', alignItems: 'center' },
    boxText: { marginLeft: 15, fontSize: 17, color: 'gray' },
    containerstyle: { justifyContent: 'space-evenly', alignItems: 'stretch', width: '100%' }
});
const mapStateToProps = state =>{
    return{
        userData: state.user.personal,
        wpData:state.user.wp,
        Daydata:state.user.days
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
        wp:(type,userData) => wp(type,userData,dispatch),
        daysF:(type,userData)=> daysF(type,userData,dispatch)
        
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(wpDays);



  