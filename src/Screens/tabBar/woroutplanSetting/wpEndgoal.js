
import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, TouchableWithoutFeedback, AsyncStorage, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import BackButton from '../../../Component/BackButton';
import TitleText from '../../../Component/TitleText';
import api from '../../../../api';
import MyActivityIndicator from '../../../Component/activity_indicator';
import ImageLoad from 'react-native-image-placeholder';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper'
import { updateProfile, wp } from "../../../store/actions/user";
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const styles = StyleSheet.create({
    slide: {
        height: height - 200, width: width - 40, backgroundColor: 'white', marginTop: 10

    },
});
let count = 0;
class wpendgoal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,
            selectedIndex: 0,
            slides: [],
            loader: false, whereid: null, endgoalid: null
        }
    }

    async componentDidMount() {
        let data = this.props.wpData.gender;
        let userData = this.props.userData;
        this.setState({ userData });
        let whereid = this.props.userData
        setTimeout(() => {
            this.getImages(data);
            this.setState({ endgoalid: userData.whatyourendgoal_id })
        }, 280);

    }

    getImages = async (data) => {
        this.setState({
            loader: true
        })
        try {
            let body = {
                "gender": data
            };
            let response = await api.request('/where-your-end-goal', 'post', body, null);
            if (response.status == 200) {
                response.json().then((data) => {
                    this.setState({
                        slides: data.records,
                        
                    }, () => {
                        var selected_id = this.props.userData.whatyourendgoal_id;
                        let index = 0;
                        this.state.slides.map((item, index2) => {
                            // console.log(index, item)
                            if (selected_id === item.id) {
                                index = index2
                            }
                        })
                        console.log("selected", index, this.state.slides[index]);
                        const value = { 'id': this.state.slides[index].id }
                        console.log("Value is ")
                        this.swiper.scrollBy(index,true);

                        console.log(value)
                        this.setState({
                            selectedOption: this.state.slides[index],
                            selectedIndex: index,
                            loader: false
                        })
                    })
                })
            }
            else {
                this.setState({ loader: false });
                response.json().then((respons) => {
                    alert(respons.errors);
                })
            }
        } catch (error) {
            this.setState({ loader: false });
            alert(error.message);
        }
    }

    async nextBtnAction() {
        await this.props.wp(false, { endgoal: this.state.selectedOption });
        this.props.navigation.navigate('wpDays');
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    _onPress(index) {
        console.log("selected", index, this.state.slides[index]);
        const value = { 'id': this.state.slides[index].id }
        console.log("Value is ")
        console.log(value)
        this.setState({
            selectedOption: this.state.slides[index],
            selectedIndex: index
        })
    }

    _renderItem = ({ item, index }) => {
        return (
            <TouchableWithoutFeedback onPress={() => this._onPress(item, index)}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <ImageLoad
                        style={styles.slide}
                        loadingStyle={{ size: 'large', color: 'black' }}
                        source={{ uri: (item.image) }}
                    />
                    {
                        this.state.selectedOption === item.id ? (
                            <Image style={{ width: 150, height: 150, marginTop: -180 }} source={require('../../../assets/SelectedTick.png')} />
                        ) : (null)
                    }
                </View>
            </TouchableWithoutFeedback>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <BackButton
                    buttonAction={() => this.backButtonAction()} />
                <BottomBorderView
                    horizontal={0}
                    top={0}
                />
                <TitleText
                    size={15}
                    color='black'
                    weight='400'
                    width='90%'
                    alignment='left'
                    height={50}
                    title="What's your end goal?"
                    top={10}
                />
                {this.state.loader ? <MyActivityIndicator /> : null}
                {
                  this.state.slides.length>0 ?
                <Swiper
                ref={component => this.swiper = component} 
                onIndexChanged={(index) => { console.log("app intro slider selected", index,);this._onPress(index)}}
                loop={false}
                showsButtons={false}
                height={height - 250}
                dotColor={'gray'}
                activeDotColor={'red'}
                scrollEnabled={true}
                index={this.state.selectedIndex}
                >
                {this.state.slides.map((item, key) => {
                                                   return (
                                                    <View key={key}
                                                    style={{alignItems: 'center', flex: 1, height: '100%',width: '95%',}}>
                                                    <ImageLoad
                                                     style={{width: '95%', height: '100%'}}
                 style={styles.slide}
                 loadingStyle={{ size: 'large', color: 'black' }}
                 source={{ uri: (item.image) }}
             />
             </View>
                                                   )
                                               })}
      </Swiper>:<View style={styles.slide}></View>
              }
                {/* <AppIntroSlider
                    renderItem={this._renderItem}
                    skipLabel={this.state.selectedOption}
                    slides={this.state.slides}
                    showSkipButton={false}
                    showDoneButton={false}
                    showNextButton={false}
                    showPrevButton={false}
                    activeDotStyle={style = { backgroundColor: 'red' }} 
                    ref={ref => this.AppIntroSlider = ref}
                onSlideChange={(index, lastIndex) => { console.log("app intro slider selected", index, lastIndex); this._onPress(index) }}
                /> */}
                

                <Button
                    horizontal='12%'
                    top={-20}
                    radius={18}
                    bottom={10}
                    backgColor='#69D3A9'
                    height={36}
                    weight='400'
                    textColor='white'
                    titleSize={18}
                    title='Next'
                    buttonAction={() => this.nextBtnAction()}
                />
            </View>
        );
    }
}
const mapStateToProps = state => {
    return {
        userData: state.user.personal,
        wpData: state.user.wp
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: (type, userData) => updateProfile(type, userData, dispatch),
        wp: (type, userData) => wp(type, userData, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(wpendgoal);