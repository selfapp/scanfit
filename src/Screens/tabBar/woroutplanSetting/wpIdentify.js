
import React, {Component} from 'react';
import {View, TouchableOpacity, Image, StyleSheet, AsyncStorage} from 'react-native';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import { connect } from 'react-redux';
import { updateProfile, wp } from "../../../store/actions/user";
import { ScrollView } from 'react-native-gesture-handler';
class wpIdentifyUrself extends Component {

    constructor(props) {
        super(props);
        this.state={
            selectedGender:'',
            identityChange:false
        }
    }

    componentDidMount() {
        //  this.props.navigation.getParam('wpTellus')
        console.log('wpid',this.props.userData)
        this.setState({selectedGender:this.props.userData.gender})

        // AsyncStorage.getItem('identity').then((data)=> {

        //     this.setState({identityChange:false})
        //     // AsyncStorage.setItem('identityChange','false')
        //     console.log("Identity data is", data)
        //     if(data !== null){
        //        this.setState({selectedGender:data})
        //     }
        // })
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    selectedOtion(value) {
        console.log("Selected value is ", value)

        if(value === this.state.selectedGender){
            console.log("Enter into if")
            this.setState({identityChange:false})
            // AsyncStorage.setItem('identityChange','false')
        }else{
            console.log("Enter into else")
            this.setState({identityChange:true})
            // AsyncStorage.setItem('identityChange','true')
        }
        this.setState({selectedGender:value})
        // AsyncStorage.setItem("identity",value)
    }

   async nextBtnAction() {
        console.log("selected gender ",this.state.selectedGender )

        if(this.state.selectedGender === null || this.state.selectedGender.length === 0){
            alert("Please select one option.")
         }else{
            await  this.props.wp(false,{gender:this.state.selectedGender});
            this.props.navigation.navigate('wpwhere')
         }
    }

    render(){

        return(
        <View style={{flex:1}}>
            
            <BackButton
                 buttonAction = {()=> this.backButtonAction()}/>
            <BottomBorderView 
                    horizontal={0}
                    top={0}
                    />
            <TitleText
                    size = {15}
                    color = 'black'
                    weight = '400'
                    width = '90%'
                    height = {50}
                    alignment = 'left'
                    title = 'How do you identify yourself?'
                    top = {10}
                />
            <ScrollView contentContainerStyle={{ alignItems:'center'}}>
                <TouchableOpacity style={styles.circleView} onPress={()=>this.selectedOtion('Male')}>
                        <Image style={styles.circleImage} source={require('../../../assets/Male.png')}></Image>
                    {
                        this.state.selectedGender === 'Male' ? (
                            <Image style={[styles.circleImage, {marginTop:-160}]} source={require('../../../assets/SelectedTick.png')}></Image>                           
                        ) : (null)
                    }
                </TouchableOpacity>
                <TouchableOpacity style={styles.circleView} onPress={()=>this.selectedOtion('Female')}>
                      <Image style={styles.circleImage} source={require('../../../assets/Female.png')}></Image>
                {
                        this.state.selectedGender === 'Female' ? (
                            <Image style={[styles.circleImage, {marginTop:-160}]} source={require('../../../assets/SelectedTick.png')}></Image>
                        ) : (null)
                }
                </TouchableOpacity>
            </ScrollView>
            <Button
                    horizontal = '12%'
                    top = {10}
                    radius = {18}
                    bottom = {10}
                    backgColor = '#69D3A9'
                    height = {36}
                    weight = '400'
                    textColor = 'white'
                    titleSize = {18}
                    title = 'Next'
                    buttonAction = {()=>this.nextBtnAction()}
                />
        </View>
        )
        
    }
}

const  styles = StyleSheet.create({
    circleView:{width:162, height:162,  marginTop:'10%', justifyContent:'center', alignItems:'center'},
    circleImage:{width:160, height:160}
})
const mapStateToProps = state =>{
    return{
        userData: state.user.personal,
        wp:state.user.wp
    }
  };
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
        wp:(type,userData) => wp(type,userData,dispatch)
    }
  };
  export default connect(mapStateToProps, mapDispatchToProps)(wpIdentifyUrself);