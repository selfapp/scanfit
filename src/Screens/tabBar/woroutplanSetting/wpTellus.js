import React, { Component } from 'react';
import {
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  ScrollView,
  UIManager,
  Platform,
  Dimensions,
  AsyncStorage,
  Alert
} from 'react-native';
import moment, { months } from 'moment';
import BottomBorderView from '../../../Component/BottomBorderView';
import ExpandableItemComponent from '../../../Component/ExpandableItemComponent';
import Picker from 'react-native-picker';
import { connect } from 'react-redux';
import { updateProfile, wp } from "../../../store/actions/user";
import API from '../../../../redAPI'
const width = Dimensions.get("window").width;
const options = [['4', '5', '6', '7', '8','9'],
['0', '1', '2', '3', '4', '5', '6', '7', '8','9','10','11']]
var age2=''
var height2=''
class wpTellUs extends Component {
  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = { 
      userProfile:null,
      listDataSource: [
      {
        index:0,
        isExpanded: false,
        testExpand:'',
        category_name: 'Age',
        image:require('../../../assets/age.png'),
        width:18,
        height:18,
        subcategory: [],
      },
      {
        index:1,
        isExpanded: false,
        testExpand:'',
        width:20,
        height:25,
        category_name: 'Height',
        image:require('../../../assets/height.png'),
        subcategory: [],
      },
      {
        index:2,
        isExpanded: false,
        testExpand:'',
        width:18,
        height:18,
        category_name: 'Weight',
        image:require('../../../assets/weight-scale.png'),
        subcategory: [],
      },
      {
        index:3,
        isExpanded: false,
        testExpand:'',
        width:22,
        height:21,
        category_name: 'Current Lifestyle',
        image:require('../../../assets/gym.png'),
        subcategory: [{ id: 0, val: 'Active',image:require('../../../assets/active.png'), isCheck:false }, 
        {id: 1,val: 'Moderately Active',image:require('../../../assets/ModActive.png'), isCheck:false},
        {id: 2,val: 'Sednetary(Inactive)', image:require('../../../assets/sedentary.png'), isCheck:false}],
      },
      {
        index:4,
        isExpanded: false,
        testExpand:'',
        width:19,
        height:12,
        category_name: 'Current Experience with Excercise',
        image:require('../../../assets/dumbbell.png'),
        subcategory: [{ id: 0, val: 'Beginner', image:require('../../../assets/Shape.png'), isCheck:false}, 
        {id: 1, val: 'Intermediate', image:require('../../../assets/intermediate.png'),isCheck:false}, 
        {id: 2, val: 'Advanced', image:require('../../../assets/rings.png'), isCheck:false}],
      },
    ],
    age2:'',
        age:'',
        height:'',
        weightKg:'',
        weightLbs:'',
        currentLifeStyle:'',
        currentExperience:'',
        selectedValue:''
     };
  }


componentWillMount(){
  var jdata=this.props.userData;
  const {userData}= this.props
 {
   this.updateHeight(userData.height);
   this.setState({height:userData.height})
   this.updateWeightInLbs(userData.weight);

   
 }
  var dat1 = this.props.navigation.getParam('data');
  height2=dat1.height;
  age2=dat1.age;
  this.setState({
    
    age:dat1.age,
  
  },()=>{
    // alert(this.state.age)
  })
}

  async componentDidMount() {
    const {userData}= this.props
let targted1;
    let listdata = this.state.listDataSource;
      listdata.map((v,i)=>{
        console.log(listdata);
        if (listdata[i].index === 4) {
          targted1=i;
          const targetOption = listdata[i];
          targetOption.subcategory.map((value, placeindex) => {
            console.log(placeindex, value)
            if (value.val === userData.current_experience) {
              value.isCheck = true
              console.log('mila kya',value)
            }
          })
        }
        
      })
     if(targted1){
       console.log("targeted",this.state.listDataSource[targted1]);
       this.state.listDataSource[targted1].testExpand='fff'
     }
    console.log("new list data", listdata)
    this.setState({
      listDataSource:listdata
    })
    console.log(this.state.listDataSource);

    let targted2;
    let listdata2 = this.state.listDataSource;
      listdata2.map((v,i)=>{
        console.log(listdata2);
        if (listdata2[i].index === 3) {
          targted2=i;
          const targetOption2 = listdata[i];
          targetOption2.subcategory.map((value, placeindex) => {
            console.log(placeindex, value)
            if (value.val === userData.current_lifestyle) {
              value.isCheck = true
              console.log('mila kya',value)
            }
          })
        }
        
      })
     if(targted2){
       console.log("targeted",this.state.listDataSource[targted2]);
       this.state.listDataSource[targted2].testExpand='fff'
     }
    console.log("new list data 2", listdata2)
    this.setState({
      listDataSource:listdata2
    })
    console.log(this.state.listDataSource);
    this.setState({
      first_name: userData.first_name,
      last_name: userData.last_name,
      gender: userData.gender,
      height: userData.height,
      currentExperience:userData.current_experience,
      currentLifeStyle:userData.current_lifestyle,
    }, console.log('username', userData))
    if (userData.weight_base_unit === "lbs") {
      this.updateWeightInLbs(userData.weight);
    }
    Picker.init({
      pickerData: options,
      pickerTitleText: 'Select Feet and inch',
      pickerFontSize: 16,
      pickerBg:[255, 255, 255, 1],
      pickerConfirmBtnText: 'Confirm',
      pickerCancelBtnText: 'Cancel',
      onPickerConfirm: data => {
          console.log(data);
          console.log(data[0]);
          console.log(data[1]);
          let height = data[0]+"."+data[1];
          this.setState({height:height})
      },
      onPickerCancel: data => {
          console.log(data);
      },
      onPickerSelect: data => {
          console.log(data);
      }
  });
}
updateAge = (value) => {
  console.log('ios',value);
  this.setState({age:value})
  console.log('ios',value);
}
    updateHeight = (feet,inch) => {
      let height = feet+"."+inch;
      this.setState({height:height})
    }

  updateWeight = (value) => {
    this.setState({weightKg:value})
    if(value){
      var convertToPound = value * 2.2046
      convertToPound = convertToPound.toFixed(2)
      this.setState({weightLbs:String(convertToPound)})
    }else{
      this.setState({weightLbs:''})
    }
    
  }

  updateWeightInLbs = (value) => {
    this.setState({weightLbs:value})
    if(value){
      var convertToKg =  value * 0.453592
      convertToKg = convertToKg.toFixed(2)
      this.setState({weightKg: String(convertToKg)})
    }else{
      this.setState({weightKg:''})
    }
   
  }

  updateUI = (sectionCount, value, itemId) => {
    const targetOption = this.state.listDataSource[sectionCount];
    var count = 0;
    targetOption.subcategory.map((value, placeindex) =>{
      placeindex === itemId
      ? (targetOption.subcategory[placeindex]['isCheck'] = !targetOption.subcategory[placeindex]['isCheck'])
      : (targetOption.subcategory[placeindex]['isCheck'] = false)
      if(sectionCount === 3){
        console.log(targetOption.subcategory[placeindex]['isCheck'])
        if(targetOption.subcategory[placeindex]['isCheck']){
          count = 1;
        }
      }else if(sectionCount === 4){
        if(targetOption.subcategory[placeindex]['isCheck']){
          count = 1;
        }
      }
    }
  );

  console.log("New target option ....")
  console.log(targetOption);

    const array = [...this.state.listDataSource];
    array.map((value, placeindex) =>
      placeindex === sectionCount
        ? (array[placeindex]['testExpand'] = 'fffff')
        : (array[placeindex]['testExpand'] = '')
    );
    const newArray = [...this.state.listDataSource]
    array[sectionCount] = targetOption
  
      this.setState(() => {
        return {
          listDataSource: newArray,
        };
      });

      if(sectionCount === 3){
       
        if(count === 1){
          this.setState({currentLifeStyle:value})
        }else{
          this.setState({currentLifeStyle:''})
        }
      }else if(sectionCount === 4){
        if(count === 1){
          this.setState({currentExperience:value})
        }else{
          this.setState({currentExperience:''})
        }
      }
  }

  updateLayout = index => {

    console.log("index value is ", index)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...this.state.listDataSource];
    array.map((value, placeindex) =>
      placeindex === index
        ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
        : (array[placeindex]['isExpanded'] = false)
    );
    this.setState(() => {
      return {
        listDataSource: array,
      };
    });
  };

  async continueAction() {
    console.log("height is ")
    console.log(this.state.age)
    if(this.state.height.length === 0) {
      alert("Please enter your height.")
    }else if(this.state.weightLbs.length === 0) {
      alert("Please enter your weight.")
    }else if(this.state.currentLifeStyle.length === 0) {
      alert("Please select current life style.")
    }else if(this.state.currentExperience.length === 0) {
      alert("Please select current experience with excercise.")
    }
    else {
      var Body ={
        date_of_birth : this.state.age,
      }
      let response = await API.CheckDob(Body)
      response.json().then((JRes) => {
        if(JRes.success){
          var value = [{'height':this.state.height.toString()}, 
          {'weight':this.state.weightLbs}, 
          {'age':this.state.age},
          {'lifestyle':this.state.currentLifeStyle},
          {'experience':this.state.currentExperience}]
        console.log( 'height', this.state.height.toString());
          this.props.wp(false,{tellus:value})
          this.props.navigation.navigate('wpIdentify',{tellus:value})
        
        }
        else{
          alert(JRes.errors)
        }
      })

    
    }
  }
 
  render() {
    const {userData}= this.props
    return (
      <View style={styles.container}>
           <BottomBorderView 
                horizontal={0}
                top={0}
                />
        <Text style={styles.topHeading}>Tell us about yourself</Text>
        <ScrollView>
          {this.state.listDataSource.map((item, key) => (
            <ExpandableItemComponent
            ageType={false}
              key={item.category_name}
              age1={this.state.age}
              age2={age2}
              height2={height2}
              height1={this.state.height}
              weight1={this.state.weightKg}
              weightLbs = {this.state.weightLbs}
              updateUI={this.updateUI.bind(this)}
              updateAge={age => {console.log('tell us age---------',age);this.setState({age})}}
              updateHeight={this.updateHeight.bind(this)}
              updateWeight={this.updateWeight.bind(this.state.weightKg)}
              updateWeightInLbs={this.updateWeightInLbs.bind(this.state.weightLbs)}
              onClickFunction={this.updateLayout.bind(this, key)}
              item={item}
              page={'tellus'}
            />
          ))}
           <Button
                horizontal = '8%'
                top = {50}
                radius = {20}
                backgColor = '#69D3A9'
                height = {40}
                weight = 'bold'
                textColor = 'white'
                titleSize = {16}
                title = 'Continue'
                buttonAction = {()=>this.continueAction()}
                />
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  topHeading: {
    paddingLeft: 10,
    fontSize: 20,
    marginVertical: 15
  },
});
 
const CONTENT = [
  {
    index:0,
    isExpanded: false,
    testExpand:'',
    category_name: 'Age',
    image:require('../../../assets/age.png'),
    width:18,
    height:18,
    subcategory: [],
  },
  {
    index:1,
    isExpanded: false,
    testExpand:'',
    width:20,
    height:25,
    category_name: 'Height',
    image:require('../../../assets/height.png'),
    subcategory: [],
  },
  {
    index:2,
    isExpanded: false,
    testExpand:'',
    width:18,
    height:18,
    category_name: 'Weight',
    image:require('../../../assets/weight-scale.png'),
    subcategory: [],
  },
  {
    index:3,
    isExpanded: false,
    testExpand:'',
    width:22,
    height:21,
    category_name: 'Current Lifestyle',
    image:require('../../../assets/gym.png'),
    subcategory: [{ id: 0, val: 'Active',image:require('../../../assets/active.png'), isCheck:false }, 
    {id: 1,val: 'Moderately Active',image:require('../../../assets/ModActive.png'), isCheck:false},
    {id: 2,val: 'Sednetary(Inactive)', image:require('../../../assets/sedentary.png'), isCheck:false}],
  },
  {
    index:4,
    isExpanded: false,
    testExpand:'',
    width:19,
    height:12,
    category_name: 'Current Experience with Excercise',
    image:require('../../../assets/dumbbell.png'),
    subcategory: [{ id: 0, val: 'Beginner', image:require('../../../assets/Shape.png'), isCheck:false}, 
    {id: 1, val: 'Intermediate', image:require('../../../assets/intermediate.png'),isCheck:false}, 
    {id: 2, val: 'Advanced', image:require('../../../assets/rings.png'), isCheck:false}],
  },
];
const mapStateToProps = state =>{
  return{
      userData: state.user.personal,
      wp:state.user.wp
  }
};

const mapDispatchToProps = (dispatch) =>{
  return{
      updateProfile:(type,userData) => updateProfile(type,userData,dispatch),
      wp:(type,userData) => wp(type,userData,dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(wpTellUs);