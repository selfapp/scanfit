
import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, TouchableWithoutFeedback, AsyncStorage, Image,Text } from 'react-native';
// import AppIntroSlider from 'react-native-app-intro-slider';
import BottomBorderView from '../../../Component/BottomBorderView';
import Button from '../../../Component/Button';
import BackButton from '../../../Component/BackButton';
import TitleText from '../../../Component/TitleText';
import api from '../../../../api';
import MyActivityIndicator from '../../../Component/activity_indicator';
import ImageLoad from 'react-native-image-placeholder';
import { connect } from 'react-redux';
import { updateProfile, wp } from "../../../store/actions/user";
import Swiper from 'react-native-swiper'
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
let count = 0;
const styles = StyleSheet.create({
    slide: {
        height: height - 200, width: width - 40, backgroundColor: 'white', marginTop: 10
    },
    wrapper: {
        
        marginTop: 10,
        height: height - 200,
        width: width - 40,
    },
  slide1: {
    height: height - 250,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB'
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }
});
class WhereAreYou extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,
            selectedIndex: 0,
            slides: [],
            loader: false,
            userData: '',
            whereid: ''
        }
    }

    async componentDidMount() {
        let data = this.props.wpData.gender;
        let userData = this.props.userData;
        this.setState({ userData });
        let whereid = this.props.userData
        setTimeout(() => {
            this.getImages(data);
            this.setState({ whereid: userData.whereareyouat_id })
        }, 280);


    }

    getImages = async (data) => {

        this.setState({
            loader: true
        })
        try {
            let body = {
                "gender": data
            };
            let response = await api.request('/where-are-you-at', 'post', body, null);
            if (response.status == 200) {
                response.json().then((data) => {
                    this.setState({
                        slides: data.records,
                    }, () => {
                        var selected_id = this.props.userData.whereareyouat_id;
                        let index = 0;
                        this.state.slides.map((item, index2) => {
                            // console.log(index, item)
                            if (selected_id === item.id) {
                                index = index2
                            }
                        })
                        
                        const value = { 'id': this.state.slides[index].id }
                        console.log("Value is ")
                        console.log(value)
                        this.swiper.scrollBy(index,true);
                        this.setState({
                            selectedOption: this.state.slides[index],
                             selectedIndex:index,
                            loader: false
                        },()=>{
                        })
                    })
                })
            }
            else {
                this.setState({ loader: false });
                response.json().then((respons) => {
                    alert(respons.errors);
                })
            }
        } catch (error) {
            this.state.loader(false);
            alert(error.message);
        }
    }

    async nextBtnAction() {
        await this.props.wp(false, { WhereAreYou: this.state.selectedOption });
        this.props.navigation.navigate('wpEndgoal');
    }

    backButtonAction() {
        this.props.navigation.navigate('wpIdentify')
    }

    _onPress(index) {
        console.log("selected", index, this.state.slides[index]);
        const value = { 'id': this.state.slides[index].id }
        console.log("Value is ")
        console.log(value)
       // AsyncStorage.setItem('whereru', JSON.stringify(value))
        this.setState({
            selectedOption: this.state.slides[index],
            selectedIndex: index
        })
    }

    _renderItem = (item,index ) => {
        console.log(item,index)
        return(
            <View style={styles.slide1}>
                <Image
          style={{width: 50, height: 50}}
          source={{uri: 'https://facebook.github.io/react-native/img/tiny_logo.png'}}
        />
        </View>
        )
        // return (
        //     <View style={styles.slide1}
        //         key={item.id}>
        //         <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        //             <ImageLoad
        //                 style={styles.slide}
        //                 loadingStyle={{ size: 'large', color: 'black' }}
        //                 source={{ uri: (item.image) }}
        //             />
        //             {
        //                 this.state.selectedOption === item.id ? (
        //                     <Image
        //                     style={{ width: 150, height: 150, marginTop: -180 }}
        //                     source={require('../../../assets/SelectedTick.png')}
        //                     />
        //                 ) : (null)
        //             }
        //         </View>
        //     </View>
        // );

    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <BackButton
                    buttonAction={() => this.backButtonAction()} />
                <BottomBorderView horizontal={0} top={0}/>
                <TitleText
                    size={15}
                    color='black'
                    weight='400'
                    alignment='left'
                    width='90%'
                    height={50}
                    title='Where are you at?'
                    top={10}
                />
                {this.state.loader ? <MyActivityIndicator /> : null}
              {
                  this.state.slides.length>0 ?
                <Swiper
                ref={component => this.swiper = component} 
                // onMomentumScrollEnd = {this._onMomentumScrollEnd.bind(this)}
                onIndexChanged={(index) => { console.log("app intro slider selected", index,);this._onPress(index)}}
                loop={false}
                showsButtons={false}
                height={height - 250}
                dotColor={'gray'}
                activeDotColor={'red'}
                scrollEnabled={true}
                index={this.state.selectedIndex}
                // onIndexChanged={()=>{alert('hi')}}

                >
                {this.state.slides.map((item, key) => {
                                                   return (
                                                       <View key={key}
                                                           style={{alignItems: 'center', flex: 1, height: '100%',width: '95%',}}>
                                                           <ImageLoad
                                                            style={{width: '95%', height: '100%'}}
                        style={styles.slide}
                        loadingStyle={{ size: 'large', color: 'black' }}
                        source={{ uri: (item.image) }}
                    />
                                                           {/* <Image
                                                               style={{width: '95%', height: '100%'}}
                                                               source={{uri: item.image}}
                                                               resizeMode='contain'
                                                           /> */}
                                                       </View>
                                                   )
                                               })}
      </Swiper>:<View style={styles.slide}></View>
              
              }
                {/* <AppIntroSlider
                    renderItem={this._renderItem}
                    skipLabel={this.state.selectedOption}
                    slides={this.state.slides}
                    showSkipButton={false}
                    showDoneButton={false}
                    showNextButton={false}
                    showPrevButton={false}
                    ref={ref => this.AppIntroSlider = ref}
                    onSlideChange={(index, lastIndex) => { console.log("app intro slider selected", index, lastIndex); this._onPress(index) }}
                    activeDotStyle={style = { backgroundColor: 'red' }}
                /> */}
                <Button
                    horizontal='12%'
                    top={-20}
                    radius={18}
                    bottom={10}
                    backgColor='#69D3A9'
                    height={36}
                    weight='400'
                    textColor='white'
                    titleSize={18}
                    title='Next'
                    buttonAction={() => this.nextBtnAction()}
                />
            </View>
        );
    }
}
const mapStateToProps = state => {
    return {
        userData: state.user.personal,
        wpData: state.user.wp
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: (type, userData) => updateProfile(type, userData, dispatch),
        wp: (type, userData) => wp(type, userData, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(WhereAreYou);


   //    if(count === 0){
    //     if(item.id===this.state.userData.whereareyouat_id){
    //         setTimeout(() => {
    //             this._onPress(item,index);
    //         }, 500);
    //         count=1
    //        }
    //    }