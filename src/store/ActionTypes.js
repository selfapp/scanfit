
export const CURRENT_LIFESTYLE = 'CURRENT_LIFESTYLE';
export const EQUIPMENT_LIST_START = 'EQUIPMENT_LIST_START';
export const EQUIPMENT_LIST_SUCESS = 'EQUIPMENT_LIST_SUCESS';
export const EQUIPMENT_LIST_FAILED = 'EQUIPMENT_LIST_FAILED';
export const EQUIPMENT_ADD_FAILED = 'EQUIPMENT_ADD_FAILED';
export const SAVEUSER = 'SAVEUSER';
export const WP = 'WP';
export const DAYS = 'DAYS';
export const TODAYWORKOUT ='TODAYWORKOUT';
export const COMPLETEEXERCISE ='COMPLETEEXERCISE';
export const YOURACTIVITY ='YOURACTIVITY';
export const LOADING ='LOADING';
export const STOBLOGLISTING = 'STOBLOGLISTING';
export const GETBLOGLISTINGSTART = 'GETBLOGLISTINGSTART';
export const GETBLOGLISTINGEND = 'GETBLOGLISTINGEND';
export const FETCHCHATUSERS ='FETCHCHATUSERS';
export const FETCHCHATUSERS_SUCCESS ='FETCHCHATUSERS_SUCCESS';
export const FETCHCHATUSERS_FAILED='FETCHCHATUSERS_FAILED';
export const CONTACTSYNCSTART='CONTACTSYNCSTART';
export const CONTACTSYNCEND='CONTACTSYNCEND';
export const CONTACTSYNCFAILED='CONTACTSYNCSTART';
export const CHATLOADINGTRUE='CHATLOADINGTRUE';
export const CHATLOADINGFALSE='CHATLOADINGFALSE';
export const ROOMCREATED='ROOMCREATED';
export const ROOMLEFT='ROOMLEFT';
export const FETCHCHATROOMSSUCCESS = 'FETCHCHATROOMSSUCCESS';
export const FETCHCHATROOMSFAIL = 'FETCHCHATROOMSFAIL';
export const SAVETOKENFCM = 'SAVETOKENFCM';
export const FETCHASSESMENT = "FETCHASSESMENT";
export const FETCHASSESMENTSUCCESS = "FETCHASSESMENTSUCCESS";
export const FETCHASSESMENTFAIL = "FETCHASSESMENTFAIL";


