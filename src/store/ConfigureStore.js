
import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import reducers from './reducers';

const configureStore = () => {

    return createStore(reducers, applyMiddleware(logger));
};
export default configureStore;