import * as actionTypes from '../ActionTypes';
import API from '../../../redAPI';
import {
    AsyncStorage, 
} from 'react-native';



export const CreateNewRoom =async (body,name,myid,urpic,navigation,dispatch) =>{
    dispatch({
        type: actionTypes.CHATLOADINGTRUE,
    })
    API.createRoom(body).then((res) =>res.json()).then( jsonRes => {
        console.log("create new room",jsonRes)
        dispatch({
            type: actionTypes.CHATLOADINGFALSE,
        })
        if(jsonRes.success){
            // alert('hs')
            dispatch({
                type: actionTypes.ROOMCREATED,
                value:jsonRes.records.id
            })  
            console.log(myid)
            // if(jsonRes.records.receiver === myid){
                // API.getotherParty(jsonRes.records.sender).then((res2)=>res2.json()).then(jsonRes2=>{
                //     console.log(jsonRes2) 
                    navigation.navigate('ChatPage',{data:name,picOther:urpic})
                // })
            // }else if(jsonRes.records.sender === myid){
            //     // API.getotherParty(jsonRes.records.receiver).then((res2)=>res2.json()).then(jsonRes2=>{
            //     //     console.log(jsonRes2) 
            //         navigation.navigate('ChatPage',{data:name,})
            //     // })
            // }else{
            //     alert('Something Went Wrong')
            // }
            
        }else if (jsonRes.success === false){
            alert(jsonRes.errors)
        }
    })
}

export const GetChatRoom = async (id,dispatch)=>{
    console.log('get rooms-----',id)
    dispatch({
        type: actionTypes.CHATLOADINGTRUE,
    })
    API.getmyrooms(id).then((res)=>res.json()).then(jsonres=>{
        console.log('get rooms-----',id)
        dispatch({
            type: actionTypes.CHATLOADINGFALSE,
        })
        if(jsonres.success){

            dispatch({
                type: actionTypes.FETCHCHATROOMSSUCCESS,
                value:jsonres.records,
            })
        
    }else if (jsonres.success === false){
        alert(jsonres.errors)
    }
    })
}
export const ChatRoomSelected = async (id,name,urpic,navigation,dispatch)=>{
    console.log("chatroomsel",id,name,urpic )
    dispatch({
        type: actionTypes.ROOMCREATED,
        value:id
    })  
    navigation.navigate('ChatPage',{data:name,picOther:urpic})
}
export const ChatRoomLeft = async (dispatch)=>{
    dispatch({
        type: actionTypes.ROOMLEFT,
    })  
    // navigation.navigate('ChatPage',{data:name,picOther:urpic})
}
export const StoreToken = async (token,dispatch)=>{
// alert(token,"store")
   
    dispatch({
        type: actionTypes.SAVETOKENFCM,
        value:token
    }) 
  

}
export const saveDeviceToken = async (token,dispatch)=>{
    // AsyncStorage.getItem('fcmToken').then(token=>{
        // alert(token,"save")
        console.log("fcmToken at sending", token)
 API.saveDeviceTokenApi(token).then((res)=>res.json()).then(jsonres=>{
    console.log("-----------------------------------------------",jsonres)
 });
    // });
   
  

}
