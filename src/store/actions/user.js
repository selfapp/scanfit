import * as actionTypes from '../ActionTypes';
import API from '../../../redAPI';
import {
    AsyncStorage, 
} from 'react-native';
export const setLifeStyle = value =>{
return {
    type: actionTypes.CURRENT_LIFESTYLE,
    value
}
}
export const fetchMyEquipment = (dispatch) => {
    dispatch({
        type: actionTypes.EQUIPMENT_LIST_START
      });
      API.fetchMyEquipmentList()
      .then((res) =>res.json())
      .then(jsonRes => {
          console.log(jsonRes)
          var flag= false;
          if(jsonRes.success){
              var indexesArr=[];
              var noID=null;
              if(jsonRes.records.length===0){
                dispatch({
                    type: actionTypes.EQUIPMENT_LIST_SUCESS,
                     value: [],
                     indexes:[]
                 })

             }else{
              for(var i=0;i<jsonRes.records.length;i++){

                indexesArr.push(jsonRes.records[i].equipment_id);
                if(jsonRes.records[i].equipment_id === 119){
                    flag=true;
                    noID=i
                }
                if(i==jsonRes.records.length-1){
                    console.log('hi------------------------------------------------------',flag,noID,)
                   if(flag){
                    dispatch({
                        type: actionTypes.EQUIPMENT_LIST_SUCESS,
                         value: jsonRes.records,
                         indexes:indexesArr,
                         noequip:true,
                     })
                    
                   }else{
                       console.log("before dispatch", jsonRes)
                    dispatch({
                        type: actionTypes.EQUIPMENT_LIST_SUCESS,
                         value: jsonRes.records,
                         indexes:indexesArr,
                         noequip:false,
                     })
                   }
                }
                }
              }
          }else{
              dispatch({
                  type: actionTypes.EQUIPMENT_LIST_FAILED,
                  value: jsonRes.error
              })
          }
      })   
}
export const addEquipment = (dispatch) => {
    dispatch({
        type: actionTypes.EQUIPMENT_LIST_START
      });
      API.addEquipment(item)
      .then((res) =>res.json())
      .then(jsonRes => {
          console.log("inreducer",jsonRes.success);
          if(jsonRes.success){
              fetchMyEquipment(dispatch);
          }else{
              dispatch({
                  type: actionTypes.EQUIPMENT_LIST_FAILED,
                  value: jsonRes.error
              })
          }
      })     
}

export const updateProfile = async (type,data,dispatch)=>{
    console.log("dispatch_______________-----------",type,data)
    if(!type){
        var sDt = await JSON.stringify(data)
        AsyncStorage.setItem("user",sDt)
        dispatch({
            type: actionTypes.SAVEUSER,
            value: data
          });  
    }
// API.profile()
}
export const wp = async (type,data,dispatch)=>{
    console.log("dispatch__________",type,data)
    if(!type){
        var sDt = await JSON.stringify(data)
        // AsyncStorage.setItem("user",sDt)
        dispatch({
            type: actionTypes.WP,
            value: data
          });  
    }
// API.profile()
}

export const daysF = async (type,data,dispatch)=>{
    console.log("dispatch__________",type,data)
    if(!type){
        var sDt = await JSON.stringify(data)
        AsyncStorage.setItem("daysf",sDt)
        dispatch({
            type: actionTypes.DAYS,
            value: data
          });  
    }
// API.profile()
}
export const saveuser = (userData,dispatch) => {
    console.log(userData);
    dispatch({
        type: actionTypes.SAVEUSER,
        value: userData
      });    
}

export const todayworkout = (body,dispatch) => {
    console.log("today body",body)
    dispatch({
        type: actionTypes.LOADING,
    })
      API.todWorkout(body)
      .then((res) =>res.json())
      .then(jsonRes => {
          if(!jsonRes.success){
              alert(jsonRes.errors)
              dispatch({
                type: actionTypes.GETBLOGLISTINGEND,
            })
          }else{
          console.log("inreducer todayworkout",jsonRes);
          dispatch({
            type: actionTypes.TODAYWORKOUT,
            value: jsonRes.records
        })
    }
      })     
}

export const completeExercise = (data,dispatch) => {
    console.log("completeExercise",data);
    dispatch({
        type: actionTypes.LOADING,
    })
    API.completexr(data)
    .then((res) =>res.json())
    .then(jsonRes => {
        console.log("inreducer todayworkout End",jsonRes);
        if(!jsonRes.success){
            alert(jsonRes.errors)
        }
        console.log("inreducer todayworkout End",jsonRes);
        dispatch({
          type: actionTypes.COMPLETEEXERCISE,
          value: jsonRes.records
      })
    })    
}

export const yourActivity = (dispatch) => {
    dispatch({
        type: actionTypes.LOADING,
    })
    API.yrActivity(item)
    .then((res) =>res.json())
    .then(jsonRes => {
        console.log("inreducer todayworkout",jsonRes.success);
        dispatch({
          type: actionTypes.YOURACTIVITY,
          value: jsonRes.records
      })
    }) 
}
export const GetBlogListFunction =(page,refreshing,dispatch) =>{
    dispatch({
        type: actionTypes.GETBLOGLISTINGSTART,
    })
    API.getBlogList(page).then((res) =>res.json()).then( jsonRes => {
        // dispatch({
        //     type: actionTypes.GETBLOGLISTINGEND,
        // })
        console.log(jsonRes)
        if(jsonRes.success){
            // alert('hs')
            dispatch({
                type: actionTypes.STOBLOGLISTING,
                value:jsonRes.records,
                refresh:refreshing
            })  
        }
    })
}
export const GetChatingUsers = async (searchTerm,dispatch) => {
    dispatch({
        type:actionTypes.FETCHCHATUSERS
    })
    if(searchTerm){
        await API.getUserList({search:searchTerm}).then((res)=>res.json()).then(async jsonRes =>{
            console.log('chatlistusers', jsonRes)
            if(jsonRes.success){
                dispatch({
                    type:actionTypes.FETCHCHATUSERS_SUCCESS,
                    value:jsonRes.records
                })
            }
            else{
                dispatch({
                    type:actionTypes.FETCHCHATUSERS_FAILED,
                })
            }
        })
    }else{
        await API.getUserList().then((res)=>res.json()).then(async jsonRes =>{
            console.log('chatlistusers', jsonRes)
            if(jsonRes.success){
                dispatch({
                    type:actionTypes.FETCHCHATUSERS_SUCCESS,
                    value:jsonRes.records
                })
            }
            else{
                dispatch({
                    type:actionTypes.FETCHCHATUSERS_FAILED,
                })
            }
        })
    }
    
}
export const savenewDetails =async(data,days,dispatch)=>{
    console.log("savenewDetails=============================.................>>>>",days);
    await  dispatch({
        type: actionTypes.SAVEUSER,
        value: data
      }); 
if(days){
    await   dispatch({
        type: actionTypes.DAYS,
        value: days
      });
} 
}
export const FindScanfitContacts = async (Contacts,dispatch) => {
    dispatch({
        type:actionTypes.CONTACTSYNCSTART
    })
    console.log(Contacts);
        await API.getAvailableContacts({contacts:Contacts}).then((res)=>res.json()).then(async jsonRes =>{
            console.log('chatlistusers', jsonRes)
            if(jsonRes.success){
                dispatch({
                    type:actionTypes.CONTACTSYNCEND,
                    value:jsonRes.records
                })
            }
            else{
                dispatch({
                    type:actionTypes.CONTACTSYNCFAILED,
                })
            }
        })}

        export const fetchAssesments = async (dispatch) => {
            console.log("--------------------------------------------------------------------")
            dispatch({
                type:actionTypes.FETCHASSESMENT
            })
                await API.getMyAssesment().then((res)=>res.json()).then(async jsonRes =>{
                    console.log('myAssesment', jsonRes)
                    if(jsonRes.success){
                        var data=[];
                        data.push({
                            title: 'General',
                            uid:'gen',
                            content: [{Title:"Resting heart rate",icon:require('../../assets/Heart.png'),unit:'BPM'},{Title:"Max Heart Rate",icon:require('../../assets/gym.png'),unit:'BPM'},{Title:"VO2 max",icon:require('../../assets/weight-scale.png'),unit:'Kg/Min'},{Title:"Body fat",icon:require('../../assets/body.png'),unit:'BPM'}],
                          });
                          data.push(jsonRes.records.assessments.upper_body)

                          data.push(jsonRes.records.assessments.lower_body)
                          
                        dispatch({
                            type:actionTypes.FETCHASSESMENTSUCCESS,
                            value:jsonRes.records,
                            special:data
                        })
                    }
                    else{
                        dispatch({
                            type:actionTypes.FETCHASSESMENTFAIL,
                        })
                    }
                })}
                export const PostAssesment = async (data,dispatch) => {
                    console.log("--------------------------------------------------------------------",data)
                    dispatch({
                        type:actionTypes.FETCHASSESMENT
                    })
                        await API.PostMyAssesment(data).then((res)=>res.json()).then(async jsonRes =>{
                            console.log('post assesments ---------', jsonRes)
                            if(jsonRes.success === true){
                                alert("Assesment recorded succesfully!");
                                fetchAssesments(dispatch);
                                // dispatch({
                                //     type:actionTypes.FETCHASSESMENTFAIL,
                                // })
                            }
                            else{
                                alert("Something went wrong!");
                                dispatch({
                                    type:actionTypes.FETCHASSESMENTFAIL,
                                })
                            }
                        })}
        
export const myProfile = async(dispatch) => {
    // dispatch({
    //     type: actionTypes.LOADING,
    // })
    await API.getProfile()
    .then((res) =>res.json())
    .then(async jsonRes => {
       if(jsonRes.success){
        await  AsyncStorage.setItem("user",JSON.stringify(jsonRes.user));
       await  AsyncStorage.setItem("daysf",JSON.stringify(jsonRes.user.trainings.days))
        dispatch({
            type: actionTypes.DAYS,
            value: jsonRes.user.trainings.days
          }); 
          dispatch({
            type: actionTypes.SAVEUSER,
            value: jsonRes.user
          }); 
       }
    }) 

  
   
}
