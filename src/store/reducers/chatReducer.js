import * as actionTypes from '../ActionTypes';

const initialState = {
    chattingLoading:false,
    errors: {},
    roomId:null,
    chatRooms:[],
    fcmToken:null,
}

export const chatReducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.CHATLOADINGTRUE:{
            return{
              ...state,
              chattingLoading:true
            }
          }
          case actionTypes.CHATLOADINGFALSE:{
            return{
              ...state,
              chattingLoading:false
            }
          }
          case actionTypes.ROOMCREATED:{
            return{
              ...state,
              roomId:action.value
            }
          }
          case actionTypes.FETCHCHATROOMSSUCCESS:{
            return{
              ...state,
              chatRooms:action.value
            }
          }
          case actionTypes.SAVETOKENFCM:{
            return{
              ...state,
              fcmToken:action.value
            }
          }
          case actionTypes.ROOMLEFT:{
            return{
              ...state,
              roomId:null,
            }
          }
        default :{
            return{
                ...state,
               
            }
        }
    }
}