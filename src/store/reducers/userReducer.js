import * as actionTypes from '../ActionTypes';

const initialState = {
    isSelected:null,
    equipmetList:[],
    HAVENOEQUIP:false,

    loading:false,
    errors: {},
    personal:{},
    wp:{},
    days:[],
    todayWorkout:[],
    CompleteExercice:[],
    YourActivity:[],
   blogLoading:false,
   blogList:[],
   selectedItems_Equipments:[],
   chatLoader:false,
   ChatUserList:[],
   Contacts:[],
   MyAssesment:[],
   AssesmentList:[],
   contactLoader:false,

}

export const userReducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.LOADING:{
            return {
                ...state,
                loading:true
            }
        }
        case actionTypes.TODAYWORKOUT:{
            return {
                ...state,
                todayWorkout:action.value,
                loading:false
            }
        }
        case actionTypes.YOURACTIVITY:{
            return {
                ...state,
                YourActivity:action.value,
                loading:false
            }
        }
        case actionTypes.COMPLETEEXERCISE:{
            return {
                ...state,
                CompleteExercice:action.value,
                loading:false
            }
        }
        case actionTypes.CURRENT_LIFESTYLE:{
            return{
                ...state,
                isSelected: action.value
            }
        }
        case actionTypes.EQUIPMENT_LIST_START:{
            return {
                ...state,
                loading: true,
                equipmetList:[],
                HAVENOEQUIP:false,
                // selectedItems_Equipments:[]
            }
        }
        case actionTypes.EQUIPMENT_LIST_SUCESS:{
            return {
                ...state,
                equipmetList: action.value,
                selectedItems_Equipments:action.indexes,
                HAVENOEQUIP:action.noequip,
                loading: false
            }
        }
        case actionTypes.EQUIPMENT_LIST_FAILED:{
            console.log("failed");
            return {
                ...state,
                loading: false,
                errors: action.value
            }
        }
        case actionTypes.EQUIPMENT_ADD_FAILED:{
            return {
                ...state,
                loading: false,
                errors: action.value
            }
        }
        case actionTypes.SAVEUSER:{
            console.log('SAVEuser',action.value)
            return {
                ...state,
                personal:action.value
            }
        }
        case actionTypes.WP:{
            return {
                ...state,
                wp:{...state.wp,...action.value}
            }
        }
        case actionTypes.DAYS:{
            console.log('days',action.value)

            return {
                ...state,
                days:action.value
            }
        }
        case actionTypes.DAYS:{
            return {
                ...state,
                days:action.value
            }
        }
        case actionTypes.GETBLOGLISTINGSTART:{
            return {
                ...state,
                loading: true,
            }
        }
        case actionTypes.GETBLOGLISTINGEND:{
            return {
                ...state,
                loading: false,
            }
        }
        case actionTypes.STOBLOGLISTING:{
          if(!action.refresh){
            return {
                ...state,
                blogList:[...state.blogList,...action.value,]
            }
          }else{
            return {
                ...state,
                blogList:action.value
            }
          }
        }
        case actionTypes.FETCHCHATUSERS:{
            return{
              ...state,
              chatLoader:true
            }
          }
          case actionTypes.FETCHCHATUSERS_SUCCESS:{
            return{
              ...state,
              chatLoader:false,
              ChatUserList:action.value
            }
          }
          case actionTypes.FETCHCHATUSERS_FAILED:{
            return{
              ...state,
              chatLoader:false
            }
          }
          case actionTypes.CONTACTSYNCSTART:{
            return{
              ...state,
              contactLoader:true
              // chatLoader:true
            }
          }
          case actionTypes.CONTACTSYNCEND:{
            return{
              ...state,
              chatLoader:false,
              contactLoader:false,
              Contacts:action.value
            }
          }
          case actionTypes.CONTACTSYNCFAILED:{
            return{
              ...state,
              chatLoader:false,
              contactLoader:false,
            }
          }
          case actionTypes.FETCHASSESMENT:{
            return{
              ...state,
              loading: true
            }
          }
          case actionTypes.FETCHASSESMENTFAIL:{
            return{
              ...state,
              loading: false
            }
          }
          case actionTypes.FETCHASSESMENTSUCCESS:{
            return{
              ...state,
              loading: false,
              MyAssesment:action.value,
              AssesmentList:action.special
            }
          }
        default :{
            return{
                ...state,
               
            }
        }
    }
}